#include "testing/catch.hpp"

#include "simulator/dsl.h"
#include "utilities/Utilities.h"
//#include "benchmarks.h"
#include "collision/gpuCon.h"
//#include "ofMain.h"
#include "simulator/system.h"

#include <map>

#include "mapbox/variant.hpp"

#include "settings.h"
#include "utilities/ttimer.h"

#include "simulator/dsl.h"

using namespace dsl;

TEST_CASE("if for reaction"
          "[modelling]") {
  setupDSL();

  set_temperature(300 * units::kelvin);
  set_viscocity(nature::water_viscocity);

  species(bird);
  radius(bird) = 1 * units::nano_meter;

  reaction splitInTwo(as_A(bird));
  splitInTwo.rate = 1e15 / units::second;
  splitInTwo.createAt_A(bird);

  bird.local_density = nature::protein_density;

  setSize(100 * units::nano_meter);

  SECTION(" not allowed first order ") {
    splitInTwo.reacPossibleFunc = B_FUNC(false);
    putSomewhere(bird, 100);
    runNoVisual(100);
    REQUIRE(usedSystem->size() == 100);
  }

  SECTION("allowed first order ") {
    putSomewhere(bird, 100);
    REQUIRE(usedSystem->size() == 100);
    runNoVisual(100);
    REQUIRE(usedSystem->size() > 100);
  }

  setupDSL();
}
