#include "testing/catch.hpp"

#include "simulator/dsl.h"
#include "utilities/Utilities.h"
//#include "benchmarks.h"
#include "collision/gpuCon.h"
//#include "ofMain.h"
#include "simulator/system.h"
#include <map>

#include "mapbox/variant.hpp"

#include "settings.h"
#include "utilities/ttimer.h"

#include "simulator/dsl.h"

using namespace dsl;

TEST_CASE("splitting and reaction inside") {
  setupDSL();
  ADD_SPECIES(bird);
  ADD_SPECIES(fish);
  auto ADD_PROPERTY(weight, bird, int, 33);

  bird.local_density = nature::protein_density;
  fish.local_density = nature::protein_density;

  // auto weight = bird.addProperty<int>(33);

  /* setting size of the system */
  set_temperature(300 * units::kelvin);
  set_viscocity(nature::water_viscocity);
  setSize(420 * units::nano_meter);

  radius(bird) = 5 * units::nano_meter;
  radius(fish) = 1 * units::nano_meter;

  /* Adding objects to system */
  putInside(putSomewhere(bird, 10), fish, 3);
  // putSomewhere(fish, 0);

  reaction grow(as_A(bird));
  grow.rate = .2 / units::second;
  grow.afterFuncA = G_FUNC(surface(A) = surface(A) + 1;
                           if (radius(A) > 15) { std::cout << "groß\n"; });

  reaction split(as_A(bird));
  split.divide(as_B(bird), 10);
  split.reacPossibleFunc = B_FUNC(radius(A) > 15);
  assert(split.doesDivide);
  split.afterFuncA = [=](Particle &A, Particle &B,
                         pSystem &system) { radius(A) *= .5; };
  split.afterFuncB = [=](Particle &A, Particle &B,
                         pSystem &system) { radius(B) = radius(A); };
  split.rate = 100;
  // split.rateFunc = [=](const Particle &A, const Particle &B,
  //                   const pSystem &system) { return 100.; };

  /*reaction reac(as_A(fish), as_B(bird));
  reac.rate = 100;
  reac.rateFunc = [=](const Particle &A, const Particle &B,
                      const pSystem &system) {
    return 25 / std::pow(system.time, 1.);
  };
  reac.AinB();
  reac.afterFuncA =
      G_FUNC(colorType(A)++; std::cout << "InReac: " << system.time << "\n";);

  reaction leaver(as_A(fish), as_B(bird));
  leaver.rateFunc = [=](const Particle &A, const Particle &B,
                        const pSystem &system) {
    // colorType(A)++;
    return 1e-10; // std::max(system.time - 150, 1e-10);
  };
  leaver.afterFuncB =
      G_FUNC(colorType(A)++; std::cout << "OUTReac: " << system.time << "\n";);
  leaver.AleaveB();
  */
  /*leaver.afterFuncA =
      G_FUNC(colorType(A) = util::randomZeroOne() * 100; radius(A) *= 1.6;);*/

  /* Adding observations */
  observe::nr_of(bird);
  observe::nr_of_A(fish, B_FUNC(A.parentId != -1));
  observe::average_prop(bird, weight);
  observe::average_prop(bird, volume);
  observe::average_prop(bird, surface);

  runNoVisual(100);
  setupDSL();
}
