#include "testing/catch.hpp"

#include "simulator/dsl.h"
#include "utilities/Utilities.h"
//#include "benchmarks.h"
#include "collision/gpuCon.h"
//#include "ofMain.h"
#include "simulator/system.h"

#include <map>

#include "mapbox/variant.hpp"

#include "settings.h"
#include "utilities/ttimer.h"

#include "simulator/dsl.h"

using namespace dsl;

TEST_CASE("Division of small particles in a cell.") {
  setupDSL();
  set_temperature(300 * units::kelvin);
  set_viscocity(3.5E-11 * units::kilogram / units::nano_meter / units::second);

  ADD_SPECIES(cell); // yeast cell
  // volume(cell) = 96.E9;
  radius(cell) = 1240;
  auto phase = cell.addProperty<int>(1); // 1 : G1, 2: S/G2, 3 = M
                                         // auto ADD_PROPERTY
  cell.local_density = nature::protein_density;
  colorType(cell) = 1;
  cell.move = false;

  ADD_SPECIES(M_A); // active MPF
  radius(M_A) = 3 * 10;
  colorType(M_A) = 11;
  M_A.local_density = nature::protein_density;

  ADD_SPECIES(M_IA); // M_I - M_A complex
  radius(M_IA) = 3.8 * 10;
  colorType(M_IA) = 4;
  M_IA.local_density = nature::protein_density;

  const double box_size = 5000 * units::nano_meter;
  setSize(box_size);

  reaction activation_2(as_A(M_IA));
  activation_2.rate = 20. / 60;
  activation_2.divide(as_B(M_A), 5.E18 * units::nano_meter / units::second /
                                     units::second);
  activation_2.afterFuncA =
      G_FUNC(changeSpecies(A, M_A); radius(A) = 3 * 10 * units::nano_meter;);
  activation_2.afterFuncB = G_FUNC(radius(B) = 3 * 10 * units::nano_meter;);

  auto cell_1 = putHere(cell, box_size / 2, box_size / 2, box_size / 2);
  putInside(cell_1, M_IA, 500);

  // maxInternalFramerate(500);
  // visualize(30);
  runNoVisual(10000);
  setupDSL();
}
