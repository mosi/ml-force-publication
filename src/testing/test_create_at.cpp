#include "testing/catch.hpp"

#include "simulator/dsl.h"
#include "utilities/Utilities.h"
//#include "benchmarks.h"
#include "collision/gpuCon.h"
//#include "ofMain.h"
#include "simulator/system.h"

#include <map>

#include "mapbox/variant.hpp"

#include "settings.h"
#include "utilities/ttimer.h"

#include "simulator/dsl.h"

using namespace dsl;

TEST_CASE("test create_atA") {
  setupDSL();
  set_temperature(300 * units::kelvin);
  set_viscocity(3.5E-11 * units::kilogram / units::nano_meter / units::second);

  const int box_size = 5200 * units::nano_meter;
  setSize(box_size);

  ADD_SPECIES(cell);
  radius(cell) = 2500 * units::nano_meter;
  assert(units::nano_meter > 0);
  // cell.radius = 2500 * units::nano_meter;
  assert(cell.targetRadius > 0);
  colorType(cell) = 55;
  cell.move = false;
  cell.local_density = nature::protein_density;

  ADD_SPECIES(comp_1);
  radius(comp_1) = 250 * units::nano_meter;
  colorType(comp_1) = 3;
  comp_1.move = false;
  comp_1.local_density = nature::protein_density;

  ADD_SPECIES(ves_A);
  radius(ves_A) = 25 * units::nano_meter;
  ves_A.move = false;
  colorType(ves_A) = 1;
  ves_A.local_density = nature::protein_density;

  ADD_SPECIES(ves_B);
  radius(ves_B) = 25 * units::nano_meter;
  // colorType(ves_B) = 1;
  ves_B.local_density = nature::protein_density;

  Vec pos_1;
  pos_1.at(0) = 0.5 * box_size + 0.25 * 2 * radius(cell);
  pos_1.at(1) = 0.5 * box_size - 0.07 * 2 * radius(cell);
  if (DIMENSION == 3) {
    pos_1.at(2) = 0;
  }

  reaction budding_1_A(as_A(comp_1));
  budding_1_A.reacPossibleFunc = B_FUNC(surface(A) > 10);
  budding_1_A.rateFunc =
      R_FUNC(volume(A) / pow(units::nano_meter, 3) / units::second);
  budding_1_A.createAt_A(ves_A);

  auto cell_1 = putHere(cell, 0.5 * box_size, 0.5 * box_size, 0.5 * box_size);
  putInsideAt(cell_1, comp_1, pos_1.at(0), pos_1.at(1), 0.5 * box_size);
  putSomewhere(ves_B, 2);

  // runNoVisual(5000);
  // maxInternalFramerate(500);
  // visualize(30);

  while (usedSystem->size() <= 1) {
    runNoVisual(10);
    // std::cout << " do step\n";
  }

  // REQUIRE(usedSystem->size() > 5);
  double dist;
  Particle comp;
  for (auto test : usedSystem->data) {
    auto test_2 = test.second;
    if (test_2.id == 1) {
      comp = test_2;
      std::cout << "r = " << comp.radius << "\n";
    }
    if (test_2.id > 3) {
      dist = usedSystem->calcDistance(comp, test_2);
      REQUIRE(dist < ((comp.radius + test_2.radius) * 1.05));
      REQUIRE(dist > (comp.radius + test_2.radius) * 0.95);
    }
  }
  setupDSL();
}
