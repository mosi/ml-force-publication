// 2D model
#include "simulator/dsl.h"

using namespace dsl;

namespace model {

void lipidraft_model() {
  set_temperature(300 * units::kelvin);
  set_viscocity(1E-3 * units::pascal * units::second);

  ADD_SPECIES(LRP5); // LRP 5/6
  radius(LRP5) = 3.7 * units::nano_meter;
  LRP5.local_density = nature::protein_density;
  colorType(LRP5) = 2;

  ADD_SPECIES(LR); // Lipid Rafts
  radius(LR) = 200 * units::nano_meter;
  LR.local_density = nature::protein_density;
  LR.local_viscosity = 10E-3 * units::pascal * units::second;
  colorType(LR) = 5;

  ADD_SPECIES(CK1); // CK1-gamma
  radius(CK1) = 2.4 * units::nano_meter;
  CK1.local_density = nature::protein_density;
  colorType(CK1) = 8;

  const double box_size = 709;
  setSize(box_size);

  reaction CK1_in(as_A(CK1), as_B(LR));
  CK1_in.barrier = 0 * units::kilogram * units::nano_meter * units::nano_meter /
                   units::second / units::second;
  CK1_in.AinB();

  reaction LRP5_in(as_A(LRP5), as_B(LR));
  LRP5_in.barrier = 12.E-3 * units::kilogram * units::nano_meter *
                    units::nano_meter / units::second / units::second;
  LRP5_in.AinB();

  reaction CK1_out(as_A(CK1), as_B(LR));
  CK1_out.barrier = 0 * units::kilogram * units::nano_meter *
                    units::nano_meter / units::second / units::second;
  CK1_out.AleaveB();

  reaction LRP5_out(as_A(LRP5), as_B(LR));
  LRP5_out.barrier = 0 * units::kilogram * units::nano_meter *
                     units::nano_meter / units::second / units::second;
  LRP5_out.AleaveB();

  auto LR_test = putHere(LR, box_size / 2, box_size / 2, box_size / 2);
  putSomewhere(LRP5, 200);
  putSomewhere(CK1, 200);

  /* Adding observations */
  observe::nr_of(LRP5);
  observe::nr_of(CK1);
  observe::nr_of(LRP5, B_FUNC(A.parentId != -1));
  observe::nr_of(CK1, B_FUNC(A.parentId != -1));

  /* run the visualization */
  visualize(25);
}
} // namespace model