// 3D model
#include "simulator/dsl.h"

using namespace dsl;

namespace model {

void irrev_react() {
  const double box_size = 58.5 * units::nano_meter;

  set_temperature(300 * units::kelvin);
  set_viscocity(4.392E-12 * units::kilogram / units::nano_meter /
                units::second);
  setSize(box_size);

  /* declaring species and properties */
  ADD_SPECIES(part_A);
  part_A.local_density = nature::protein_density;
  radius(part_A) = 0.5 * units::nano_meter;
  colorType(part_A) = 1;

  /* declaring A + A -> 0 reaction */
  reaction irrev(as_A(part_A), as_B(part_A));
  irrev.barrier = 0.;
  irrev.Adead();
  irrev.Bdead();
  irrev.setOverlapp(0.01);

  /* inital state */
  putSomewhere(part_A, 2000);

  /* defines output */
  observe::nr_of(part_A);

  visualize(25);
}
} // namespace model
