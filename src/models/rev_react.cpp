// 3D model
#include "simulator/dsl.h"

using namespace dsl;

namespace model {

void rev_react() {
  set_temperature(300 * units::kelvin);
  set_viscocity(4.392E-12 * units::kilogram / units::nano_meter /
                units::second);

  const double box_size = 58.5 * units::nano_meter;

  /* declaring species and properties */
  ADD_SPECIES(part_A);
  radius(part_A) = 0.5 * units::nano_meter;
  colorType(part_A) = 1;
  part_A.local_density = nature::protein_density;

  ADD_SPECIES(part_B);
  radius(part_B) = 0.5 * units::nano_meter;
  colorType(part_B) = 3;
  part_B.local_density = nature::protein_density;

  ADD_SPECIES(part_C);
  radius(part_C) = 0.5 * units::nano_meter;
  colorType(part_C) = 6;
  part_C.local_density = nature::protein_density;

  setSize(box_size);

  /* declaring A + B -> C reaction */
  reaction dimer(as_A(part_A), as_B(part_B));
  dimer.barrier = 0.;
  dimer.afterFuncA =
      G_FUNC(changeSpecies(A, part_C); radius(A) = 0.5 * units::nano_meter;);
  dimer.Bdead();
  dimer.setOverlapp(0.3);

  /* declaring C -> A + B reaction */
  reaction decay_mol(as_A(part_C));
  decay_mol.rate = 4390352 / units::second;
  decay_mol.divide(as_B(part_B),
                   5.E25 * units::nano_meter / units::second / units::second);
  decay_mol.afterFuncA =
      G_FUNC(changeSpecies(A, part_A); radius(A) = 0.5 * units::nano_meter;);
  decay_mol.afterFuncB = G_FUNC(radius(B) = 0.5 * units::nano_meter;);

  /* inital state */
  putSomewhere(part_A, 1000);
  putSomewhere(part_B, 1000);

  /* defines output */
  observe::nr_of(part_A);
  observe::nr_of(part_B);
  observe::nr_of(part_C);

  visualize(25);
}
} // namespace model