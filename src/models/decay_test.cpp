// 3D model
#include "simulator/dsl.h"

using namespace dsl;

namespace model {

void decay_test() {
  double box_size = 1100 * units::nano_meter;
  set_temperature(300 * units::kelvin);
  set_viscocity(1E-10 * units::kilogram / units::nano_meter / units::second);
  setSize(box_size);

  /* declaring species and properties */
  ADD_SPECIES(A);
  A.local_density = nature::protein_density;
  radius(A) = 5 * units::nano_meter;
  colorType(A) = 8;

  /* declaring A -> 0 reaction */
  reaction decay(as_A(A));
  decay.rate = 0.5 / units::second;
  decay.Adead();

  /* inital state */
  putSomewhere(A, 1000);

  /* defines output */
  observe::nr_of(A);

  visualize(25);
}
} // namespace model