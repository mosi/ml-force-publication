// 3D model
#include "simulator/dsl.h"
#include <random>
#include <cmath>
#include <ctime>

using namespace dsl;

namespace model {

void diff_test() {

  double box_size = 1100 * units::nano_meter;
  set_temperature(300 * units::kelvin);
  set_viscocity(4.392E-12 * units::kilogram / units::nano_meter /
                units::second);
  setSize(box_size);

  /* declaring species and properties */
  ADD_SPECIES(A);
  A.local_density = nature::protein_density;
  radius(A) = 0.5 * units::nano_meter;
  colorType(A) = 8;

  double D;
  D = nature::boltzmann * 300 /
      (6 * mPI * 4.392E-12 * units::kilogram / units::nano_meter /
       units::second * 0.5 * units::nano_meter);
  double x0 = 500;
  const unsigned int seed = time(0);

  std::mt19937 gen{ seed };
  std::normal_distribution<> d{ 0, 1 };

  for (int i = 1; i <= 20; i++) {
    putHere(A, sqrt(2. * D * 1 * units::micro * units::second) * d(gen) + x0,
            sqrt(2. * D * 1 * units::micro * units::second) * d(gen) + x0,
            sqrt(2. * D * 1 * units::micro * units::second) * d(gen) + x0);
  };

  visualize(25);
}
}