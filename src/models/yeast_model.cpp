// 3D model
#include "simulator/dsl.h"

using namespace dsl;

namespace model {

void yeast_model() {
  set_temperature(300 * units::kelvin);
  set_viscocity(3.5E-11 * units::kilogram / units::nano_meter / units::second);

  const double V_0 = 50 * 1E9 * pow(units::nano_meter, 3);
  const int Dtot_int = 1000;
  const double k1 = 0.015 / units::minute * 1000;
  const double k2 = 200 / units::minute;
  const double k3prime = 0.018 / units::minute;
  const double k3 = 680 / units::minute;
  const double k4 = 4.5 / units::minute;
  const double k5 = 0.6 / units::minute;
  const double k6 = 1 / units::minute;
  const double k7 = 1.E4 / units::minute;
  const double k8 = 1.E4 / units::minute;
  const double k9 = 1.E4 / units::minute;
  const double k10 = 1 / units::minute;
  const double k13 = 0.005 / units::minute;

  const double Td = 116;
  const int t7 = 250;
  const int t8 = 70;
  const int t9 = 20;
  const double box_size = (50000 + 50000) * units::nano_meter;

  ADD_SPECIES(Box);
  radius(Box) = 24000 * units::nano_meter;
  Box.move = false;
  Box.local_density = nature::water_density;
  Box.colortype = 5;

  ADD_SPECIES(F_M); // Pheromone M
  radius(F_M) = 1. * 1e-6 * units::meter;
  F_M.local_density = nature::protein_density;
  F_M.colortype = 8;

  ADD_SPECIES(F_P); // Pheromone P
  radius(F_P) = 1. * 1e-6 * units::meter;
  F_P.local_density = nature::protein_density;
  F_P.colortype = 12;

  ADD_SPECIES(X); // Sxa2
  radius(X) = 1. * 1e-6 * units::meter;
  X.local_density = nature::protein_density;
  X.colortype = 15;

  ADD_SPECIES(dummy);
  radius(dummy) = 1. * 1e-6 * units::meter;
  dummy.local_density = nature::protein_density;
  dummy.colortype = 45;

  ADD_SPECIES(cell); // yeast cell
  volume(cell) = V_0;
  cell.local_density = 1000 * nature::protein_density;
  colorType(cell) = 2;
  cell.move = true;
  cell.diffuse = false;

  auto mating = cell.addProperty<int>(-1); // -1 = P-type, 1 = M-type
  auto receptor_total = cell.addProperty<int>(600);
  auto receptor_occupied = cell.addProperty<int>(0);
  auto phase = cell.addProperty<int>(1); // 1 : G1, 2: S/G2, 3 = M
                                         // auto ADD_PROPERTY
  auto C = cell.addProperty<int>(0);
  auto Y = cell.addProperty<int>(0);
  auto Yp = cell.addProperty<int>(0);
  auto D = cell.addProperty<int>(Dtot_int - 1);
  auto Mi = cell.addProperty<int>(0);
  auto Ma = cell.addProperty<int>(1);
  auto Mr = cell.addProperty<int>(0);
  auto dtot = cell.addProperty<int>(1000);
  auto switching = cell.addProperty<int>(1); // 0 = don't switch, 1 = switch

  /* setting size of the system */
  setSize(box_size);

  Vec up;
  up.at(0) = 0;
  up.at(1) = 0;
  up.at(2) = -1;
  /* add a force, which holds the cells on a plane */
  add_force(F_FUNC(1E-23 * (box_size / 2 - A.position.at(2))),
            B_FUNC(A.type == cell.type));
  /* add a force which allows pheromones only to diffuse above the plane */
  add_force(
      F_FUNC(1E-21 * up),
      B_FUNC((A.type == F_M.type || A.type == F_P.type || A.type == X.type) &&
             A.position[1] > box_size / 2));

  /* creates a second cell once at the beginning of the simulation */
  reaction set_second_cell(as_A(cell));
  set_second_cell.rate = 100000 / units::second;
  set_second_cell.divide(as_B(cell), 1.E-8);
  set_second_cell.reacPossibleFunc = B_FUNC(
      nr_in_system(cell) == 1 && usedSystem->time < (100 * units::second));
  set_second_cell.afterFuncA = G_FUNC(mating(B) = 1; B.colortype = -2;);

  /* cell cycle reaction (1-9) */
  // 1) cyclin synthesis in cell   0 -> Y
  reaction Y_synthesis(as_A(cell));
  Y_synthesis.rateFunc = R_FUNC(k1);
  Y_synthesis.afterFuncA = G_FUNC(Y(A) = Y(A) + 1;);

  // 2) formation of inctive MPF complex   D + Y -> M_I
  reaction form_MPF(as_A(cell));
  form_MPF.reacPossibleFunc = B_FUNC(Y(A) > 0);
  form_MPF.rateFunc = R_FUNC(k2 * Y(A) * D(A) / dtot(A));
  form_MPF.afterFuncA =
      G_FUNC(Y(A) = Y(A) - 1; D(A) = D(A) - 1; Mi(A) = Mi(A) + 1;);

  // 3) activation of MPF complex   M_I + M_A -> 2 M_A
  reaction activation_1(as_A(cell));
  activation_1.reacPossibleFunc = B_FUNC(Mi(A) > 0 && Ma(A) > 0);
  activation_1.rateFunc =
      R_FUNC(k3prime * Mi(A) + k3 * Ma(A) / dtot(A) * Ma(A) / dtot(A) * Mi(A));
  activation_1.afterFuncA = G_FUNC(Mi(A) = Mi(A) - 1; Ma(A) = Ma(A) + 1;);

  // 4) breakage of activated MPF complex M_A -> Y_p + D
  reaction breakage(as_A(cell));
  breakage.reacPossibleFunc = B_FUNC(Ma(A) > 1);
  breakage.rateFunc = R_FUNC(k4 / (volume(A) / V_0) * Ma(A));
  breakage.afterFuncA =
      G_FUNC(Ma(A) = Ma(A) - 1; Yp(A) = Yp(A) + 1; D(A) = D(A) + 1;);

  // 5) cyclin degradation   Y_p -> 0
  reaction degradation(as_A(cell));
  degradation.reacPossibleFunc = B_FUNC(Yp(A) > 0);
  degradation.rateFunc = R_FUNC(k5 * Yp(A));
  degradation.afterFuncA = G_FUNC(Yp(A) = Yp(A) - 1;);

  // 6) cell growth
  reaction growth(as_A(cell));
  growth.rateFunc = R_FUNC(k6);
  growth.afterFuncA = G_FUNC(volume(A) = volume(A) + V_0 / Td;);

  // 7) cell cycle transition from G1->S/G2
  reaction phase_c1(as_A(cell));
  phase_c1.reacPossibleFunc = B_FUNC(Mi(A) > t7 && phase(A) == 1); //#M_I > t7
  phase_c1.rate = k7;
  phase_c1.afterFuncA = G_FUNC(phase(A) = 2;);

  // 8) cell cycle transition from S/G2->M
  reaction phase_c2(as_A(cell));
  phase_c2.reacPossibleFunc = B_FUNC(Ma(A) > t8 && phase(A) == 2); //#M_A > t8
  phase_c2.rate = k8;
  phase_c2.afterFuncA = G_FUNC(phase(A) = 3;);

  // 9) cell division (transition from M->G1)
  reaction division(as_A(cell));
  division.reacPossibleFunc = B_FUNC(Ma(A) < t9 && phase(A) == 3); // #M_A < t9
  division.rate = k9;
  division.divide(as_B(cell), 1.E-8);
  division.afterFuncA = G_FUNC(
      volume(B) = 0.5 * volume(A); phase(B) = 1; D(B) = D(A) / 2;
      Y(B) = Y(A) / 2; Yp(B) = Yp(A) / 2; Ma(B) = Ma(A) / 2; Mi(B) = Mi(A) / 2;
      Mr(B) = Mr(A) / 2; dtot(B) = D(B) + Ma(B) + Mi(B) + Mr(B);
      receptor_occupied(B) = receptor_occupied(A) / 2;
      volume(A) = 0.5 * volume(A); phase(A) = 1; D(A) = D(A) - D(B);
      Y(A) = Y(A) - Y(B); Yp(A) = Yp(A) - Yp(B); Ma(A) = Ma(A) - Ma(B);
      Mi(A) = Mi(A) - Mi(B); Mr(A) = Mr(A) - Mr(B); dtot(A) = dtot(A) - dtot(B);
      receptor_occupied(A) = receptor_occupied(A) - receptor_occupied(B););
  division.afterFuncB = G_FUNC(if (switching(A) == 0) {
    mating(B) = mating(A);
    switching(B) = 1;
    colorType(B) = colorType(A);
  } else {
    mating(B) = -mating(A);
    switching(B) = 0;
    colorType(B) = -colorType(A);
  });

  // adds cdc2 to the cell, if it is growing
  reaction D_synthesis(as_A(cell));
  D_synthesis.reacPossibleFunc = B_FUNC(dtot(A) / volume(A) < Dtot_int / V_0);
  D_synthesis.rate = 200 / units::minute;
  D_synthesis.afterFuncA = G_FUNC(D(A) = D(A) + 1; dtot(A) = dtot(A) + 1;);

  reaction death(as_A(cell));
  death.reacPossibleFunc = B_FUNC(nr_in_system(cell) > 1);
  death.rate = 0.003 / units::minute;
  death.Adead();

  reaction produce_F_M(as_A(cell));
  produce_F_M.rate = k10;
  produce_F_M.reacPossibleFunc = B_FUNC(mating(A) == -1);
  produce_F_M.createAt_A(F_M);

  reaction produce_F_P(as_A(cell));
  produce_F_P.rate = k10;
  produce_F_P.reacPossibleFunc = B_FUNC(mating(A) == 1);
  produce_F_P.createAt_A(F_P);

  reaction produce_X(as_A(cell));
  produce_X.rate = k10 / 10;
  produce_X.reacPossibleFunc = B_FUNC(mating(A) == -1);
  produce_X.createAt_A(X);

  reaction bind_pherome_M(as_A(cell), as_B(F_M));
  bind_pherome_M.barrier = 0.;
  bind_pherome_M.setOverlapp(0.01);
  bind_pherome_M.reacPossibleFunc =
      B_FUNC(receptor_occupied(A) < receptor_total(A) && mating(A) == 1);
  bind_pherome_M.afterFuncA =
      G_FUNC(receptor_occupied(A) = receptor_occupied(A) + 1;);
  bind_pherome_M.Bdead();

  reaction bind_pherome_P(as_A(cell), as_B(F_P));
  bind_pherome_P.barrier = 0.;
  bind_pherome_P.setOverlapp(0.01);
  bind_pherome_P.reacPossibleFunc =
      B_FUNC(receptor_occupied(A) < receptor_total(A) && mating(A) == -1);
  bind_pherome_P.afterFuncA =
      G_FUNC(receptor_occupied(A) = receptor_occupied(A) + 1;);
  bind_pherome_P.Bdead();

  reaction unbind_pheromone_M(as_A(cell));
  unbind_pheromone_M.rateFunc =
      R_FUNC(2 * 0.02 / units::minute * receptor_occupied(A));
  unbind_pheromone_M.divide(as_B(F_M), 1e-11);
  unbind_pheromone_M.reacPossibleFunc =
      B_FUNC(receptor_occupied(A) > 0 && mating(A) == 1);
  unbind_pheromone_M.afterFuncA =
      G_FUNC(receptor_occupied(A) = receptor_occupied(A) - 1;);

  reaction unbind_pheromone_P(as_A(cell));
  unbind_pheromone_P.rateFunc =
      R_FUNC(2 * 0.02 / units::minute * receptor_occupied(A));
  unbind_pheromone_P.divide(as_B(F_P), 1e-11);
  unbind_pheromone_P.reacPossibleFunc =
      B_FUNC(receptor_occupied(A) > 0 && mating(A) == -1);
  unbind_pheromone_P.afterFuncA =
      G_FUNC(receptor_occupied(A) = receptor_occupied(A) - 1;);

  reaction response(as_A(cell));
  response.reacPossibleFunc = B_FUNC(receptor_occupied(A) > 0 && Mi(A) > 0);
  response.rateFunc =
      R_FUNC(1.5 / units::minute * receptor_occupied(A) * receptor_occupied(A) *
             receptor_occupied(A) /
             (300 * 300 * 300 + receptor_occupied(A) * receptor_occupied(A) *
                                    receptor_occupied(A)) *
             Mi(A) * V_0 * V_0 / (volume(A) * volume(A)));
  response.afterFuncA = G_FUNC(Mi(A) = Mi(A) - 1; Mr(A) = Mr(A) + 1;);

  reaction response_back(as_A(cell));
  response_back.reacPossibleFunc = B_FUNC(Mr(A) > 0);
  response_back.rateFunc = R_FUNC(0.02 / units::minute * Mr(A));
  response_back.afterFuncA = G_FUNC(Mr(A) = Mr(A) - 1; Mi(A) = Mi(A) + 1;);

  reaction degradation_P(as_A(F_P));
  degradation_P.rate = k13;
  degradation_P.Adead();

  reaction degradation_M(as_A(F_M));
  degradation_M.rate = k13;
  degradation_M.Adead();

  reaction degradate_PX(as_A(X), as_B(F_P));
  degradate_PX.barrier = 0.;
  degradate_PX.setOverlapp(0.01);
  degradate_PX.Bdead();

  reaction degradation_X(as_A(X));
  degradation_X.rate = k13;
  degradation_X.Adead();

  Vec begin;
  begin.at(0) = box_size / 2;
  begin.at(1) = box_size / 2;
  begin.at(2) = box_size / 2;

  /* diffusion out of the system for F_P, F_M and X */
  reaction diff_out_F_P(as_A(F_P));
  diff_out_F_P.rate = 0.001;
  diff_out_F_P.reacPossibleFunc = B_FUNC(abs(A.position - begin) > 24000);
  diff_out_F_P.Adead();

  reaction diff_out_F_M(as_A(F_M));
  diff_out_F_M.rate = 0.001;
  diff_out_F_M.reacPossibleFunc = B_FUNC(abs(A.position - begin) > 24000);
  diff_out_F_M.Adead();

  reaction diff_out_X(as_A(X));
  diff_out_X.rate = 0.001;
  diff_out_X.reacPossibleFunc = B_FUNC(abs(A.position - begin) > 24000);
  diff_out_X.Adead();

  /* Adding objects to system */
  auto dummy_1 = putHere(dummy, 0, 0, 0);
  auto box_1 = putHere(Box, box_size / 2, box_size / 2, box_size / 2);
  putInsideAt(box_1, cell, box_size / 2, box_size / 2, box_size / 2);

  /* Adding observations */
  observe::nr_of(F_P);
  observe::nr_of(F_M);
  observe::nr_of(X);
  observe::nr_of(cell, B_FUNC(mating(A) == 1));
  observe::nr_of(cell, B_FUNC(mating(A) == -1));

  /* run the visualization */
  visualize(25);
}
} // namespace model