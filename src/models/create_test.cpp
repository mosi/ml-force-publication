// 3D model
#include "simulator/dsl.h"

using namespace dsl;

namespace model {

void create_test() {
  double box_size = 1100 * units::nano_meter;
  set_temperature(300 * units::kelvin);
  set_viscocity(1E-10 * units::kilogram / units::nano_meter / units::second);
  setSize(box_size);

  /* declaring species and properties */
  ADD_SPECIES(A);
  A.local_density = nature::protein_density;
  radius(A) = 5 * units::nano_meter;
  colorType(A) = 8;

  ADD_SPECIES(Dummy); // dummy partilce for the timestep
  Dummy.local_density = nature::protein_density;
  radius(Dummy) = 5 * units::nano_meter;
  colorType(Dummy) = 1;

  ADD_SPECIES(Box);
  Box.local_density = nature::protein_density;
  radius(Box) = 500 * units::nano_meter;
  colorType(Box) = 2;
  Box.move = false;

  /* declaring Box[] -> Box[A] reaction */
  reaction create(as_A(Box));
  create.rate = 5. * 1E8 / units::second;
  create.createIn_A(A);

  /* inital state */
  putHere(Box, box_size / 2, box_size / 2, box_size / 2);
  putSomewhere(Dummy, 2);

  /* defines output */
  observe::nr_of(A);

  visualize(25);
}
} // namespace model