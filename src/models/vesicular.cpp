// 3D model
#include "simulator/dsl.h"

using namespace dsl;

namespace model {

void vesicular() {
  set_temperature(300 * units::kelvin);
  set_viscocity(4.392E-12 * units::kilogram / units::nano_meter /
                units::second);

  const int amount_X = 2 * 50000;
  const int amount_Y = 2 * 50000;
  const int box_size = 5200 * units::nano_meter;

  ADD_SPECIES(cell);
  radius(cell) = 2500 * units::nano_meter;
  colorType(cell) = 55;
  cell.move = false;
  cell.local_density = nature::protein_density;

  const double r_comp = 250 * units::nano_meter;
  const double v_comp = 3.14 * 4 / 3 * pow(r_comp, 3);

  ADD_SPECIES(comp_1);
  volume(comp_1) = 2 * v_comp * 0.9;
  colorType(comp_1) = 3;
  comp_1.move = false;
  auto ADD_PROPERTY(snare_X, comp_1, int, amount_X * 0.9);
  auto ADD_PROPERTY(snare_Y, comp_1, int, amount_Y * 0.9);
  comp_1.local_density = nature::protein_density;

  ADD_SPECIES(comp_2);
  volume(comp_2) = 2 * v_comp * 0.1;
  colorType(comp_2) = 4;
  comp_2.move = false;
  ADD_PROPERTY(snare_X, comp_2, int, amount_X * 0.1);
  ADD_PROPERTY(snare_Y, comp_2, int, amount_Y * 0.1);
  comp_2.local_density = nature::protein_density;

  const double r_ves = 25 * units::nano_meter;

  ADD_SPECIES(ves_A);
  radius(ves_A) = r_ves;
  colorType(ves_A) = 1;
  ADD_PROPERTY(snare_X, ves_A, int, 0);
  ADD_PROPERTY(snare_Y, ves_A, int, 0);
  ves_A.local_density = nature::protein_density;
  ves_A.diffuse = true;

  ADD_SPECIES(ves_B);
  radius(ves_B) = r_ves;
  colorType(ves_B) = 2;
  ADD_PROPERTY(snare_X, ves_B, int, 0);
  ADD_PROPERTY(snare_Y, ves_B, int, 0);
  ves_B.local_density = nature::protein_density;
  ves_B.diffuse = true;

  ADD_SPECIES(dummy);
  radius(dummy) = r_ves;
  colorType(dummy) = 13;
  dummy.local_density = nature::protein_density;

  Vec pos_1;
  pos_1.at(0) = 0.5 * box_size + 0.25 * 2 * radius(cell);
  pos_1.at(1) = 0.5 * box_size - 0.07 * 2 * radius(cell);
  pos_1.at(2) = 0.5 * box_size;
  Vec pos_2;
  pos_2.at(0) = 0.5 * box_size - 0.25 * 2 * radius(cell);
  pos_2.at(1) = 0.5 * box_size - 0.07 * 2 * radius(cell);
  pos_2.at(2) = 0.5 * box_size;

  /* "dipole" like force to move the vesicle */
  // to generate the results for the pure Brownian motion, remove these forces.

  add_force(
      F_FUNC(1E-21 * ((A.position - pos_1) / (pow(abs(A.position - pos_1), 3)) -
                      (A.position - pos_2) / (pow(abs(A.position - pos_2), 3)))
                         .normalize()),
      B_FUNC(A.type == ves_A.type));
  add_force(F_FUNC(-1E-21 *
                   ((A.position - pos_1) / (pow(abs(A.position - pos_1), 3)) -
                    (A.position - pos_2) / (pow(abs(A.position - pos_2), 3)))
                       .normalize()),
            B_FUNC(A.type == ves_B.type));

  /* budding of new vesicles from compartment 1 or 2 with coat A or B */
  reaction budding_1_A(as_A(comp_1));
  budding_1_A.reacPossibleFunc =
      B_FUNC(volume(A) > 330000 * pow(units::nano_meter, 3));
  budding_1_A.rateFunc =
      R_FUNC(0.0001 * volume(A) / pow(units::nano_meter, 3) / units::second);
  budding_1_A.createAt_A(ves_A);
  budding_1_A.create_atA_func = C_FUNC(
      snare_X(created) = 500 * snare_X(A) / surface(A);
      snare_X(A) -= snare_X(created);
      snare_Y(created) = 500 * snare_Y(A) / surface(A);
      snare_Y(A) -= snare_Y(created); volume(A) = volume(A) - volume(created););

  reaction budding_1_B(as_A(comp_1));
  budding_1_B.reacPossibleFunc =
      B_FUNC(volume(A) > 330000 * pow(units::nano_meter, 3));
  budding_1_B.rateFunc =
      R_FUNC(0.0001 * volume(A) / pow(units::nano_meter, 3) / units::second);
  budding_1_B.createAt_A(ves_B);
  budding_1_B.create_atA_func = C_FUNC(
      snare_X(created) = 50 * snare_X(A) / surface(A);
      snare_X(A) -= snare_X(created);
      snare_Y(created) = 50 * snare_Y(A) / surface(A);
      snare_Y(A) -= snare_Y(created); volume(A) = volume(A) - volume(created););

  reaction budding_2_A(as_A(comp_2));
  budding_2_A.reacPossibleFunc =
      B_FUNC(volume(A) > 330000 * pow(units::nano_meter, 3));
  budding_2_A.rateFunc =
      R_FUNC(0.0001 * volume(A) / pow(units::nano_meter, 3) / units::second);
  budding_2_A.createAt_A(ves_A);
  budding_2_A.create_atA_func = C_FUNC(
      snare_X(created) = 500 * snare_X(A) / surface(A);
      snare_X(A) -= snare_X(created);
      snare_Y(created) = 500 * snare_Y(A) / surface(A);
      snare_Y(A) -= snare_Y(created); volume(A) = volume(A) - volume(created););

  reaction budding_2_B(as_A(comp_2));
  budding_2_B.reacPossibleFunc =
      B_FUNC(volume(A) > 330000 * pow(units::nano_meter, 3));
  budding_2_B.rateFunc =
      R_FUNC(0.0001 * volume(A) / pow(units::nano_meter, 3) / units::second);
  budding_2_B.createAt_A(ves_B);
  budding_2_B.create_atA_func = C_FUNC(
      snare_X(created) = 50 * snare_X(A) / surface(A);
      snare_X(A) -= snare_X(created);
      snare_Y(created) = 50 * snare_Y(A) / surface(A);
      snare_Y(A) -= snare_Y(created); volume(A) = volume(A) - volume(created););

  /* fusion of vesicles with compartment 1 or 2 */
  reaction fusion_1_A(as_A(comp_1), as_B(ves_A));
  fusion_1_A.barrier = 0.;
  fusion_1_A.afterFuncA =
      G_FUNC(volume(A) = volume(A) + volume(B); snare_X(A) += snare_X(B);
             snare_Y(A) += snare_Y(B););
  fusion_1_A.Bdead();

  reaction fusion_2_A(as_A(comp_2), as_B(ves_A));
  fusion_2_A.barrier = 0.;
  fusion_2_A.afterFuncA =
      G_FUNC(volume(A) = volume(A) + volume(B); snare_X(A) += snare_X(B);
             snare_Y(A) += snare_Y(B););
  fusion_2_A.Bdead();

  reaction fusion_1_B(as_A(comp_1), as_B(ves_B));
  fusion_1_B.barrier = 0.;
  fusion_1_B.afterFuncA =
      G_FUNC(volume(A) = volume(A) + volume(B); snare_X(A) += snare_X(B);
             snare_Y(A) += snare_Y(B););
  fusion_1_B.Bdead();

  reaction fusion_2_B(as_A(comp_2), as_B(ves_B));
  fusion_2_B.barrier = 0.;
  fusion_2_B.afterFuncA =
      G_FUNC(volume(A) = volume(A) + volume(B); snare_X(A) += snare_X(B);
             snare_Y(A) += snare_Y(B););
  fusion_2_B.Bdead();

  /* Adding observations */
  // time // 1
  observe::nr_of(ves_A);                  // 2
  observe::nr_of(ves_B);                  // 3
  observe::average_prop(ves_A, snare_X);  // 4
  observe::average_prop(ves_B, snare_X);  // 5
  observe::average_prop(comp_1, snare_X); // 6
  observe::average_prop(comp_2, snare_X); // 7
  observe::average_prop(ves_A, snare_Y);  // 8
  observe::average_prop(ves_B, snare_Y);  // 9
  observe::average_prop(comp_1, snare_Y); // 10
  observe::average_prop(comp_2, snare_Y); // 11
  observe::average_prop(comp_1, volume);  // 12
  observe::average_prop(comp_2, volume);  // 13

  /* setting size of the system */
  setSize(box_size);

  /* Adding objects to system */
  auto cell_1 = putHere(cell, 0.5 * box_size, 0.5 * box_size, 0.5 * box_size);
  putInsideAt(cell_1, comp_1, pos_1.at(0), pos_1.at(1), 0.5 * box_size);
  putInsideAt(cell_1, comp_2, pos_2.at(0), pos_2.at(1), 0.5 * box_size);
  putHere(dummy, 0, 0, 0);
  putHere(dummy, 0, 0, 0);

  /* run the visualization */
  visualize(30);
}
} // namespace model