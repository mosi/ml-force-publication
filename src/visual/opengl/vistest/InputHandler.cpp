#include "InputHandler.hpp"

bool InputHandler::keyStates[256];
bool InputHandler::oldKeyStates[256];

bool InputHandler::specialKeyStates[256];
bool InputHandler::oldSpecialKeyStates[256];

void InputHandler::keyPressed(unsigned char key, int x, int y) {
  keyStates[key] = true;
}

void InputHandler::keyUp(unsigned char key, int x, int y) {
  keyStates[key] = false;
}

bool InputHandler::keyJustPressed(unsigned char key) {
  if (keyStates[key] && !oldKeyStates[key]) {
    return true;
  } else
    return false;
}

bool InputHandler::keyJustUp(unsigned char key) {
  if (!keyStates[key] && oldKeyStates[key]) {
    return true;
  } else
    return false;
}

void InputHandler::UpdateInputHandler() {
  int i;
  for (i = 0; i < 256; i++) {
    oldKeyStates[i] = keyStates[i];
  }

  for (i = 0; i < 256; i++) {
    oldSpecialKeyStates[i] = specialKeyStates[i];
  }
}

void InputHandler::specialKeyPressed(int key, int x, int y) {
  switch (key) {
  case GLUT_KEY_UP:
    specialKeyStates[GLUT_KEY_UP] = true;
    break;
  case GLUT_KEY_DOWN:
    specialKeyStates[GLUT_KEY_DOWN] = true;
    break;
  case GLUT_KEY_LEFT:
    specialKeyStates[GLUT_KEY_LEFT] = true;
    break;
  case GLUT_KEY_RIGHT:
    specialKeyStates[GLUT_KEY_RIGHT] = true;
    break;
  case GLUT_KEY_PAGE_UP:
    specialKeyStates[GLUT_KEY_PAGE_UP] = true;
    break;
  case GLUT_KEY_PAGE_DOWN:
    specialKeyStates[GLUT_KEY_PAGE_DOWN] = true;
    break;
  }
}

void InputHandler::specialKeyUp(int key, int x, int y) {
  switch (key) {
  case GLUT_KEY_UP:
    specialKeyStates[GLUT_KEY_UP] = false;
    break;
  case GLUT_KEY_DOWN:
    specialKeyStates[GLUT_KEY_DOWN] = false;
    break;
  case GLUT_KEY_LEFT:
    specialKeyStates[GLUT_KEY_LEFT] = false;
    break;
  case GLUT_KEY_RIGHT:
    specialKeyStates[GLUT_KEY_RIGHT] = false;
    break;
  case GLUT_KEY_PAGE_UP:
    specialKeyStates[GLUT_KEY_PAGE_UP] = false;
    break;
  case GLUT_KEY_PAGE_DOWN:
    specialKeyStates[GLUT_KEY_PAGE_DOWN] = false;
    break;
  default:
    break;
  }
}
