#ifndef SPHERE
#define SPHERE

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>

//standard libraries
#include <cstdlib>
#include <memory>
#include <algorithm> 
#include <iostream>
#include <string>
#include <vector>

#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

class Sphere
{
	private:
		GLuint vertexArrayObject;
		double radius;
		int degreeStep;
		glm::vec4 color;
		glm::vec3 position;
		
		
		std::vector <glm::vec3> vertices;
		std::vector <glm::vec3> normals;
		std::vector <glm::vec4> colors;
	
	public:
		Sphere();
		Sphere(float, int, glm::vec4, glm::vec3);
		
		inline bool operator==(const Sphere);
		inline bool operator!=(const Sphere);
		
		double distance;
		void SetPosition(glm::vec3);
		void SetupGeometry(GLint);
		void Draw(GLint, GLuint);
		glm::vec3 getPosition();
		static bool sphereComp(Sphere,Sphere);
};
#endif
