#ifndef __INPUTHANDLER_H_INCLUDED__
#define __INPUTHANDLER_H_INCLUDED__

#include <GL/glut.h>

class InputHandler
{
	private:
		
	public:
		static bool keyStates[256];
		static bool oldKeyStates[256];
		
		static bool specialKeyStates[256];
		static bool oldSpecialKeyStates[256];
		
		static void keyPressed(unsigned char key, int x, int y);
  
		static void keyUp(unsigned char key, int x, int y);

		static bool keyJustPressed(unsigned char key);
  
		static bool keyJustUp(unsigned char key);
		
		static void specialKeyPressed(int, int, int);
		
		static void specialKeyUp(int, int, int);

		static void UpdateInputHandler();
};

#endif
