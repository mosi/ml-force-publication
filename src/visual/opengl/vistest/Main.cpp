#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>

// for randomness
#include <stdlib.h>
#include <time.h>

// standard libraries
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <vector>

#include "glm/gtc/matrix_transform.hpp"
#include <glm/glm.hpp>

#include "InputHandler.hpp"

#include "Sphere.hpp"

GLint shaderProgramID = 0;

GLuint modelMatrixID = 0;
GLuint viewMatrixID = 0;
GLuint projectionMatrixID = 0;
GLuint mvpMatrixID = 0;
GLuint ViewerID = 0;
GLuint lightID = 0;

glm::vec3 eye = glm::vec3(0.0, 0.0, 4.0);
glm::vec3 cameraDirection = glm::vec3(0.0, 0.0, -1);

glm::mat4 viewMatrix;
glm::mat4 modelMatrix;
glm::mat4 projectionMatrix;

glm::mat4 modelviewMatrix;

glm::vec3 light = (glm::vec3(3.0, 0.0, 2.0));

std::vector<Sphere> spheres;

const char *vertex_shader =
    "#version 130\n"
    "in vec3 vp;"
    "in vec4 vc;"
    "in vec3 vn;"
    "uniform vec3 viewer;"
    "uniform vec3 light;"
    "out float alpha;"
    "uniform mat4 modelMatrix;"
    "uniform mat4 viewMatrix;"
    "uniform mat4 projectionMatrix;"
    "varying mat4 MVP;"
    "out vec4 fsColor;"
    "varying vec3 viewingDirection;"
    "void main() {"
    "	MVP = projectionMatrix*viewMatrix*modelMatrix;"
    "	gl_Position = MVP *vec4(vp, 1.0);"
    "	viewingDirection = normalize(viewer - vec3( modelMatrix * "
    "vec4(vp, 0.0) ) );"
    "	alpha = 1 - 1.5*abs(dot(viewingDirection, normalize(vn)));"
    "	fsColor = vec4(vc.xyz, alpha);"
    "}";

const char *fragment_shader = "#version 130\n"
                              "in float alpha;"
                              "in vec4 fsColor;"
                              "out vec4 frag_colour;"
                              "void main() {"
                              "	frag_colour = fsColor;"
                              "}";

static void graphics_timer(int id) { glutPostRedisplay(); }

void SetupShader() {
  GLuint vs = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vs, 1, &vertex_shader, NULL);
  glCompileShader(vs);
  GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fs, 1, &fragment_shader, NULL);
  glCompileShader(fs);

  shaderProgramID = glCreateProgram();
  glAttachShader(shaderProgramID, fs);
  glAttachShader(shaderProgramID, vs);
  glLinkProgram(shaderProgramID);
}

void SetupCamera() {
  eye = glm::vec3(0.0, 0.0, 2.0);
  cameraDirection = glm::vec3(0.0, 0.0, +1);

  projectionMatrix =
      glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
  viewMatrix = glm::lookAt(eye, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

  modelviewMatrix = glm::mat4(1.0f);

  modelMatrixID = glGetUniformLocation(shaderProgramID, "modelMatrix");
  viewMatrixID = glGetUniformLocation(shaderProgramID, "viewMatrix");
  projectionMatrixID =
      glGetUniformLocation(shaderProgramID, "projectionMatrix");

  ViewerID = glGetUniformLocation(shaderProgramID, "viewer");
  lightID = glGetUniformLocation(shaderProgramID, "light");

  glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
  glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);
  glUniformMatrix4fv(projectionMatrixID, 1, GL_FALSE, &projectionMatrix[0][0]);
  glUniform3f(ViewerID, eye.x, eye.y, eye.z);
  glUniform3f(lightID, light.x, light.y, light.z);
}

void UpdateCamera() {
  glm::mat4 viewMatrix =
      glm::lookAt(eye, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

  // modelviewMatrix = viewMatrix*modelMatrix;

  glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);
  glUniformMatrix4fv(projectionMatrixID, 1, GL_FALSE, &projectionMatrix[0][0]);

  glUniform3f(ViewerID, eye.x, eye.y, eye.z);
  glUniform3f(lightID, light.x, light.y, light.z);
}

void swapSpheres(Sphere **s1, Sphere **s2) {
  Sphere *tmp;
  tmp = *s1;
  *s1 = *s2;
  *s2 = tmp;
}

bool compareSpheres(Sphere s1, Sphere s2) {
  if (s1.distance > s2.distance) {
    // printf("true\n");
    return true;

  } else {
    // printf("false\n");
    return false;
  }
}

void mydisplay() {

  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  for (std::vector<Sphere>::iterator it = spheres.begin(); it != spheres.end();
       ++it) {
    (*it).distance = glm::distance((*it).getPosition(), eye);
  }

  std::sort(spheres.begin(), spheres.end(), compareSpheres);

  for (std::vector<Sphere>::iterator it = spheres.begin(); it != spheres.end();
       ++it) {
    (*it).Draw(shaderProgramID, modelMatrixID);
  }

  // Camera control
  if (InputHandler::specialKeyStates[GLUT_KEY_DOWN]) {
    glm::mat4 rotation;
    rotation =
        glm::rotate(rotation, glm::radians(1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    eye = glm::vec3(rotation * glm::vec4(eye, 0.0f));

  } else if (InputHandler::specialKeyStates[GLUT_KEY_UP]) {
    glm::mat4 rotation;
    rotation =
        glm::rotate(rotation, glm::radians(-1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    eye = glm::vec3(rotation * glm::vec4(eye, 0.0f));
  }

  if (InputHandler::specialKeyStates[GLUT_KEY_RIGHT]) {
    glm::mat4 rotation;
    rotation =
        glm::rotate(rotation, glm::radians(1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    eye = glm::vec3(rotation * glm::vec4(eye, 0.0f));

  } else if (InputHandler::specialKeyStates[GLUT_KEY_LEFT]) {
    glm::mat4 rotation;
    rotation =
        glm::rotate(rotation, glm::radians(-1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    eye = glm::vec3(rotation * glm::vec4(eye, 0.0f));
  }

  UpdateCamera();
  InputHandler::UpdateInputHandler();

  glutSwapBuffers();
  glutTimerFunc(10, graphics_timer, 0);
  glFlush();
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH | GLUT_ALPHA);
  glutInitWindowSize(800, 600);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("simple");
  glutDisplayFunc(mydisplay);
  glewExperimental = GL_TRUE;
  glewInit();

  glEnable(GL_BLEND);
  glCullFace(GL_FRONT_AND_BACK);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glutKeyboardFunc(InputHandler::keyPressed);
  glutKeyboardUpFunc(InputHandler::keyUp);

  glutSpecialFunc(InputHandler::specialKeyPressed);
  glutSpecialUpFunc(InputHandler::specialKeyUp);

  SetupShader();

  glUseProgram(shaderProgramID);

  SetupCamera();

  srand((unsigned)time(0));

  for (int i = 0; i < 50; i++) {
    spheres.push_back(
        Sphere(0.09 * ((double)rand() / RAND_MAX) + 0.01,
               30, // 120 tetraeder, 90 oktaeder
               glm::vec4(((float)rand() / RAND_MAX), ((float)rand() / RAND_MAX),
                         ((float)rand() / RAND_MAX), 1),
               glm::vec3(2 * (((float)rand() / RAND_MAX) - 0.5),
                         2 * (((float)rand() / RAND_MAX) - 0.5),
                         2 * (((float)rand() / RAND_MAX) - 0.5))));
  }

  for (std::vector<Sphere>::iterator it = spheres.begin(); it != spheres.end();
       ++it) {
    (*it).SetupGeometry(shaderProgramID);
  }

  glutMainLoop();

  glUseProgram(0);
}
