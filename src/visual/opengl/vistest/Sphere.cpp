#include "Sphere.hpp"

Sphere::Sphere() { vertexArrayObject = 0; }

Sphere::Sphere(float radius, int degreeStep, glm::vec4 color, glm::vec3 pos) {
  vertexArrayObject = 0;
  this->radius = radius;
  this->degreeStep = degreeStep;
  this->color = color;
  this->position = pos;
  this->distance = 0;
}

inline bool Sphere::operator==(const Sphere s) {
  if (this->vertexArrayObject == s.vertexArrayObject &&
      this->radius == s.radius && this->degreeStep == s.degreeStep &&
      this->color == s.color && this->position == s.position)
    return true;
  else
    return false;
}

inline bool Sphere::operator!=(const Sphere s) {
  if (this->vertexArrayObject == s.vertexArrayObject &&
      this->radius == s.radius && this->degreeStep == s.degreeStep &&
      this->color == s.color && this->position == s.position)
    return false;
  else
    return true;
}

void Sphere::SetPosition(glm::vec3 pos) {
  position = pos;
};

void Sphere::SetupGeometry(GLint shaderProgramID) {
  glm::vec3 v;
  glm::vec3 n;

  for (int theta = 0; theta < 180; theta += degreeStep) {
    for (int phi = 0; phi <= 360; phi += degreeStep) {
      for (int theta2 = theta; theta2 <= theta + degreeStep;
           theta2 += degreeStep) {
        v = glm::vec3(radius * sin((double)theta2 / 360 * (2 * M_PI)) *
                          cos((double)phi / 360 * (2 * M_PI)),
                      radius * sin((double)theta2 / 360 * (2 * M_PI)) *
                          sin((double)phi / 360 * (2 * M_PI)),
                      radius * cos((double)theta2 / 360 * (2 * M_PI)));
        vertices.push_back(v);
        n = glm::normalize(v); // (sqrt(v[0] * v[0] + v[1] * v[1] + v[2] *
                               // v[2]));
        // printf("vertex: %f,%f,%f\n", v[0],v[1],v[2]);
        normals.push_back(n);
        // color.push_back(vec4(0.5*sin(((double)theta/360)*(2*M_PI)*12)+0.5,
        // 0.5*sin(((double)phi/360)*(2*M_PI)*12)+0.5, 0.0, 1.0));
        colors.push_back(color);
      }
    }
    // printf("theta:%d\n", theta);
  }

  glGenVertexArrays(1, &vertexArrayObject);
  glBindVertexArray(vertexArrayObject);

  GLuint vboPosition = 0;
  glGenBuffers(1, &vboPosition); // printf("Position: %d\n", vboPosition);
  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(),
               vertices.data(), GL_STATIC_DRAW);

  GLint attributePositionIndex = glGetAttribLocation(shaderProgramID, "vp");
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributePositionIndex);

  GLuint vboColor = 0;
  glGenBuffers(1, &vboColor); // printf("Color: %d\n", vboColor);
  glBindBuffer(GL_ARRAY_BUFFER, vboColor);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * colors.size(),
               colors.data(), GL_STATIC_DRAW);

  GLint attributeColorIndex = glGetAttribLocation(shaderProgramID, "vc");
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeColorIndex);

  GLuint vboNormal = 0;
  glGenBuffers(1, &vboNormal); // printf("Normals: %d\n", vboNormal);
  glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * normals.size(),
               normals.data(), GL_STATIC_DRAW);

  GLint attributeNormalIndex = glGetAttribLocation(shaderProgramID, "vn");
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeNormalIndex);

  glBindVertexArray(0);
}

void Sphere::Draw(GLint shaderProgramID, GLuint modelMatrixID) {
  glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), position);
  glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
  glBindVertexArray(vertexArrayObject);
  glDrawArrays(
      GL_TRIANGLE_STRIP, 0,
      2 * (360 / degreeStep + 1) *
          (180 /
           degreeStep)); //				<----------------
  glBindVertexArray(0);
}

glm::vec3 Sphere::getPosition() { return position; }
