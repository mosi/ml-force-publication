#include "visual/visual.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>

// for randomness
#include <stdlib.h>
#include <time.h>

// standard libraries
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <vector>

#include "glm/gtc/matrix_transform.hpp"
#include <glm/glm.hpp>

//#include "InputHandler.hpp"

#include "Camera.h"
#include "InputHandler.h"
#include "Sphere.hpp"
#include "settings.h"
#include "utilities/Utilities.h"

class OpenglSystemView : public systemView {
public:
  OpenglSystemView(int framerateIN = 25);
  ~OpenglSystemView();
  int stepCounter;

  GLFWwindow *window;

  Camera camera;

  void updateWindow(const pSystem &sys);

  GLint shaderProgramID = 0;

  GLuint modelMatrixID = 0;
  GLuint viewMatrixID = 0;
  GLuint projectionMatrixID = 0;
  GLuint mvpMatrixID = 0;
  GLuint ViewerID = 0;
  GLuint lightID = 0;
  GLuint colorID = 0;

  glm::mat4 viewMatrix;
  glm::mat4 modelMatrix;
  glm::mat4 projectionMatrix;

  glm::mat4 modelviewMatrix;

  std::vector<Sphere> spheres;

  int sphereCount;

  const char *vertex_shader =
      "#version 130\n"
      "in vec3 vp;"
      "in vec4 vc;"
      "in vec3 vn;"
      "uniform vec3 color;"
      "uniform vec3 viewer;"
      "uniform vec3 light;"
      "out float alpha;"
      "uniform mat4 modelMatrix;"
      "uniform mat4 viewMatrix;"
      "uniform mat4 projectionMatrix;"
      "varying mat4 MVP;"
      "out vec4 fsColor;"
      "varying vec3 viewingDirection;"
      "void main() {"
      "	MVP = projectionMatrix*viewMatrix*modelMatrix;"
      "	gl_Position = MVP *vec4(vp, 1.0);"
      "	viewingDirection = normalize(viewer - vec3( modelMatrix * "
      "vec4(vp, 1.0) ) );"
      "	alpha = max(0.8 - 1.2*abs(dot(viewingDirection, normalize(vn))), 0);"
      "	fsColor = vec4(color, alpha);"
      "}";

  const char *fragment_shader = "#version 130\n"
                                "in float alpha;"
                                "in vec4 fsColor;"
                                "out vec4 frag_colour;"
                                "void main() {"
                                "	frag_colour = fsColor;"
                                "}";

  // Functions

  // static void graphics_timer(int);

  void SetupShader();

  void SetupMatrices();

  void CameraControl();

  void UpdateMatrices();

  static bool compareSpheres(Sphere, Sphere);
};
