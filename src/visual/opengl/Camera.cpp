#include "Camera.h"

Camera::Camera() {
  position = glm::vec3(0.3, 0.3, 1.5);
  direction = glm::vec3(0.0, 0.0, -1);
  speed = 0.025;

  forward = glm::vec3(0, 0, -1);
  backward = glm::vec3(0, 0, 1);

  left = glm::vec3(-1, 0, 0);
  right = glm::vec3(1, 0, 0);

  up = glm::vec3(0, 1, 0);
  down = glm::vec3(0, -1, 0);
}

glm::vec4 Camera::quaterionCrossProduct(glm::vec4 x, glm::vec4 y) {
  return glm::vec4(x[0] * y[0] - x[1] * y[1] - x[2] * y[2] - x[3] * y[3],
                   x[0] * y[1] + x[1] * y[0] + x[2] * y[3] - x[3] * y[2],
                   x[0] * y[2] - x[1] * y[3] + x[2] * y[0] + x[3] * y[1],
                   x[0] * y[3] + x[1] * y[2] - x[2] * y[1] + x[3] * y[0]);
}

glm::vec3 Camera::quaterionRotation(glm::vec3 point, glm::vec3 axis,
                                    float theta) {
  float a = cos(theta / 2);
  float b = axis.x * sin(theta / 2);
  float c = axis.y * sin(theta / 2);
  float d = axis.z * sin(theta / 2);

  glm::vec4 h = glm::vec4(a, b, c, d);
  glm::vec4 h2 = glm::vec4(a, -b, -c, -d);

  glm::vec4 qpoint, qpoint2;
  qpoint = glm::vec4(0, point[0], point[1], point[2]);
  qpoint2 = quaterionCrossProduct(quaterionCrossProduct(h, qpoint), h2);

  return glm::vec3(qpoint2[1], qpoint2[2], qpoint2[3]);
}

void Camera::Control() {
  float theta = glm::radians(1.0f);

  // Camera Rotation via Mouse

  if (InputHandler::leftMouseButtonState && InputHandler::mouseIsMoving) {
    double mouseX, mouseY;
    glm::vec3 mouseVector;
    float mouseVectorLength;
    float alpha;

    mouseX = InputHandler::mouseDeltaX;
    mouseY = InputHandler::mouseDeltaY;

    mouseVector = glm::vec3(mouseX, mouseY, 0);
    mouseVectorLength = mouseVector.length();

    if (mouseX == 0)
      if (mouseY > 0)
        alpha = M_PI_2;
      else
        alpha = -M_PI_2;
    else if (mouseX < 0)
      if (mouseY < 0)
        alpha = atan(mouseY / mouseX) - M_PI;
      else
        alpha = atan(mouseY / mouseX) + M_PI;
    else
      alpha = atan(mouseY / mouseX);

    glm::vec3 wmv =
        glm::normalize(-up * (float)sin(alpha) + right * (float)cos(alpha));
    glm::vec3 axis = glm::cross(forward, wmv);
    direction = quaterionRotation(
        direction, axis, theta * mouseVectorLength * config::mouseSensitivity);
  }

  InputHandler::mouseIsMoving = false; // room for improvement

  // Camera Rotation via Keyboard
  if (InputHandler::keyState[GLFW_KEY_K]) {
    direction = quaterionRotation(direction, left, theta);

  } else if (InputHandler::keyState[GLFW_KEY_I]) {
    direction = quaterionRotation(direction, right, theta);
  }

  if (InputHandler::keyState[GLFW_KEY_J]) {
    direction = quaterionRotation(direction, up, theta);

  } else if (InputHandler::keyState[GLFW_KEY_L]) {
    direction = quaterionRotation(direction, down, theta);
  }

  // Camera translation
  if (InputHandler::keyState[GLFW_KEY_A]) {
    position += speed * left;
  } else if (InputHandler::keyState[GLFW_KEY_D]) {
    position += speed * right;
  }

  if (InputHandler::keyState[GLFW_KEY_W]) {
    position += speed * up;
  } else if (InputHandler::keyState[GLFW_KEY_S]) {
    position += speed * down;
  }

  if (InputHandler::keyState[GLFW_KEY_Q]) {
    position += speed * forward;
  } else if (InputHandler::keyState[GLFW_KEY_E]) {
    position += speed * backward;
  }
}

void Camera::Update() {
  Control();

  forward = glm::normalize(direction);
  backward = -forward;

  right = glm::normalize(glm::cross(forward, up));
  left = -right;

  up = glm::normalize(glm::cross(forward, left));
  down = -up;
}
