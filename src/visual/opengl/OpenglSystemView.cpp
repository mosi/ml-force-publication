#include "OpenglSystemView.h"

// Initialization of GLFW
OpenglSystemView::OpenglSystemView(int framerateIN) {
  framerate = framerateIN;

  // GLFW-Initialization
  if (!glfwInit())
    std::cout << "Failed to initialize glfw\n";

  /* Create a windowed mode window and its OpenGL context */
  window = glfwCreateWindow(config::outputsize, config::outputsize,
                            "mlforce-visualization", NULL, NULL);
  if (!window) {
    glfwTerminate();
    std::cout << "Failed to create window\n";
  }

  /* Make the window's context current */
  glfwMakeContextCurrent(window);

  // Visualization-Initialization
  glfwSetKeyCallback(window, InputHandler::keyUpdate);
  glfwSetMouseButtonCallback(window, InputHandler::mouseButtonUpdate);
  glfwSetCursorPosCallback(window, InputHandler::cursorPosUpdate);

  glewExperimental = GL_TRUE;
  glewInit();

  glEnable(GL_BLEND);
  glCullFace(GL_FRONT_AND_BACK);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  SetupShader();

  glUseProgram(shaderProgramID);

  Sphere::SetupLowResGeometry(shaderProgramID);
  Sphere::SetupMedResGeometry(shaderProgramID);
  Sphere::SetupHighResGeometry(shaderProgramID);

  camera = Camera();

  SetupMatrices();

  srand((unsigned)time(0));

  stepCounter = 0;
}

// Correctly terminating GLFW after the visualzation ended
OpenglSystemView::~OpenglSystemView() { glfwTerminate(); }

// Visualizationitt
void OpenglSystemView::SetupShader() {
  GLuint vs = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vs, 1, &vertex_shader, NULL);
  glCompileShader(vs);
  GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fs, 1, &fragment_shader, NULL);
  glCompileShader(fs);

  shaderProgramID = glCreateProgram();
  glAttachShader(shaderProgramID, fs);
  glAttachShader(shaderProgramID, vs);
  glLinkProgram(shaderProgramID);
}

void OpenglSystemView::SetupMatrices() {
  camera.position = glm::vec3(0.3, 0.3, 1.5);
  camera.direction = glm::vec3(0.0, 0.0, -1);

  projectionMatrix = glm::perspective(glm::radians(config::aperture),
                                      4.0f / 4.0f, 0.1f, 100.0f);
  viewMatrix = glm::lookAt(camera.position, camera.position + camera.direction,
                           camera.up);

  modelviewMatrix = glm::mat4(1.0f);

  modelMatrixID = glGetUniformLocation(shaderProgramID, "modelMatrix");
  viewMatrixID = glGetUniformLocation(shaderProgramID, "viewMatrix");
  projectionMatrixID =
      glGetUniformLocation(shaderProgramID, "projectionMatrix");

  ViewerID = glGetUniformLocation(shaderProgramID, "viewer");
  colorID = glGetUniformLocation(shaderProgramID, "color");

  glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
  glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);
  glUniformMatrix4fv(projectionMatrixID, 1, GL_FALSE, &projectionMatrix[0][0]);
  glUniform3f(ViewerID, camera.position.x, camera.position.y,
              camera.position.z);
  glUniform3f(colorID, 0.0, 1.0, 1.0);
}

void OpenglSystemView::UpdateMatrices() {
  glm::mat4 viewMatrix = glm::lookAt(
      camera.position, camera.position + camera.direction, camera.up);

  glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);
  glUniformMatrix4fv(projectionMatrixID, 1, GL_FALSE, &projectionMatrix[0][0]);

  glUniform3f(ViewerID, camera.position.x, camera.position.y,
              camera.position.z);
}

bool OpenglSystemView::compareSpheres(Sphere s1, Sphere s2) {
  if (s1.distance > s2.distance) {
    return true;

  } else {
    return false;
  }
}

void OpenglSystemView::updateWindow(const pSystem &sys) {

  if (stepCounter >= config::renderSkipSteps) {

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (const auto &pair : sys.data) {
      const auto id = pair.first;
      const auto ptcl = pair.second;
      float r = ptcl.radius;
      bool found = false;

      // Edit existing Spheres
      for (std::vector<Sphere>::iterator it = spheres.begin();
           it != spheres.end(); ++it) {
        if ((*it).id == id) {

          glm::vec3 vecPos;
          if (DIMENSION == 3) {
            vecPos = glm::vec3(ptcl.position[0] / sys.systemSize[0],
                               ptcl.position[1] / sys.systemSize[1],
                               ptcl.position[2] / sys.systemSize[2]);
          } else {
            vecPos = glm::vec3(ptcl.position[0] / sys.systemSize[0],
                               ptcl.position[1] / sys.systemSize[1], 0);
          }

          (*it).SetPosition(vecPos);
          (*it).SetRadius(r / sys.systemSize[0]);
          found = true;
          break;
        }
      }

      // Adding new Spheres
      if (!found) {
        glm::vec3 vecPos;
        if (DIMENSION == 3) {
          vecPos = glm::vec3(ptcl.position[0] / sys.systemSize[0],
                             ptcl.position[1] / sys.systemSize[1],
                             ptcl.position[2] / sys.systemSize[2]);
        } else {
          vecPos = glm::vec3(ptcl.position[0] / sys.systemSize[0],
                             ptcl.position[1] / sys.systemSize[1], 0);
        }
        spheres.push_back(Sphere(id, ptcl.colortype,
                                 ptcl.radius / sys.systemSize[0],
                                 Sphere::highResSphereVAO, vecPos));
      }
    }

    // Remove non existing spheres
    for (std::vector<Sphere>::iterator it = spheres.begin();
         it != spheres.end(); ++it) {
      bool found = false;

      for (const auto &pair : sys.data) {
        const auto id = pair.first;

        if ((*it).id == id) {
          found = true;
          break;
        }
      }

      if (!found) {
        spheres.erase(it);
      }
    }

    // Counters for amount of spheres rendered in different resolutions
    int highrescount, medrescount, lowrescount = 0;
    // Determing distance to camera
    for (std::vector<Sphere>::iterator it = spheres.begin();
         it != spheres.end(); ++it) {
      (*it).distance = glm::distance((*it).GetPosition(), camera.position);

      // Quality-adjustment
      float projratio =
          (2 * (*it).GetRadius()) /
          (2 * tan(glm::radians(config::aperture) / 2) * (*it).distance);

      if (projratio > 0.1 * config::qualityFactor) {
        (*it).SetVAO(Sphere::highResSphereVAO);
        highrescount++;
      } else if (projratio > 0.01 * config::qualityFactor) {
        (*it).SetVAO(Sphere::medResSphereVAO);
        medrescount++;
      } else {
        (*it).SetVAO(Sphere::lowResSphereVAO);
        lowrescount++;
      }
    }

    // Sort spheres by distance to camera
    std::sort(spheres.begin(), spheres.end(), compareSpheres);

    // Draw spheres
    for (std::vector<Sphere>::iterator it = spheres.begin();
         it != spheres.end(); ++it) {
      (*it).Draw(shaderProgramID, modelMatrixID, colorID);
    }

    camera.Update();

    UpdateMatrices();

    glFlush();

    /* Swap front and back buffers */
    glfwSwapBuffers(window);

    /* Poll for and process events */
    glfwPollEvents();

    stepCounter = 0;
  } else {
    stepCounter++;
  }
}
