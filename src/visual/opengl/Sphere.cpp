#include "Sphere.hpp"
#include <random>

GLuint Sphere::lowResSphereVAO = 1;
GLuint Sphere::medResSphereVAO = 2;
GLuint Sphere::highResSphereVAO = 3;

Sphere::Sphere() {
  id = 0;
  colorseed = 0;
  radius = 0;
  vertexArrayObject = 0;
  position = glm::vec3(0, 0, 0);
}

Sphere::Sphere(int pctlid, int celltype, float radius, GLuint vao,
               glm::vec3 pos) {
  id = pctlid;
  colorseed = celltype;
  vertexArrayObject = vao;
  this->radius = radius;
  this->position = pos;
  this->distance = 0;
  this->color = glm::vec4(1.0, 1.0, 1.0, 1.0);
}

Sphere::Sphere(float radius, int degreeStep, glm::vec4 color, glm::vec3 pos) {
  id = 0;
  colorseed = 0;
  vertexArrayObject = 0;
  this->radius = radius;
  this->degreeStep = degreeStep;
  this->color = color;
  this->position = pos;
  this->distance = 0;
}

inline bool Sphere::operator==(const Sphere s) {
  if (this->id == s.id)
    return true;
  else
    return false;
}

inline bool Sphere::operator!=(const Sphere s) {
  if (this->id != s.id)
    return true;
  else
    return false;
}

void Sphere::SetPosition(glm::vec3 pos) {
  position = pos;
};
void Sphere::SetVAO(GLuint vao) {
  vertexArrayObject = vao;
};

void Sphere::SetupGeometry(GLint shaderProgramID) {
  glm::vec3 v;
  glm::vec3 n;

  for (int theta = 0; theta < 180; theta += degreeStep) {
    for (int phi = 0; phi <= 360; phi += degreeStep) {
      for (int theta2 = theta; theta2 <= theta + degreeStep;
           theta2 += degreeStep) {
        v = glm::vec3(radius * sin((double)theta2 / 360 * (2 * M_PI)) *
                          cos((double)phi / 360 * (2 * M_PI)),
                      radius * sin((double)theta2 / 360 * (2 * M_PI)) *
                          sin((double)phi / 360 * (2 * M_PI)),
                      radius * cos((double)theta2 / 360 * (2 * M_PI)));
        vertices.push_back(v);
        n = glm::normalize(v);
        normals.push_back(n);
        colors.push_back(color);
      }
    }
  }

  glGenVertexArrays(1, &vertexArrayObject);
  glBindVertexArray(vertexArrayObject);

  GLuint vboPosition = 0;
  glGenBuffers(1, &vboPosition); // printf("Position: %d\n", vboPosition);
  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(),
               vertices.data(), GL_STATIC_DRAW);

  GLint attributePositionIndex = glGetAttribLocation(shaderProgramID, "vp");
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributePositionIndex);

  GLuint vboColor = 0;
  glGenBuffers(1, &vboColor); // printf("Color: %d\n", vboColor);
  glBindBuffer(GL_ARRAY_BUFFER, vboColor);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * colors.size(),
               colors.data(), GL_STATIC_DRAW);

  GLint attributeColorIndex = glGetAttribLocation(shaderProgramID, "vc");
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeColorIndex);

  GLuint vboNormal = 0;
  glGenBuffers(1, &vboNormal); // printf("Normals: %d\n", vboNormal);
  glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * normals.size(),
               normals.data(), GL_STATIC_DRAW);

  GLint attributeNormalIndex = glGetAttribLocation(shaderProgramID, "vn");
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeNormalIndex);

  glBindVertexArray(0);
}

void Sphere::SetupLowResGeometry(GLint shaderProgramID) {
  glm::vec3 v;
  glm::vec3 n;

  std::vector<glm::vec3> new_vertices;
  std::vector<glm::vec3> new_normals;
  std::vector<glm::vec4> new_colors;

  int lowDegreeStep = 60;

  for (int theta = 0; theta < 180; theta += lowDegreeStep) {
    for (int phi = 0; phi <= 360; phi += lowDegreeStep) {
      for (int theta2 = theta; theta2 <= theta + lowDegreeStep;
           theta2 += lowDegreeStep) {
        v = glm::vec3(sin((double)theta2 / 360 * (2 * M_PI)) *
                          cos((double)phi / 360 * (2 * M_PI)),
                      sin((double)theta2 / 360 * (2 * M_PI)) *
                          sin((double)phi / 360 * (2 * M_PI)),
                      cos((double)theta2 / 360 * (2 * M_PI)));
        new_vertices.push_back(v);
        n = glm::normalize(v);
        new_normals.push_back(n);
        new_colors.push_back(glm::vec4(1.0, 1.0, 1.0, 1.0));
      }
    }
  }

  glGenVertexArrays(1, &lowResSphereVAO);
  glBindVertexArray(lowResSphereVAO);

  GLuint vboPosition = 0;
  glGenBuffers(1, &vboPosition);
  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * new_vertices.size(),
               new_vertices.data(), GL_STATIC_DRAW);

  GLint attributePositionIndex = glGetAttribLocation(shaderProgramID, "vp");
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributePositionIndex);

  GLuint vboColor = 0;
  glGenBuffers(1, &vboColor);
  glBindBuffer(GL_ARRAY_BUFFER, vboColor);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * new_colors.size(),
               new_colors.data(), GL_STATIC_DRAW);

  GLint attributeColorIndex = glGetAttribLocation(shaderProgramID, "vc");
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeColorIndex);

  GLuint vboNormal = 0;
  glGenBuffers(1, &vboNormal);
  glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * new_normals.size(),
               new_normals.data(), GL_STATIC_DRAW);

  GLint attributeNormalIndex = glGetAttribLocation(shaderProgramID, "vn");
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeNormalIndex);

  glBindVertexArray(0);
}

void Sphere::SetupMedResGeometry(GLint shaderProgramID) {
  glm::vec3 v;
  glm::vec3 n;

  std::vector<glm::vec3> new_vertices;
  std::vector<glm::vec3> new_normals;
  std::vector<glm::vec4> new_colors;

  int medDegreeStep = 30;

  for (int theta = 0; theta < 180; theta += medDegreeStep) {
    for (int phi = 0; phi <= 360; phi += medDegreeStep) {
      for (int theta2 = theta; theta2 <= theta + medDegreeStep;
           theta2 += medDegreeStep) {
        v = glm::vec3(sin((double)theta2 / 360 * (2 * M_PI)) *
                          cos((double)phi / 360 * (2 * M_PI)),
                      sin((double)theta2 / 360 * (2 * M_PI)) *
                          sin((double)phi / 360 * (2 * M_PI)),
                      cos((double)theta2 / 360 * (2 * M_PI)));
        new_vertices.push_back(v);
        n = glm::normalize(v);
        new_normals.push_back(n);
        new_colors.push_back(glm::vec4(1.0, 1.0, 1.0, 1.0));
      }
    }
  }

  glGenVertexArrays(1, &medResSphereVAO);
  glBindVertexArray(medResSphereVAO);

  GLuint vboPosition = 0;
  glGenBuffers(1, &vboPosition);
  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * new_vertices.size(),
               new_vertices.data(), GL_STATIC_DRAW);

  GLint attributePositionIndex = glGetAttribLocation(shaderProgramID, "vp");
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributePositionIndex);

  GLuint vboColor = 0;
  glGenBuffers(1, &vboColor);
  glBindBuffer(GL_ARRAY_BUFFER, vboColor);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * new_colors.size(),
               new_colors.data(), GL_STATIC_DRAW);

  GLint attributeColorIndex = glGetAttribLocation(shaderProgramID, "vc");
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeColorIndex);

  GLuint vboNormal = 0;
  glGenBuffers(1, &vboNormal);
  glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * new_normals.size(),
               new_normals.data(), GL_STATIC_DRAW);

  GLint attributeNormalIndex = glGetAttribLocation(shaderProgramID, "vn");
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeNormalIndex);

  glBindVertexArray(0);
}

void Sphere::SetupHighResGeometry(GLint shaderProgramID) {
  glm::vec3 v;
  glm::vec3 n;

  std::vector<glm::vec3> new_vertices;
  std::vector<glm::vec3> new_normals;
  std::vector<glm::vec4> new_colors;

  int highDegreeStep = 5;

  for (int theta = 0; theta < 180; theta += highDegreeStep) {
    for (int phi = 0; phi <= 360; phi += highDegreeStep) {
      for (int theta2 = theta; theta2 <= theta + highDegreeStep;
           theta2 += highDegreeStep) {
        v = glm::vec3(sin((double)theta2 / 360 * (2 * M_PI)) *
                          cos((double)phi / 360 * (2 * M_PI)),
                      sin((double)theta2 / 360 * (2 * M_PI)) *
                          sin((double)phi / 360 * (2 * M_PI)),
                      cos((double)theta2 / 360 * (2 * M_PI)));
        new_vertices.push_back(v);
        n = glm::normalize(v);
        new_normals.push_back(n);
        new_colors.push_back(glm::vec4(1.0, 1.0, 1.0, 1.0));
      }
    }
  }

  glGenVertexArrays(1, &highResSphereVAO);
  glBindVertexArray(highResSphereVAO);

  GLuint vboPosition = 0;
  glGenBuffers(1, &vboPosition);
  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * new_vertices.size(),
               new_vertices.data(), GL_STATIC_DRAW);

  GLint attributePositionIndex = glGetAttribLocation(shaderProgramID, "vp");
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributePositionIndex);

  GLuint vboColor = 0;
  glGenBuffers(1, &vboColor);
  glBindBuffer(GL_ARRAY_BUFFER, vboColor);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * new_colors.size(),
               new_colors.data(), GL_STATIC_DRAW);

  GLint attributeColorIndex = glGetAttribLocation(shaderProgramID, "vc");
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeColorIndex);

  GLuint vboNormal = 0;
  glGenBuffers(1, &vboNormal);
  glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * new_normals.size(),
               new_normals.data(), GL_STATIC_DRAW);

  GLint attributeNormalIndex = glGetAttribLocation(shaderProgramID, "vn");
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(attributeNormalIndex);

  glBindVertexArray(0);
}

void Sphere::Draw(GLint shaderProgramID, GLuint modelMatrixID, GLuint colorID) {

  glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), position);
  modelMatrix = modelMatrix *
                glm::scale(glm::mat4(1.0f), glm::vec3(radius, radius, radius));
  glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);

  glm::vec3 color2;

  if (colorseed == -1) {
    color2 = Sphere::colorFromSeed(id);
  } else {
    color2 = Sphere::colorFromSeed(colorseed);
  }
  // std::cout << color2.x << ", " << color2.y << ", " << color2.z << "\n";
  glUniform3f(colorID, color2.x, color2.y, color2.z); //<------------

  glBindVertexArray(vertexArrayObject);

  if (vertexArrayObject == Sphere::lowResSphereVAO) {
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 2 * (360 / 60 + 1) * (180 / 60)); //
  } else if (vertexArrayObject == Sphere::medResSphereVAO) {
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 2 * (360 / 30 + 1) * (180 / 30)); //
  } else if (vertexArrayObject == Sphere::highResSphereVAO) {
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 2 * (360 / 5 + 1) * (180 / 5)); //

  } else {
    glDrawArrays(GL_TRIANGLE_STRIP, 0,
                 2 * (360 / degreeStep + 1) * (180 / degreeStep)); //
  }
  glBindVertexArray(0);
}

glm::vec3 Sphere::GetPosition() { return position; }

void Sphere::SetRadius(float r) { radius = r; }

float Sphere::GetRadius() { return radius; }

glm::vec3 Sphere::colorFromSeed(int seed) {
  std::mt19937 generator(seed);
  std::uniform_int_distribution<int> distribution(0, 255);

  auto randColorRed = distribution(generator);
  auto randColorGreen = distribution(generator);
  auto randColorBlue = distribution(generator);

  return glm::vec3((double)randColorRed / 255, (double)randColorGreen / 255,
                   (double)randColorBlue / 255);
  /*
  if(seed == 0)
  {
          return glm::vec3(1.0, 0.0, 0.0);
  }
  else if(seed == 1)
  {
          return glm::vec3(0.0, 1.0, 0.0);
  }
  else if(seed == 2)
  {
          return glm::vec3(0.0, 0.0, 1.0);
  }
  else
  {
          return glm::vec3(1.0, 1.0, 1.0);
  }*/
}
