#include "InputHandler.h"
#include <iostream>

bool InputHandler::keyState[349];
bool InputHandler::oldKeyState[349];

bool InputHandler::leftMouseButtonState;
bool InputHandler::rightMouseButtonState;

double InputHandler::mousePosX = 0;
double InputHandler::mousePosY = 0;

double InputHandler::mouseDeltaX = 0;
double InputHandler::mouseDeltaY = 0;

bool InputHandler::mouseIsMoving = false;

void InputHandler::keyUpdate(GLFWwindow *window, int key, int scancode,
                             int action, int mods) {
  // std::cout << "InputHandler::update called\n";
  for (int i = 0; i < 256; i++) {
    if (key == i && action == GLFW_PRESS) {
      InputHandler::keyState[i] = true;
      // std::cout << key << ":" << glfwGetKeyName(i, 0) << "\n";
    } else if (key == i && action == GLFW_RELEASE) {
      InputHandler::keyState[i] = false;
    }
  }
}

void InputHandler::mouseButtonUpdate(GLFWwindow *window, int button, int action,
                                     int mods) {
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    leftMouseButtonState = true;
  else
    leftMouseButtonState = false;

  if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
    rightMouseButtonState = true;
  else
    rightMouseButtonState = false;
}

void InputHandler::cursorPosUpdate(GLFWwindow *window, double xpos,
                                   double ypos) {
  mouseDeltaX = mousePosX - xpos;
  mouseDeltaY = mousePosY - ypos;

  mousePosX = xpos;
  mousePosY = ypos;

  mouseIsMoving = true;
}
