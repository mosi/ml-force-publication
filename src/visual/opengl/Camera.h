#ifndef CAMERA
#define CAMERA

#include "InputHandler.h"
#include "settings.h"
#include <glm/glm.hpp>
#include <iostream>

class Camera {
public:
  Camera();
  glm::vec3 position;
  glm::vec3 direction;

  glm::vec3 forward, backward, left, right, up, down;

  void Control();
  void Update();

private:
  float speed;
  static glm::vec4 quaterionCrossProduct(glm::vec4, glm::vec4);
  static glm::vec3 quaterionRotation(glm::vec3, glm::vec3, float);
};

#endif
