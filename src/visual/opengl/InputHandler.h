#ifndef INPUTHANDLER
#define INPUTHANDLER

#include <GLFW/glfw3.h>

class InputHandler {
public:
  static bool keyState[349];
  static bool oldKeyState[349];

  static bool leftMouseButtonState;
  static bool rightMouseButtonState;

  static double mousePosX;
  static double mousePosY;

  static double mouseDeltaX;
  static double mouseDeltaY;

  static bool mouseIsMoving;

  static void keyUpdate(GLFWwindow *window, int key, int scancode, int action,
                        int mods);

  static void mouseButtonUpdate(GLFWwindow *window, int button, int action,
                                int mods);

  static void cursorPosUpdate(GLFWwindow *window, double xpos, double ypos);
};

#endif
