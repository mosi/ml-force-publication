#include <iostream>
#include <memory>

#include "visual/cimg/createPlot.h"
#include "visual/file/fileVis.h"

#include "visual/visual.h"

#ifdef VIS_opengl
#include "visual/opengl/OpenglSystemView.h"
#endif

systemView *getView(int framerate) {
  systemView *v;
  bool hasRet = false;
#ifdef VIS_CIMG
  assert(!hasRet);
  v = new cimgSystemView;
  hasRet = true;
#endif

#ifdef VIS_opengl
  assert(!hasRet);
  v = new OpenglSystemView;
  hasRet = true;
#endif

#ifdef VIS_NO
  assert(!hasRet);
  v = new systemView;
  hasRet = true;
#endif
#ifdef VIS_FILE
  assert(!hasRet);
  v = new fileSystemView;
  hasRet = true;
#endif
  if (!hasRet) {
    throw std::runtime_error("Used Visualizer not implemented");
  }
  return v;
}
