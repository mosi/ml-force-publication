#include "simulator/system.h"
#include "utilities/Utilities.h"
#include "utilities/ttimer.h"
#include "visual/visual.h"
#include <fstream>
#include <future>
#include <iostream>
#include <mutex>
#include <random>
#include <string>
#include <vector>

class fileSystemView : public systemView {
public:
  fileSystemView(int framerateIN = 25);
  ~fileSystemView();
  void updateWindow(const pSystem &sys);
  std::vector<std::ofstream *> files;
};
