

//#include "Utilities.h"
#include "settings.h"
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <thread>

#include "visual/file/fileVis.h"

fileSystemView::fileSystemView(int framerateIN) {};
void fileSystemView::updateWindow(const pSystem &sys) {
  if (files.size() < sys.size()) {
    auto oldSize = files.size();
    files.resize(sys.size());
    for (; oldSize < sys.size(); oldSize++) {
      std::string filename;
      filename = "ptcl-" + std::to_string(oldSize) + ".txt";
      files[oldSize] = new std::ofstream;
      files[oldSize]->open(filename);
      *files[oldSize] << "time[s]"
                      << "\tmass[kg]"
                      << "\tposX[m]\tposY[m]";
      if (DIMENSION == 3) {
        *files[oldSize] << "\tposZ[m]";
      }
      *files[oldSize] << "\n";
    }
  }

  for (const auto &ptc : sys.data) {
    auto &ptcl = ptc.second;

    *files[ptcl.id] << sys.time / units::second << "\t"
                    << ptcl.mass / units::kilogram << "\t";
    for (const auto &x : ptcl.position) {
      *files[ptcl.id] << x / units::meter << "\t";
    }
    *files[ptcl.id] << "\n";
  }
}

fileSystemView::~fileSystemView() {
  for (const auto &ptr : files) {
    ptr->close();
  }
}
