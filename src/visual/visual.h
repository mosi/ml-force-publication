
#include "simulator/system.h"
#include <iostream>
#include <memory>

#pragma once

class systemView {
public:
  int framerate = 25;
  virtual void updateWindow(const pSystem &sys) {};
};

systemView *getView(int framerate);
