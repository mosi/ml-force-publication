#include "simulator/system.h"
#include "utilities/Utilities.h"
#include "utilities/ttimer.h"
#include "visual/visual.h"
#include <future>
#include <mutex>
#include <random>
#include <string>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wformat-nonliteral"
#pragma clang diagnostic ignored "-Wfloat-equal"
#pragma clang diagnostic ignored "-Wcast-align"
#pragma clang diagnostic ignored "-Wshadow"
#pragma clang diagnostic ignored "-Wextra-semi"
#pragma clang diagnostic ignored "-Wdeprecated"
#pragma clang diagnostic ignored "-Wunreachable-code"
#pragma clang diagnostic ignored "-Wdocumentation"
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wdouble-promotion"
#pragma clang diagnostic ignored "-Wpadded"
#pragma clang diagnostic ignored "-Wimplicit-fallthrough"
#pragma clang diagnostic ignored "-Wold-style-cast"
#pragma clang diagnostic ignored "-Wweak-vtables"
#pragma clang diagnostic ignored "-Wcomma"
#pragma clang diagnostic ignored "-Wdocumentation-unknown-command"
#pragma clang diagnostic ignored "-Weverything"
#include "CImg.hpp"
#pragma clang diagnostic pop

#pragma once

void createPlot(const pSystem &sys, std::string, size_t nr);
std::future<void> createPlotAsync(const pSystem &sys, std::string, size_t nr);

class cimgSystemView : public systemView {
public:
  ttimer sinceLast;
  cimgSystemView(int framerateIN = 25);
  // int framerate = 25;
  cimg_library::CImgDisplay window;
  int test() { return 1; }
  void updateWindow(const pSystem &sys);
  std::mutex is_drawing;
};

cimg_library::CImg<unsigned char>
createImg(const std::map<ptclid_t, Particle> &data, Vec systemSize);
/*struct systemPlot {
  uint plotSize = 1000;
  unit faktor = 4;
  cimg_library::CImg<unsigned char> img(plotSize *faktor, plotSize *faktor, 1,
3, 255);

  void addParticle(vec pos, size_t id, double outerRadius) ;
  void createFile(std::string filename) ;
}*/
