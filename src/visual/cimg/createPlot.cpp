

#include "createPlot.h"
//#include "Utilities.h"
#include "settings.h"
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <thread>

//#define cimg_use_png
using namespace cimg_library;

void drawRing2(CImg<unsigned char> &img, uint x, uint y, uint maxRad,
               uint minRad, const unsigned char *col) {
  int increment = 1;
  if (config::fastRender) {
    increment = 2;
  }
  for (uint rad = minRad; rad <= maxRad + 1; rad += increment) {
    uint pattern = 0xFFFFFF;
    img.draw_circle(x, y, rad, col, 1., pattern);
  }
}

void do_plot(const std::map<ptclid_t, Particle> &data, std::string filename,
             size_t nr, Vec systemSize);

std::future<void> createPlotAsync(const pSystem &sys, std::string filename,
                                  size_t nr) {
  auto systemSize = sys.systemSize;
  auto data = sys.data;
  std::future<void> plot =
      std::async(std::launch::async, do_plot, data, filename, nr, systemSize);
  return plot;
}

void createPlot(const pSystem &sys, std::string filename, size_t nr = 0) {
  do_plot(sys.data, filename, nr, sys.systemSize);
}

CImg<unsigned char> createImg(const std::map<ptclid_t, Particle> &data,
                              Vec systemSize) {
  uint plotSize = config::outputsize; // resolution
  uint faktor = config::oversampling; // used for a simple anti aliasing

  if (config::fastRender) {
    faktor = 1;
  }
  double conversionFaktor = faktor * plotSize / systemSize.at(0);
  assert(faktor > 0);
  CImg<unsigned char> img(plotSize * faktor, plotSize * faktor, 1, 3, 255);

  std::uniform_int_distribution<int> uniDist(0, 255);
  for (const auto &p : data) {
    const auto &ptcl = p.second;
    unsigned char col[3]; // = { id,id+1,id+2};
    int colorInit = ptcl.colortype;
    if (colorInit == -1) {
      colorInit = ptcl.id;
    }
    for (int idx = 0; idx < 3; idx++) {
      std::mt19937 mt(idx + colorInit);
      col[idx] = uniDist(mt);
    }
    auto drawPos = conversionFaktor * ptcl.position;
    auto drawRad = ptcl.radius * conversionFaktor;
    auto drawInnerRad =
        std::max(0., (drawRad - config::innerRadSub) * config::innerRadFak);

    assert(drawPos[0] >= 0);
    assert(drawPos[0] <= plotSize * faktor);
    assert(drawPos[1] >= 0);
    assert(drawPos[1] <= plotSize * faktor);

    drawRing2(img, drawPos[0], drawPos[1], drawRad, drawInnerRad, col);
    for (int x = 0; x < 2; x++) {
      if (ptcl.position[x] + ptcl.radius > systemSize[x]) {
        auto mirrorPos = drawPos;
        mirrorPos.at(x) -= systemSize.at(x) * conversionFaktor;
        drawRing2(img, mirrorPos[0], mirrorPos[1], drawRad, drawInnerRad, col);
      }
      if (ptcl.position[x] - ptcl.radius < 0) {
        auto mirrorPos = drawPos;
        mirrorPos[x] += systemSize[x] * conversionFaktor;
        drawRing2(img, mirrorPos[0], mirrorPos[1], drawRad, drawInnerRad, col);
      }
    }
  }
  img.resize(plotSize, plotSize, 1, 3, 3);

  return img;
}

void do_plot(const std::map<ptclid_t, Particle> &data, std::string filename,
             size_t nr, Vec systemSize) {

  auto img = createImg(data, systemSize);
  std::stringstream ss;
  ss << std::setw(10) << std::setfill('0') << nr;
  std::string fullname =
      filename + std::string("_") + ss.str() + std::string(".png");

  assert(false);
  img.save(fullname.c_str());
}

std::map<ptclid_t, Particle> latestData;
Vec latestSystemSize;

std::mutex doNewPlot;

// std::atomic<bool> plotDone = false;

void keepUpdatingPlot(cimgSystemView *view) {
  while (true) {
    doNewPlot.lock();
    auto data = latestData;
    auto systemSize = latestSystemSize;
    doNewPlot.unlock();
    view->window.display(createImg(data, systemSize));
  }
}

cimgSystemView::cimgSystemView(int framerateIN) {
  assert(framerateIN >= 1);
  // assert(framerateIN <= 500);
  framerate = framerateIN;
  sinceLast.start();

  if (config::useAsyncPlotting) {
    std::thread(keepUpdatingPlot, this).detach();
  }
}

bool viewHasLock = false;

void cimgSystemView::updateWindow(const pSystem &sys) {
  // std::cout << sinceLast.timePassed() << "\n";
  if (!viewHasLock) {
    if (doNewPlot.try_lock()) {
      viewHasLock = true;
    }
  }
  if (sinceLast.timePassed() > 1. / framerate) {
    sinceLast.restart();

    if (config::useAsyncPlotting) {
      if (doNewPlot.try_lock() || viewHasLock) {
        latestData = sys.data;
        latestSystemSize = sys.systemSize;
        viewHasLock = false;
        doNewPlot.unlock();
      }
    } else { // Not async
      window.display(createImg(sys.data, sys.systemSize));
    }
  }
}
