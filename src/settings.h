// Various settings for the simulation
#include "mapbox/variant.hpp"
#include "utilities/units.h"

#pragma once
namespace config {
// test

/* Rendering */
// resolution of Output
const size_t outputsize = 500;

// Oversampling for antiAliasing (4)
const int oversampling = 1;

// set true for faster rendering (false)
const bool fastRender = false;

// inner radius faktor (default .9)
const double innerRadFak = .9;
// inner radius subtract (default 1)
const double innerRadSub = 1;

const double pushOutFakt = .00;

// how of the particle is there, when it is originally created? (default .2)
const double shrinkFaktor = .99; //.2;

// how fast change in particle size?
const double growthRate = 5;

const int subdivide = 100;
const int ParallelStart = 500; // -1 if you don't want parallism

const bool useAsyncPlotting =
    true; // makes visualization faster for large systems

// only every <renderSkipSteps> updatewindow call will render a new frame in
// opengl visualization
const int renderSkipSteps = 1;

// Aperture in degree of the camera in opengl visualization
const float aperture = 45;

// Multiplier for quality adjustment percents. Smaller is for better quality
const float qualityFactor = 1;

// Sensitivity for rotation via mouse movement
const float mouseSensitivity = 0.5;

const size_t filebuffer = 100;

const bool useAsyncGaus = true;

const size_t nrOfGausToPrepare = 10000000;

const double minAllowedRate = .001;

// finess of the numerical integrator
const double granularity = 150;

// const double ApproxaddVar = 250 * units::nano_meter;
} // namespace config

typedef mapbox::util::variant<int, double> boxVariant;
