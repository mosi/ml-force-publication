

#include "simulator/systemSetup.h"
#include "utilities/ttimer.h"
#include <future>

#include "collision/partitionCollision.h"

/**
 * @brief The collisionPartition Object holds a set of particles (a partition)
 * and can calculated the collisions among them (possibly relying on further
 * partitions). This is a divied an conquere approach to the collision finding
 * problem.
 *
 */
struct collisionPartition {
  /**
   * @brief The Particles that are actually in the Cell
   */
  std::vector<s_particle> in;
  /**
   * @brief The Particle adjacent to the Cell, such that they parttially
   * overlapp, but there center is on the outside
   *
   */
  std::vector<s_particle> adj;

  /* "bottom left" corner of the cell */
  Vec startPunkt;

  /* "top right" corner of the cell */
  Vec endPunkt;

  /* the id of the dimension to partition */
  int dimToPartition = 0;

  /* the length of the periodec boundarys */
  flt_t globalSize;

  /**
   * @brief Add new particles to this partition Object. The object will
   * determine on its own, if these particles are actually included.
   *
   * @param p p_p: the particle to add
   * @param onlyAsAdj p_onlyAsAdj: add the particle only as adjacent not as
   * included
   * @return bool this is true if it was added, and false if not
   */
  bool addToPartion(const s_particle &p, const bool onlyAsAdj = false) {
    int &i = dimToPartition;
    const auto &pos = p.pos[i];

    // bool isIn = false;

    // check if truly in:
    if ((!onlyAsAdj) && (pos >= startPunkt[i]) && (pos < endPunkt[i])) {

      if (!(p.pos[0] >= startPunkt[0]) || !(p.pos[1] >= startPunkt[1]) ||
          !(p.pos[0] < endPunkt[0]) || !(p.pos[1] < endPunkt[1])) {
        throw std::runtime_error("Particle was added to Partition, that is "
                                 "outside the scope of possible particles to "
                                 "be added");
      }
      in.push_back(p);
      return true;
    } else {
      /* now that the particle is not truly in, we check if it overlapps on all
       * dimensions. If so it is added as adjacent.*/
      bool putAdj[DIMENSION];
      for (auto &val : putAdj) {
        val = false;
      }
      for (int idx = 0; idx < DIMENSION; idx++) {
        flt_t x = p.pos[idx];
        flt_t r = p.radius;
        flt_t start = startPunkt[idx];
        flt_t end = endPunkt[idx];
        bool &putThis = putAdj[idx];

        if (x <= start) {
          if (x + r >= start) {
            putThis = true;
          }
          if (x - r < 0) {
            if (globalSize - end < -(x - r)) {
              putThis = true;
            }
          }
        }
        if (x >= end) {
          if (x - r < end) {
            putThis = true;
          }
          if (x + r - globalSize > start) {
            putThis = true;
          }
        }
      }

      // bool allTrue = true;
      for (auto &val : putAdj) {
        if (!val) {
          //  allTrue = false;
        }
      }
      // if(allTrue){
      if (putAdj[0] || putAdj[1]) {
        adj.push_back(p);
      }
    }
    return false;
  }

  std::vector<std::pair<ptclid_t, ptclid_t>> getCollisions() {
    if (in.size() < config::subdivide) {
      auto CollisionsFound = collisionNaive(in, 0., globalSize);
      // std::cout << "There are " << in.size() << "inside and " << adj.size()
      //        << " adj\n";
      for (const auto &p : adj) {
        for (const auto &p2 : in) {
          if (areIntersecting(p, p2, globalSize)) {
            CollisionsFound.push_back(std::make_pair(p.id, p2.id));
          }
        }
      }
      return CollisionsFound;
    } else { // To big for one Pass

      dimToPartition++;
      dimToPartition %= DIMENSION;

      Vec diff;
      diff.init();
      diff[dimToPartition] =
          (endPunkt[dimToPartition] - startPunkt[dimToPartition]) / 2.;

      collisionPartition left;
      left.globalSize = globalSize;
      left.startPunkt = startPunkt;
      left.endPunkt = endPunkt - diff;
      left.dimToPartition = dimToPartition;

      collisionPartition right;
      right.globalSize = globalSize;
      right.startPunkt = startPunkt + diff;
      right.endPunkt = endPunkt;
      right.dimToPartition = dimToPartition;

      assert(startPunkt[dimToPartition] < endPunkt[dimToPartition]);
      assert(abs(diff) > 0);

      for (auto &p : in) {
        if (!(p.pos[dimToPartition] >= startPunkt[dimToPartition]) &&
            (p.pos[dimToPartition] < endPunkt[dimToPartition])) {
          std::cerr << "ERROR: Partition Collision detection has a foulty "
                       "particle (id: " << p.id << ", size=" << p.radius
                    << ", pos=" << p.pos << ") when partitionioning "
                    << dimToPartition << "\n";
          throw std::runtime_error("foult in subDiv");
        }
        bool isRight = right.addToPartion(p);
        bool isLeft = left.addToPartion(p);
        if (!isRight && !isLeft) {
          std::cerr << "not contained: " << p.pos << "\n";
          std::cerr << p.pos[left.dimToPartition]
                    << ">=" << left.startPunkt[left.dimToPartition] << "\n";
          std::cerr << p.pos[left.dimToPartition] << "<"
                    << left.endPunkt[left.dimToPartition] << "\n";
          std::cerr << p.pos[right.dimToPartition]
                    << ">=" << right.startPunkt[right.dimToPartition] << "\n";
          std::cerr << p.pos[right.dimToPartition] << "<"
                    << right.endPunkt[right.dimToPartition] << "\n";
          throw std::runtime_error("Particle is not containd at all");
        }
        if (isRight && isLeft) {
          std::cout << "both contained: " << p.pos << "\n";
          std::cout << p.pos[left.dimToPartition]
                    << ">=" << left.startPunkt[left.dimToPartition] << "\n";
          std::cout << p.pos[left.dimToPartition] << "<"
                    << left.endPunkt[left.dimToPartition] << "\n";
          std::cout << p.pos[right.dimToPartition]
                    << ">=" << right.startPunkt[right.dimToPartition] << "\n";
          std::cout << p.pos[right.dimToPartition] << "<"
                    << right.endPunkt[right.dimToPartition] << "\n";
          throw std::runtime_error("Particle can not be containd in both\n");
        }
      }
      for (auto &p : adj) {
        right.addToPartion(p, true);
        left.addToPartion(p, true);
      }
      assert(left.in.size() + right.in.size() == in.size());

      if (in.size() < config::ParallelStart && config::ParallelStart > 0) {
        auto resultLeft = left.getCollisions();
        for (auto &p : right.getCollisions()) {
          resultLeft.push_back(p);
        }
        return resultLeft;
      } else { // use prallism
        auto resultLeft = std::async(std::launch::async,
                                     [&]() { return left.getCollisions(); });
        auto resultRight = right.getCollisions();
        for (const auto &p : resultLeft.get()) {
          resultRight.push_back(p);
        }
        return resultRight;
      }
    }
  }
};

std::vector<std::pair<ptclid_t, ptclid_t>>
findCollisions_subdivide(const std::vector<s_particle> &data, flt_t size) {

  ttimer tSub;
  tSub.start();

  collisionPartition part;
  part.startPunkt.init();
  part.endPunkt.init(size);
  part.globalSize = size;
  part.in = data;

  auto resPartition = part.getCollisions();
  // assert(noDublicates(resPartition));

  removeDublicatesAndNormalize(resPartition);

  tSub.stop();

  return resPartition;
}
