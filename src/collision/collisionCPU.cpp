#include "collisionCPU.h"
#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include <future>
#include <mutex>
#include <thread>
#include <utility>
#include <vector>
//#include <omp.h>

std::vector<std::pair<ptclid_t, ptclid_t>>
FRNN_cpu_part(const std::vector<s_particle> &data, const flt_t &rad,
              const flt_t &size, const ptclid_t start, const ptclid_t end,
              int depth) {

  assert(end > start);

  assert(false); // not maintained (periodic boundary)
  // case system to small for parallism
  // does not work great for very large systems (>100000), as the number of
  // created calls
  // is limited and work load is asymetric -> waiting
  if ((start + 1 == end) || ((end - start) * (data.size() - start) < 1000000) ||
      (std::pow(2., depth + 1) >
       static_cast<flt_t>(std::thread::hardware_concurrency()))) {
    std::vector<std::pair<ptclid_t, ptclid_t>> result;
    for (ptclid_t idx = start; idx != end; idx++) {
      for (ptclid_t idx2 = idx + 1; idx2 != data.size(); idx2++) {
        if (areIntersectingNonPer(data[idx], data[idx2])) {
          result.push_back(std::make_pair(data[idx].id, data[idx2].id));
        }
      }
    }
    return result;
  }

  // case system large enough to be split
  ptclid_t middle = (end + start) / 2;
  auto res_fut = std::async(std::launch::async, FRNN_cpu_part, data, rad, size,
                            middle, end, depth + 1);
  auto res = FRNN_cpu_part(data, rad, size, start, middle, depth + 1);

  auto res2 = res_fut.get();
  res.reserve(res.size() + res2.size());
  for (auto &x : res2) {
    res.push_back(x);
  }
  return res;
}

std::vector<std::pair<ptclid_t, ptclid_t>>
FRNN_cpu_fut(const std::vector<s_particle> &data, flt_t rad, flt_t size) {
  return FRNN_cpu_part(data, rad, size, 0, data.size(), 0);
}

std::vector<std::pair<ptclid_t, ptclid_t>>
collisionNaive(const std::vector<s_particle> &data, flt_t rad, flt_t size) {
  Vec systemSize;
  for (auto &x : systemSize)
    x = size;

  std::vector<std::pair<ptclid_t, ptclid_t>> result;
  for (auto iter = data.cbegin(); iter != data.end(); iter++) {
    for (auto inner_iter = iter + 1; inner_iter != data.cend(); inner_iter++)
      if (areIntersecting(*iter, *inner_iter, systemSize)) {
        result.push_back(std::make_pair(iter->id, inner_iter->id));
      }
  }
  return result;
}

std::vector<std::pair<ptclid_t, ptclid_t>>
FRNN_cpu_omp(const std::vector<s_particle> &data, flt_t rad, flt_t size) {
  Vec systemSize;
  for (auto &x : systemSize)
    x = size;

  //  no parillism for small system
  if (data.size() < 500) {
    return (collisionNaive(data, rad, size));
  }

  std::vector<std::pair<ptclid_t, ptclid_t>> result;
  std::mutex writeToResult;
  ptclid_t inclusionSum = 0;

#pragma omp parallel for
  for (ptclid_t idx = 0; idx < data.size(); idx++) {
    std::vector<std::pair<ptclid_t, ptclid_t>> interrim_result;
    ptclid_t inclusionPart = 0;
    auto iter = data.cbegin() + idx;
    for (auto inner_iter = iter + 1; inner_iter != data.cend(); inner_iter++) {
      if (inner_iter->radius + iter->radius <
          abs(inner_iter->pos - iter->pos)) {
        inclusionPart++;
      }
      if (areIntersecting(*iter, *inner_iter, systemSize)) {
        interrim_result.push_back(std::make_pair(iter->id, inner_iter->id));
      }
    }
    writeToResult.lock();
    inclusionSum += inclusionPart;
    for (const auto &x : interrim_result) {
      result.push_back(x);
    }
    writeToResult.unlock();
  }
  /*std::cout << " improvement in system " << inclusionSum << "/" <<
     result.size()
            << " =  " << 1. * inclusionSum / result.size() << "\n";*/
  return result;
}

std::vector<std::pair<ptclid_t, ptclid_t>>
cpu_omp(const std::vector<s_particle> &data, Vec size,
        std::function<bool(const s_particle &, const s_particle &, const Vec &)>
            func) {

  std::vector<std::pair<ptclid_t, ptclid_t>> result;
  std::mutex writeToResult;
  ptclid_t inclusionSum = 0;

#pragma omp parallel for
  for (ptclid_t idx = 0; idx < data.size(); idx++) {
    std::vector<std::pair<ptclid_t, ptclid_t>> interrim_result;
    ptclid_t inclusionPart = 0;
    auto iter = data.cbegin() + idx;
    for (auto inner_iter = iter + 1; inner_iter != data.cend(); inner_iter++) {
      if (func(*iter, *inner_iter, size)) {
        interrim_result.push_back(std::make_pair(iter->id, inner_iter->id));
      }
    }
    writeToResult.lock();
    inclusionSum += inclusionPart;
    for (const auto &x : interrim_result) {
      result.push_back(x);
    }
    writeToResult.unlock();
  }
  return result;
}
