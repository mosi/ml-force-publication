#include "collisionCPU.h"
//#include "collisionGPU2.h"
#include "collision/GPU/collisionGPU.h"
#include "collision/GPU/collisionGPU2.h"
#include "collision/partitionCollision.h"
#include "simulator/particle.h"
#include "simulator/s_particle.h"
#include "simulator/system.h"
#include "simulator/systemSetup.h"
#include <vector>

#include "gpuCon.h"

/* GPU conector */

s_particle convertToS(const Particle &p) {
  s_particle s;
  s.id = p.id;
  s.pos[0] = p.position[0];
  s.pos[1] = p.position[1];
  if (s.pos.dim() == 3) {
    s.pos[2] = p.position[2];
  }
  // s.pos = p.position;
  // TODO parameter for oversizing
  s.radius = (1 + 3. / config::granularity) * p.radius + .1;
  s.inner_radius = p.radius * .8;
  return s;
}

std::vector<std::pair<ptclid_t, ptclid_t>> getCollisions(const pSystem &sys) {
  std::vector<s_particle> dat;
  dat.reserve(sys.size());
  for (const auto &x : sys.data) {
    dat.push_back(convertToS(x.second));
  }

// GPU NOT POSSIPLE (NONCONSEQUTIVE IDS)
// auto ret = detectSmart_GPU(dat, sys.systemSize[0]);

// Checking against known result
/*auto check = collisionNaive(dat, 0, sys.systemSize[0]);
assert(noDublicates(ret));
assert(contains(ret, check));
assert(equivalent(ret, check));*/

// return ret;
#ifdef COL_OMP
  return FRNN_cpu_omp(dat, 0, sys.systemSize[0]);
#endif

#ifdef COL_NAIVE
  return collisionNaive(dat, 0, sys.systemSize[0]);
#endif

#ifdef COL_SUBDIVIDE
  return findCollisions_subdivide(dat, sys.systemSize[0]);
#endif

#ifdef COL_GPU_NAIVE
  return FRNN_gpu(dat, 0, sys.systemSize[0]);
#endif

#ifdef COL_GPU_GHSX
  return detectSmart_GPU(dat, 0, sys.systemSize[0]);
#endif

  throw std::runtime_error("No collision Engine was set!");
  return collisionNaive(dat, 0, sys.systemSize[0]);
}
