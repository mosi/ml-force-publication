
#include "collision/collisionCPU.h"
#include "grid.h"
#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include "utilities/ttimer.h"
#include <algorithm>
#include <cassert>
#include <cuda.h>
#include <cudaProfiler.h>
#include <cuda_profiler_api.h>
#include <thrust/copy.h>
#include <thrust/device_free.h>
#include <thrust/device_malloc.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <thrust/for_each.h>
#include <thrust/functional.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/partition.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/tuple.h>
#include <utility>
#include <vector>

#include "gpu_setupBig.h"
#include "gpu_setupSmall.h"

using thrust::device_vector;

//#define USE_TIMING

void setupBigParticles(s_particleInVecIter firstBig,
                       s_particleInVecIter lastBig,
                       device_vector<cellid_t> &bigGridCellIds,
                       device_vector<cellid_t> &bigGridParticleNr, const grid g,
                       size_t &nrOfBigBoxes) {
  ptclid_t nr_of_big_particles = lastBig - firstBig;
  auto seperator = firstBig;
  s_particle *d_seperator = thrust::raw_pointer_cast(&(*firstBig));
  thrust::counting_iterator<cellid_t> simpleCount(0);
  /* This might even out the workload of neighboring particels, but it is as of
   * now untested*/
  /*thrust::sort(seperator, raw_data.end(), [] __device__(const s_particle & a,
  const s_particle &
  b)
  {
    return a.radius > b.radius;
  });*/

  /* compute the subboxes needed for each large particle and store those in
   big_max_box_per_particle
   There is clear room for improvement here. If one could find better lower
   bound for the max
   number of boxes needed */
  thrust::device_vector<cellid_t> big_max_box_per_particle(nr_of_big_particles +
                                                           1);
  auto big_and_count_zip = thrust::make_zip_iterator(
      thrust::make_tuple(seperator, big_max_box_per_particle.begin()));
  thrust::for_each(
      big_and_count_zip, big_and_count_zip + nr_of_big_particles,
      [=] __device__(thrust::tuple<const s_particle &, cellid_t &> tpl) {
        cellid_t boxReach = ceil(thrust::get<0>(tpl).radius / g.gridWidth) + 2;
        boxReach = 2 * boxReach + 1;
        // Terrible but correct upper bound
        if (g.dim == 3) {
          thrust::get<1>(tpl) = boxReach * boxReach * boxReach;
        } else if (g.dim == 2) {
          thrust::get<1>(tpl) = boxReach * boxReach;
        }
        if (use_periodic)
          assert(g.boxsPerAxis > boxReach + 1); // particle may not be too large
                                                // it wraps around
      });

  /* now create an indexing structure for these boxes. For each big particle we
   * now have a set of
   * subboxes*/
  thrust::exclusive_scan(big_max_box_per_particle.begin(),
                         big_max_box_per_particle.end(),
                         big_max_box_per_particle.begin());
  size_t bigSubBoxes = big_max_box_per_particle.back();

  assert(bigSubBoxes < cellid_t_max);

  /*bigGridCellIds stores the id of the Grid cell, that the subcell coresponds
   * to*/
  bigGridCellIds.resize(bigSubBoxes);
  cellid_t *d_bigGridCellIds = thrust::raw_pointer_cast(bigGridCellIds.data());
  /*bigGridCellIds stores the idx of the big particle for each subbox */
  bigGridParticleNr.resize(big_max_box_per_particle.back());
  cellid_t *d_bigGridParticleNr =
      thrust::raw_pointer_cast(bigGridParticleNr.data());

#ifdef USE_TIMING
  cudaDeviceSynchronize();
  setupBigDiv.start();
#endif
  /*Here the bigGrid* structures are filled. At the moment, this is very
   * inefficent, due to vastly
   * variieng particle sizes and therefore variieng workloads per therad*/
  auto BigStartEndZip = thrust::make_zip_iterator(
      thrust::make_tuple(big_max_box_per_particle.begin(),
                         big_max_box_per_particle.begin() + 1, simpleCount));
  thrust::for_each(BigStartEndZip, BigStartEndZip + nr_of_big_particles,
                   [=] __device__(const thrust::tuple<
                       const cellid_t &, const cellid_t &, const cellid_t>
                                      tpl) {
    for (cellid_t idx = thrust::get<0>(tpl); idx != thrust::get<1>(tpl);
         idx++) {
      d_bigGridParticleNr[idx] = thrust::get<2>(tpl);
      d_bigGridCellIds[idx] = idx - thrust::get<0>(tpl);
    }
  });

#ifdef USE_TIMING
  cudaDeviceSynchronize();
  setupBigDiv.stop();
#endif

  // std::cout << " # have a large gridof " << bigGridCellIds.size() << "\n";

  /* Now this overly large grid is reduced to only that part, that is needed
   * (covered by the large
   * particle)*/
  auto nrAndId_zip_start = thrust::make_zip_iterator(
      thrust::make_tuple(bigGridParticleNr.begin(), bigGridCellIds.begin()));
  auto nrAndId_zip_end = thrust::make_zip_iterator(
      thrust::make_tuple(bigGridParticleNr.end(), bigGridCellIds.end()));
#ifdef USE_TIMING
  cudaDeviceSynchronize();
  setupBigAsses.start();
#endif
  thrust::for_each(
      nrAndId_zip_start, nrAndId_zip_end,
      [=] __device__(thrust::tuple<const cellid_t &, cellid_t &> tpl) {
        /*compute the relative offsets dx etc to the origin cell*/
        const auto &p = d_seperator[thrust::get<0>(tpl)];
        cellid_t boxReach = ceil(p.radius / g.gridWidth) + 2;
        boxReach = boxReach * 2 + 1;
        assert(thrust::get<1>(tpl) < boxReach * boxReach * boxReach);
        cellid_t &nr = thrust::get<1>(tpl);
        int dx = nr % (boxReach);
        nr -= dx;
        nr /= boxReach;
        int dy = nr % (boxReach);
        nr -= dy;
        nr /= boxReach;
        int dz = nr;

        if (g.dim == 2) {
          assert(dz == 0);
        }

        boxReach--;
        dx -= boxReach / 2;
        dy -= boxReach / 2;
        if (g.dim == 3) {
          dz -= boxReach / 2;
        }

        /* compute global cell index and if cell is still in system*/
        auto calcBox = g.isIn(g.mvec2idx(p.pos), dx, dy, dz);

        /* check if covered by big particle */
        bool relevant = false;
        const flt_t consideredReach =
            (dx * dx + dy * dy + dz * dz) * g.gridWidth * g.gridWidth;
        if ((consideredReach < pow(p.radius + g.gridWidth * 2, 2)) &&
            ((p.inner_radius <= g.gridWidth * 2) ||
             (consideredReach > pow(p.inner_radius - g.gridWidth * 2, 2)))) {
          relevant = true;
        }
        // Save the cell number. The cellid_tMax ist used do mark uneccecary
        // cells
        if (calcBox.first && relevant) {
          nr = calcBox.second;
        } else {
          nr = cellid_t_max;
        }
      });

  /* remove all uneccecary cells (not needed actually covered but
   * overestimated)*/
  auto nrAndId_newEnd = thrust::remove_if(
      nrAndId_zip_start, nrAndId_zip_end,
      [=] __device__(thrust::tuple<const cellid_t &, const cellid_t &> tpl) {
        return thrust::get<1>(tpl) == UINT_MAX;
      });
  nrOfBigBoxes = nrAndId_newEnd - nrAndId_zip_start;
  // std::cout << "#Minimized to " << nrOfBigBoxes << "\n";
}
