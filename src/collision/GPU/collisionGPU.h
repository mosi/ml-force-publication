#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include <utility>
#include <vector>

std::vector<std::pair<ptclid_t, ptclid_t>>
FRNN_gpu(const std::vector<s_particle> &, flt_t rad, flt_t size);
