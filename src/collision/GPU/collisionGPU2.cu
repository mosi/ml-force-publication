#include "collision/collisionCPU.h"
#include "grid.h"
#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include "utilities/ttimer.h"
#include <algorithm>
#include <cassert>
#include <cuda.h>
#include <cudaProfiler.h>
#include <cuda_profiler_api.h>
#include <thrust/copy.h>
#include <thrust/device_free.h>
#include <thrust/device_malloc.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <thrust/for_each.h>
#include <thrust/functional.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/partition.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/tuple.h>
#include <utility>
#include <vector>

#include "collisionGPU2.h"
#include "gpu_setupBig.h"
#include "gpu_setupSmall.h"

using thrust::device_vector;

//#define USE_TIMING

/* A smart implementation of the increasing grid Size hollow collision detection
 * Algorithm */
std::vector<std::pair<ptclid_t, ptclid_t>>
detectSmart_GPU(const std::vector<s_particle> &data, flt_t size,
                flt_t gridSize_in, size_t *nrOfRaw) {
  // A set of timers used accross functions
  ttimer totalTime;
  ttimer setup;
  ttimer setupSmall;
  ttimer small_small;
  ttimer ssMemoryPrep;
  ttimer ssMemoryPost;
  ttimer setupBig;
  ttimer setupBigloop1;
  ttimer setupBigloop2;
  ttimer setupBigDiv;
  ttimer setupBigAsses;
  ttimer small_big;
  ttimer dataBack;

  thrust::counting_iterator<cellid_t> simpleCount(0);
  // check if data is valid
  for (ptclid_t idx = 0; idx < data.size(); idx++) {
    assert(data[idx].id == idx);
    for (const auto &x : data[idx].pos) {
      assert(x >= 0);
      assert(x < size);
    }
  }

  if (gridSize_in < 0) {
    gridSize_in = size / 1000000;
  }
  assert(size > 0);

#ifdef USE_TIMING
  cudaDeviceSynchronize();
  totalTime.start();
  setup.start();
#endif
  /////////////
  ////  One time pre processing
  ////////
  /*Allocate space for raw data and copy it*/
  thrust::device_vector<s_particle> raw_data(data);
  const auto conRawData(raw_data);
  const s_particle *d_conRawData = thrust::raw_pointer_cast(conRawData.data());

  /* grid Size init is determeent by the smallest particle. Notably this will be
   * rounded to the
   * nearest resonable grid Size, once the Grid will be created */
  flt_t gridSize = std::max(
      2.1f * thrust::transform_reduce(
                 raw_data.begin(), raw_data.end(),
                 [] __device__(const s_particle & a) { return a.radius; },
                 size / 10.f, thrust::minimum<flt_t>()),
      gridSize_in);

  // std::cout << "#choosen an init grid Size of " << gridSize << "/" << size <<
  // "\n";

  auto startIter = raw_data.begin();

  /*The place where the results will be stored on the GPU*/
  thrust::device_vector<thrust::tuple<ptclid_t, ptclid_t>> d_res_pruned;

  auto prunedEnd = d_res_pruned.end();
  assert(prunedEnd == d_res_pruned.begin());
#ifdef USE_TIMING
  cudaDeviceSynchronize();
  setup.stop();
#endif
  bool first = true;
  do {
//////////
//// setup for this step
//////////
#ifdef USE_TIMING
    ttimer thisStepCounter;
    thisStepCounter.start();

    cudaDeviceSynchronize();
    setup.start();
#endif
    /* Create GridLookupFunctions*/
    grid g1;
    g1.dim = DIMENSION;
    g1.boxsPerAxis =
        std::max(std::floor(size / gridSize), static_cast<flt_t>(1.));
    g1.axislength = size;
    g1.gridWidth = g1.axislength / g1.boxsPerAxis;
    gridSize = g1.gridWidth;

    const grid g(g1);

    /* The result of this step*/
    thrust::device_vector<thrust::tuple<ptclid_t, ptclid_t>> d_res;

    /* seperate Small and Big particels*/
    auto seperator = thrust::partition(startIter, raw_data.end(),
                                       [=] __device__(const s_particle & p) {
      return p.radius * 2 <= g.gridWidth;
    });

    /* Calculate the raw pointer for the begin of small dataset*/
    s_particle *d_rawData = thrust::raw_pointer_cast(
        raw_data.data() + (startIter - raw_data.begin()));

    ptclid_t nr_of_small_particles = seperator - startIter;
    ptclid_t nr_of_big_particles = raw_data.end() - seperator;
    s_particle *d_seperator = d_rawData + nr_of_small_particles;

// std::cout << "#found " << nr_of_small_particles << " small and " <<
// raw_data.end() - seperator
//<< " big particels in this step with divider " << g.gridWidth << "\n";

#ifdef USE_TIMING
    cudaDeviceSynchronize();
    setup.stop();

    ////////////////////
    // SETUP small
    ///////////////////
    setupSmall.start();
#endif

    device_vector<cellid_t> gridIndex;
    device_vector<s_particle> smallParticles;
    device_vector<cellid_t> pairsPerBox;
    device_vector<cellid_t> pairsPerBoxIdxes;
    setup_small_particle_grid(startIter, seperator, gridIndex, smallParticles,
                              g);
    arrangeStorageForSSpairs(g, pairsPerBox, pairsPerBoxIdxes, gridIndex);
    cellid_t *d_rawIndex = thrust::raw_pointer_cast(gridIndex.data());
    s_particle *d_raw_smallParticles =
        thrust::raw_pointer_cast(smallParticles.data());
    size_t nrOfCollisions =
        thrust::reduce(pairsPerBox.begin(), pairsPerBox.end());
#ifdef USE_TIMING
    cudaDeviceSynchronize();
    setupSmall.stop();

    //////////////////////
    ////// Setup Big
    /////////////////////
    setupBig.start();
#endif
    device_vector<cellid_t> bigGridCellIds;
    device_vector<cellid_t> bigGridParticleNr;
    size_t nrOfBigBoxes;
    setupBigParticles(seperator, raw_data.end(), bigGridCellIds,
                      bigGridParticleNr, g, nrOfBigBoxes);
#ifdef USE_TIMING
    cudaDeviceSynchronize();
    setupBig.stop();

    /////////////
    // FIND actuall small big interactions
    ///////
    small_big.start();
#endif

    /* holst the number of collisions per subbox */
    thrust::device_vector<cellid_t> BoxGridCollisions(nrOfBigBoxes + 1);

    auto zip7 = thrust::make_zip_iterator(
        thrust::make_tuple(bigGridCellIds.begin(), BoxGridCollisions.begin()));
    /* fill BoxGridCollisions, by checking how many particles are in that
     * subbox*/
    thrust::for_each(
        zip7, zip7 + nrOfBigBoxes,
        [=] __device__(thrust::tuple<const cellid_t &, cellid_t &> tpl) {
          thrust::get<1>(tpl) = d_rawIndex[thrust::get<0>(tpl) + 1] -
                                d_rawIndex[thrust::get<0>(tpl)];
        });

    /* Create an Index structure */
    thrust::exclusive_scan(BoxGridCollisions.begin(), BoxGridCollisions.end(),
                           BoxGridCollisions.begin());
    cellid_t *d_BoxGridCollisions =
        thrust::raw_pointer_cast(BoxGridCollisions.data());
    cellid_t nrOfSB_byNew = BoxGridCollisions.back();

    /* allocate that memory to store the found pairs in */
    thrust::device_vector<thrust::pair<ptclid_t, ptclid_t>>
    big_small_collisions(nrOfSB_byNew);
    auto *d_raw_big_small_collisions =
        thrust::raw_pointer_cast(big_small_collisions.data());

    /* find all the pairs and put them in their place. This is inefficent, due
     * to varying work
     * loads
     * (for loop of uneven length.) and could be improved, by iterating over
     * small particles
     * instred
     * of subgridcells */
    auto zip8 = thrust::make_zip_iterator(thrust::make_tuple(
        bigGridParticleNr.begin(), bigGridCellIds.begin(), simpleCount));
    thrust::for_each(
        zip8, zip8 + nrOfBigBoxes,
        [=] __device__(
            thrust::tuple<const cellid_t &, const cellid_t &, const cellid_t>
                tpl) {
          const auto &BigNr = thrust::get<0>(tpl); // the n-th big particle
          const auto &boxNr =
              thrust::get<1>(tpl); // In what box are we looking?
          const auto &contr = thrust::get<2>(tpl);
          assert(d_BoxGridCollisions[contr + 1] - d_BoxGridCollisions[contr] ==
                 d_rawIndex[boxNr + 1] - d_rawIndex[boxNr]);
          size_t start_write_idx = d_BoxGridCollisions[contr];
          for (size_t idx = 0; idx < d_BoxGridCollisions[contr + 1] -
                                         d_BoxGridCollisions[contr];
               idx++) {
            thrust::get<0>(d_raw_big_small_collisions[start_write_idx + idx]) =
                d_seperator[BigNr].id;
            thrust::get<1>(d_raw_big_small_collisions[start_write_idx + idx]) =
                d_raw_smallParticles[d_rawIndex[boxNr] + idx].id;
          }
        });

// std::cout << "#--found " << big_small_collisions.size() << " big smal
// col\n";

#ifdef USE_TIMING
    cudaDeviceSynchronize();
    small_big.stop();

    ////////////////
    // Small Small Interaction
    ///////////////////

    small_small.start();
#endif

    cellid_t *d_raw_pairsPerBox = thrust::raw_pointer_cast(pairsPerBox.data());
    cellid_t *d_raw_pairsPerBoxIdxes =
        thrust::raw_pointer_cast(pairsPerBoxIdxes.data());

    d_res.resize(d_res.size() + nrOfCollisions);
    // Initialize the collision result. Can be removed, if not needed for
    // checking
    /*thrust::for_each(d_res.begin(), d_res.end(),
                     [=] __device__(thrust::tuple<size_t, size_t> & tpl) {
      thrust::get<0>(tpl) = 66;
      thrust::get<1>(tpl) = 99;
    });*/

    auto *d_raw_res = thrust::raw_pointer_cast(d_res.data());

    ///////////////////////
    // Calculating Small - Small interaction
    //////////////////
    auto zip3start = thrust::make_zip_iterator(
        thrust::make_tuple(smallParticles.begin(), simpleCount));
    auto zip3end = zip3start + nr_of_small_particles;

    /* the main s <-> s kernel. Some improvments are possible, like avoiding for
     * loops, and not
     * storing hole particls, just ids */
    thrust::for_each(zip3start, zip3end,
                     [=] __device__(thrust::tuple<s_particle &, cellid_t> tpl) {
      const s_particle &p = thrust::get<0>(tpl);
      // More efficent if mvec2idx is avoided
      const cellid_t binNumber = g.mvec2idx(p.pos);
      const cellid_t localNumber = thrust::get<1>(tpl);
      // assert(binNumber <= localNumber);
      const cellid_t nrInBin = localNumber - d_rawIndex[binNumber];
      auto next = g.adjacentBoxs(binNumber);
      assert(localNumber < (d_rawIndex[binNumber + 1] + d_rawIndex[binNumber]));
      const cellid_t pairsPerParticle_in_this_bin =
          d_raw_pairsPerBox[binNumber] /
          (d_rawIndex[binNumber + 1] - d_rawIndex[binNumber]);

      cellid_t pairStartIdx = pairsPerParticle_in_this_bin * nrInBin +
                              d_raw_pairsPerBoxIdxes[binNumber];
      for (auto iter = next.begin(); iter != next.end(); iter++) {
        for (auto p_iter = d_raw_smallParticles + d_rawIndex[*iter];
             p_iter != d_raw_smallParticles + d_rawIndex[(*iter) + 1];
             p_iter++) {
          thrust::get<0>(d_raw_res[pairStartIdx]) = p.id;
          thrust::get<1>(d_raw_res[pairStartIdx]) = p_iter->id;
          pairStartIdx++;
        }
      }
      assert(pairStartIdx == pairsPerParticle_in_this_bin * (nrInBin + 1) +
                                 d_raw_pairsPerBoxIdxes[binNumber]);
    });

#ifdef USE_TIMING
    cudaDeviceSynchronize();
    small_small.stop();

    /////  Post Processing
    ////////////////////////
    /* The results that were calculated for d_res are now pruned (redundancies
     * removed) and added to d_res_pruned to be persistend for the next
     * iteration */
    ssMemoryPost.start();
#endif
    auto prunedOldSize = d_res_pruned.size();
    d_res_pruned.resize(d_res_pruned.size() + d_res.size());
    prunedEnd = d_res_pruned.begin() + prunedOldSize;

    assert(d_res_pruned.end() - prunedEnd >= d_res.size());

    auto prunedEndOld = prunedEnd;
    prunedEnd = thrust::copy_if(
        d_res.begin(), d_res.end(), prunedEnd,
        [] __device__(const thrust::tuple<ptclid_t, ptclid_t> & tpl) {
          return thrust::get<0>(tpl) < thrust::get<1>(tpl);
        });
    d_res_pruned.resize(prunedEnd - d_res_pruned.begin());

// std::cout << "#--found " << prunedEnd - prunedEndOld << " small small
// \n";

#ifdef USE_TIMING
    cudaDeviceSynchronize();
    ssMemoryPost.stop();

    small_big.start();
#endif
    /* copy small big Collisions*/
    prunedOldSize = d_res_pruned.size();
    d_res_pruned.resize(d_res_pruned.size() + big_small_collisions.size());
    prunedEnd = d_res_pruned.begin() + prunedOldSize;

    assert(d_res_pruned.end() - prunedEnd >= big_small_collisions.size());
    auto retIter = thrust::copy(big_small_collisions.begin(),
                                big_small_collisions.end(), prunedEnd);
    assert(retIter == d_res_pruned.end());
#ifdef USE_TIMING
    cudaDeviceSynchronize();
    small_big.stop();

    thisStepCounter.stop();
// std::cout << " this step took " << thisStepCounter.timeInSec() << "s\n";
#endif
    first = false;

    // For next iteration
    gridSize *= 2;
    startIter = seperator;

  } while (startIter != raw_data.end());

// Copying results from GPU back to main memory
// d_res_pruned.resize(prunedEnd - d_res_pruned.begin());
#ifdef USE_TIMING
  dataBack.start();
#endif
  size_t nrFound = d_res_pruned.size();
  size_t maxSize = conRawData.size();

  Vec systemSize;
  for (auto &x : systemSize)
    x = size;
  if (nrOfRaw != nullptr)
    *nrOfRaw = nrFound;

  // std::cout << " gpu found " << nrFound << "bevor pruning\n";
  // thrust::sort(d_res_pruned.begin(),d_res_pruned.end());
  auto endPruned =
      thrust::remove_if(d_res_pruned.begin(), d_res_pruned.end(),
                        [=] __device__(thrust::tuple<ptclid_t, ptclid_t> tpl) {
        assert(thrust::get<0>(tpl) < maxSize);
        assert(d_conRawData[thrust::get<0>(tpl)].id == thrust::get<0>(tpl));

        ptclid_t a = thrust::get<0>(tpl);
        ptclid_t b = thrust::get<1>(tpl);

        return !areIntersecting(d_conRawData[thrust::get<0>(tpl)],
                                d_conRawData[thrust::get<1>(tpl)], systemSize);
      });
  d_res_pruned.resize(endPruned - d_res_pruned.begin());
  thrust::host_vector<thrust::tuple<ptclid_t, ptclid_t>> result(d_res_pruned);

  std::vector<std::pair<ptclid_t, ptclid_t>> std_res(d_res_pruned.size());
  for (size_t idx = 0; idx != result.size(); idx++) {
    std_res[idx].first = thrust::get<0>(result[idx]);
    std_res[idx].second = thrust::get<1>(result[idx]);
  }
  assert(d_res_pruned.size() == result.size());
  assert(std_res.size() == result.size());
#ifdef USE_TIMING
  dataBack.stop();
  totalTime.stop();
  std::cout << "#NR\ttotal   \tsetup   \tsetupSmall\ts<-->s   \tsetupBig  "
               "\t(div)   \ts<->b   "
            << "\tdataret\n" << data.size() << "\t" << totalTime.timeInSec()
            << "\t" << setup.timeInSec() << "\t" << setupSmall.timeInSec()
            << "\t" << small_small.timeInSec() << "\t" << setupBig.timeInSec()
            << "\t" << setupBigDiv.timeInSec() << "\t" << small_big.timeInSec()
            << "\t" << dataBack.timeInSec() << "\n";
#endif
  return std_res;
}
