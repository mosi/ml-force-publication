#include "grid.h"
#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include <cassert>
#include <cuda.h>
#include <cudaProfiler.h>
#include <cuda_profiler_api.h>
#include <thrust/copy.h>
#include <thrust/device_free.h>
#include <thrust/device_malloc.h>
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <thrust/for_each.h>
#include <thrust/functional.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/partition.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/tuple.h>
#include <utility>

#include "gpu_setupSmall.h"

using thrust::device_vector;

#define USE_TIMING

// it would be more efficent to remove doubel counting
__device__ cellid_t numberOfNeighboring(const cellid_t &idx,
                                        const cellid_t &nrIn, const grid &g,
                                        const cellid_t indexStruct[]) {
  cellid_t res = 0;
  auto next_idx = g.adjacentBoxs(idx);
  for (auto iter = next_idx.begin(); iter != next_idx.end(); iter++) {
    res += nrIn * (indexStruct[(*iter) + 1] - indexStruct[*iter]);
  }
  return res;
}

void setup_small_particle_grid(const s_particleInVecIter d_small_start,
                               const s_particleInVecIter d_small_end,
                               device_vector<cellid_t> &gridIndex,
                               device_vector<s_particle> &smallParticles,
                               const grid g) {
  /* Counts how many particles are in a bin (init to 0) one extra to allow*/
  gridIndex.resize(g.total_nr_of_boxes() + 1);
  cellid_t *d_gridIndex = thrust::raw_pointer_cast(gridIndex.data());
  thrust::fill(gridIndex.begin(), gridIndex.end(), 0);
  cellid_t *d_rawIndex = thrust::raw_pointer_cast(gridIndex.data());

  assert(d_rawIndex == d_gridIndex);

  auto nr_of_small_particles = d_small_end - d_small_start;
  /* Contains what place in a bin a particle has*/
  thrust::device_vector<uint> particle_numbers(nr_of_small_particles);

  /* Here all small particels are assigned a bin. At the same time they store
   their bin number in particle_numbers*/
  auto zipStart = thrust::make_zip_iterator(
      thrust::make_tuple(d_small_start, particle_numbers.begin()));
  auto zipEnd = thrust::make_zip_iterator(
      thrust::make_tuple(d_small_end, particle_numbers.end()));
  thrust::for_each(zipStart, zipEnd,
                   [=] __device__(thrust::tuple<s_particle &, cellid_t &> tpl) {
    const s_particle &p = thrust::get<0>(tpl);
    const cellid_t binNumber = g.mvec2idx(p.pos);
    thrust::get<1>(tpl) = atomicAdd(d_rawIndex + binNumber, 1);
  });

  thrust::exclusive_scan(gridIndex.begin(), gridIndex.end(), gridIndex.begin());

  // Rearrange small particles in new part of memory called smallParticles
  // thrust::device_ptr<s_particle> smallParticles =
  //  thrust::device_malloc<s_particle>(nr_of_small_particles);
  smallParticles.resize(nr_of_small_particles);
  s_particle *d_raw_smallParticles =
      thrust::raw_pointer_cast(smallParticles.data());
  thrust::for_each(
      zipStart, zipEnd,
      [=] __device__(thrust::tuple<s_particle &, const cellid_t &> tpl) {
        const s_particle &p = thrust::get<0>(tpl);
        const cellid_t binNumber = g.mvec2idx(p.pos);
        const cellid_t binStart = d_rawIndex[binNumber];
        d_raw_smallParticles[binStart + thrust::get<1>(tpl)] = p;
      });
}

void arrangeStorageForSSpairs(grid g, device_vector<cellid_t> &pairsPerBox,
                              device_vector<cellid_t> &pairsPerBoxIdxes,
                              const device_vector<cellid_t> &gridIndex) {
  const cellid_t *d_gridIndex = thrust::raw_pointer_cast(gridIndex.data());

  // This is needed to later on allocate enoguh memory for particle pairs, and
  // place partices in parallel

  thrust::counting_iterator<cellid_t> simpleCount(0);
  auto zip2Start = simpleCount;
  auto zip2End = zip2Start + gridIndex.size() -
                 1; // minus 1, because we don't want last one

  pairsPerBox.resize(gridIndex.size() - 1);
  thrust::transform(zip2Start, zip2End, pairsPerBox.begin(),
                    [=] __device__(cellid_t tpl) {
    cellid_t nrOf = d_gridIndex[tpl + 1] - d_gridIndex[tpl];
    // d_gridIndex[thrust::get<0>(tpl) + 1] -
    // d_gridIndex[thrust::get<0>(tpl)],
    return numberOfNeighboring(tpl, nrOf, g, d_gridIndex);
  });

  pairsPerBoxIdxes = pairsPerBox;

  // in these scan operations, the leap from counting to indexing is made
  thrust::exclusive_scan(pairsPerBoxIdxes.begin(), pairsPerBoxIdxes.end(),
                         pairsPerBoxIdxes.begin());
}
