#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include <utility>
#include <vector>

std::vector<std::pair<ptclid_t, ptclid_t>>
detectSmart_GPU(const std::vector<s_particle> &, flt_t size,
                flt_t gridSize_in = -1, size_t *nrOfRaw = nullptr);
