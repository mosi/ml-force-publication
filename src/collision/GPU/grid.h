#include "utilities/Utilities.h"
#include "utilities/mVec.h"
#include <iterator>

#pragma once

#ifdef __CUDACC__
#define CUDA_HOSTDEV __host__ __device__
#else
#define CUDA_HOSTDEV
#endif

struct boolIdxPair {
  bool first;
  uint second;
};

template <typename T, uint capacity> struct limitedVec {
  T data[capacity];
  uint size = 0;
  CUDA_HOSTDEV
  void push_back(T dat) {
    assert(size < capacity);
    data[size] = dat;
    size++;
  }
  CUDA_HOSTDEV
  T *begin() { return data; }
  CUDA_HOSTDEV
  T *end() { return data + size; }
};

// This "grid" is a helper datastructure to translate beetween
// a liner arry representation and a matrix represntation
// it also alows to calculate grid from a real position
struct grid {
  size_t boxsPerAxis = 0;
  flt_t axislength = -1;
  flt_t gridWidth = -1;
  uint dim = 3;
  const bool usePeriodicBoundary = true;
  CUDA_HOSTDEV
  size_t toBox(const size_t &x, const size_t &y, const size_t &z = 0) const {
    assert(x < boxsPerAxis);
    assert(y < boxsPerAxis);
    assert(z < boxsPerAxis);
    if (dim == 2)
      assert(z == 0);
    return x + y * boxsPerAxis + z * boxsPerAxis * boxsPerAxis;
  }
  CUDA_HOSTDEV
  size_t mvec2idx(Vec in) const {
    for (int i : { 0, 1 }) {
      assert(in[i] >= 0);
      assert(in[i] < axislength);
    }
    assert(dim == in.dim());
    size_t x = std::floor(in[0] / gridWidth);
    assert(x < boxsPerAxis);
    size_t y = std::floor(in[1] / gridWidth);
    assert(y < boxsPerAxis);

    size_t z = dim == 3 ? std::floor(in[2] / gridWidth) : 0;
    assert(z < boxsPerAxis);
    return toBox(x, y, z);
  }

  CUDA_HOSTDEV
  size_t total_nr_of_boxes() const {
    assert(boxsPerAxis > 0);
    if (dim == 3) {
      return (boxsPerAxis * boxsPerAxis * boxsPerAxis);
    } else {
      assert(dim == 2);
      return boxsPerAxis * boxsPerAxis;
    }
  }

  CUDA_HOSTDEV boolIdxPair isIn(size_t idx, int dx, int dy, int dz) const {
    size_t x = idx % boxsPerAxis;
    idx -= x;
    idx /= boxsPerAxis;
    size_t y = idx % boxsPerAxis;
    idx -= y;
    idx /= boxsPerAxis;
    size_t z = idx;
    assert(x < boxsPerAxis);
    assert(y < boxsPerAxis);
    assert(z < boxsPerAxis);

    if (dim == 2) {
      assert(dz == 0);
      assert(z == 0);
    }

    boolIdxPair res;
    res.first = false;

    // this is for non periodic boundary conditions
    if (!use_periodic) {
      if ((dx < 0) && (x < -dx))
        return res;
      if ((dy < 0) && (y < -dy))
        return res;
      if ((dz < 0) && (z < -dz))
        return res;

      if (x + dx >= boxsPerAxis)
        return res;
      if (y + dy >= boxsPerAxis)
        return res;
      if (z + dz >= boxsPerAxis)
        return res;
    } else { // use periodic
      if ((dx < 0) && (x < -dx)) {
        dx = boxsPerAxis + dx;
        assert(x + dx < boxsPerAxis);
      }
      if ((dy < 0) && (y < -dy))
        dy += boxsPerAxis;
      if ((dz < 0) && (z < -dz))
        dz += boxsPerAxis;

      if (x + dx >= boxsPerAxis)
        dx -= boxsPerAxis;
      if (y + dy >= boxsPerAxis)
        dy -= boxsPerAxis;
      if (z + dz >= boxsPerAxis)
        dz -= boxsPerAxis;
    }
    res.first = true;
    res.second = toBox(x + dx, y + dy, z + dz);
    return res;
  }

  CUDA_HOSTDEV
  limitedVec<uint, 27> adjacentBoxs(size_t idx) const {
    assert(boxsPerAxis >= 3);
    limitedVec<uint, 27> res;
    const size_t x = idx % boxsPerAxis;
    idx -= x;
    idx /= boxsPerAxis;
    const size_t y = idx % boxsPerAxis;
    idx -= y;
    idx /= boxsPerAxis;
    const size_t z = idx;
    assert(x < boxsPerAxis);
    assert(y < boxsPerAxis);
    assert(z < boxsPerAxis);

    if (dim == 2) {
      assert(z == 0);
    }

    auto dVec = { -1, 0, 1 };
    auto dzVec = dVec;
    if (dim == 2) {
      dzVec = { 0 };
    }
    if (!use_periodic) {
      for (int dz : dzVec) {
        for (int dy : dVec) {
          for (int dx : dVec) {
            if ((x == 0) && (dx == -1))
              continue;
            if ((dx == 1) && (x == boxsPerAxis - 1))
              continue;

            if ((y == 0) && (dy == -1))
              continue;
            if ((dy == 1) && (y == boxsPerAxis - 1))
              continue;

            if ((z == 0) && (dz == -1))
              continue;
            if ((dz == 1) && (z == boxsPerAxis - 1))
              continue;

            auto newIDX = toBox(x + dx, y + dy, z + dz);
            res.push_back(newIDX);
          }
        }
      }
    } else if (use_periodic) {
      for (int dz : dzVec) {
        for (int dy : dVec) {
          for (int dx : dVec) {
            if ((x == 0) && (dx == -1))
              dx = boxsPerAxis - 1;
            if ((dx == 1) && (x == boxsPerAxis - 1))
              dx = -boxsPerAxis + 1;

            if ((y == 0) && (dy == -1))
              dy = boxsPerAxis - 1;
            if ((dy == 1) && (y == boxsPerAxis - 1))
              dy = -boxsPerAxis + 1;

            if ((z == 0) && (dz == -1))
              dz = boxsPerAxis - 1;
            if ((dz == 1) && (z == boxsPerAxis - 1))
              dz = -boxsPerAxis + 1;

            auto newIDX = toBox(x + dx, y + dy, z + dz);
            res.push_back(newIDX);
          }
        }
      }
    }
    if (dim == 2) {
      assert(res.size <= 9);
    }
    return res;
  }
};
