#include "mVec.h"
#include "s_particle.h"
#include <utility>
#include <vector>

std::vector<std::pair<ptclid_t, ptclid_t>>
detectSmartCPU_seq(const std::vector<s_particle> &, flt_t rad, flt_t size);

std::vector<std::pair<ptclid_t, ptclid_t>>
detectSmartCPU_par(const std::vector<s_particle> &, flt_t rad, flt_t size);
