#include "grid.h"
#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include <cassert>
#include <cuda.h>
#include <cudaProfiler.h>
#include <cuda_profiler_api.h>
#include <thrust/copy.h>
#include <thrust/device_free.h>
#include <thrust/device_malloc.h>
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <thrust/for_each.h>
#include <thrust/functional.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/partition.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/tuple.h>
#include <utility>

#pragma once

// thrust::counting_iterator<uint> simpleCount(0);

typedef thrust::detail::normal_iterator<thrust::device_ptr<s_particle>>
s_particleInVecIter;

void
setup_small_particle_grid(const s_particleInVecIter d_small_start,
                          const s_particleInVecIter d_small_end,
                          thrust::device_vector<cellid_t> &gridIndex,
                          thrust::device_vector<s_particle> &smallParticles,
                          const grid g);

void arrangeStorageForSSpairs(grid g,
                              thrust::device_vector<cellid_t> &pairsPerBox,
                              thrust::device_vector<cellid_t> &pairsPerBoxIdxes,
                              const thrust::device_vector<cellid_t> &gridIndex);
