#include "collision/collisionCPU.h"
#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include <algorithm>
#include <cassert>
#include <utility>
#include <vector>

#include <thrust/device_vector.h>

__global__ void checkIfPair(s_particle data[], ptclid_t result[],
                            int numberOfParticles, flt_t radius,
                            unsigned int *counter, ptclid_t nrOf,
                            flt_t sysSize) {
  ptclid_t current = threadIdx.x;
  s_particle *pB = &data[blockIdx.x];
  // assert(false); // Not maintained (periodic boundary)
  while (true) {
    if (current >= blockIdx.x) {
      return;
    }
    s_particle *pA = &data[current];

    current += 1024;

    // if (pA->radius + pB->radius > abs(pA->pos - pB->pos)) {
    // assert(false);
    if (areIntersecting(*pA, *pB, sysSize)) {
      unsigned int nr = atomicAdd(counter, 1);
      assert(nr < 100 * nrOf + 100);

      result[2 * nr] = pA->id;
      result[2 * nr + 1] = pB->id;
    }
  }
}

/////////
///////
std::vector<std::pair<ptclid_t, ptclid_t>>
FRNN_gpu(const std::vector<s_particle> &data, flt_t rad, flt_t size) {

  // assert(false);
  s_particle *rawData;
  auto res = cudaMalloc(&rawData, sizeof(s_particle) * data.size());
  assert(res == cudaSuccess);
  res = cudaMemcpy(rawData, data.data(), data.size() * sizeof(s_particle),
                   cudaMemcpyHostToDevice);
  assert(res == cudaSuccess);

  ptclid_t *d_result;
  res = cudaMalloc(&d_result, sizeof(ptclid_t) * (100 + data.size() * 1000));
  assert(res == cudaSuccess);

  unsigned int *d_counter;
  unsigned int h_counter = 0;
  res = cudaMalloc(&d_counter, sizeof(int));
  assert(res == cudaSuccess);
  cudaMemcpy(d_counter, &h_counter, sizeof(unsigned int),
             cudaMemcpyHostToDevice);

  checkIfPair << <data.size(), 1024>>>
      (rawData, d_result, data.size(), rad, d_counter, data.size(), size);
  cudaFree(rawData);

  cudaMemcpy(&h_counter, d_counter, sizeof(unsigned int),
             cudaMemcpyDeviceToHost);
  std::vector<ptclid_t> h_result(h_counter * 2);
  cudaMemcpy(h_result.data(), d_result, sizeof(ptclid_t) * h_result.size(),
             cudaMemcpyDeviceToHost);

  std::vector<std::pair<ptclid_t, ptclid_t>> result;
  // std::cout<<"GPU "<<h_counter<<"\n";
  result.reserve(h_counter);
  for (unsigned int c = 0; c < h_counter; c++) {
    result.push_back(
        std::make_pair(h_result.at(2 * c), h_result.at(2 * c + 1)));
  }
  cudaFree(d_result);
  cudaFree(d_counter);

  return result;
}
