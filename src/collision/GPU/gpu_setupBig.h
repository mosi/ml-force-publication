#include "collision/collisionCPU.h"
#include "grid.h"
#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include "utilities/ttimer.h"
#include <algorithm>
#include <cassert>
#include <cuda.h>
#include <cudaProfiler.h>
#include <cuda_profiler_api.h>
#include <thrust/copy.h>
#include <thrust/device_free.h>
#include <thrust/device_malloc.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <thrust/for_each.h>
#include <thrust/functional.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/partition.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/tuple.h>
#include <utility>
#include <vector>

#include "gpu_setupSmall.h"

#pragma once
//#define USE_TIMING

void setupBigParticles(s_particleInVecIter firstBig,
                       s_particleInVecIter lastBig,
                       thrust::device_vector<cellid_t> &bigGridCellIds,
                       thrust::device_vector<cellid_t> &bigGridParticleNr,
                       const grid g, size_t &nrOfBigBoxes);
