

#include "collision/collisionCPU.h"
#include "settings.h"
#include "simulator/s_particle.h"
#include "utilities/Utilities.h"
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <thread>

std::vector<std::pair<ptclid_t, ptclid_t>>
findCollisions_subdivide(const std::vector<s_particle> &data, flt_t size);
