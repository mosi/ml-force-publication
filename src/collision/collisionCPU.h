#include "simulator/s_particle.h"
#include "utilities/mVec.h"
#include <functional>
#include <utility>
#include <vector>

std::vector<std::pair<ptclid_t, ptclid_t>>
FRNN_cpu_fut(const std::vector<s_particle> &, flt_t rad, flt_t size);

std::vector<std::pair<ptclid_t, ptclid_t>>
FRNN_cpu_omp(const std::vector<s_particle> &, flt_t rad, flt_t size);

std::vector<std::pair<ptclid_t, ptclid_t>> cpu_omp(
    const std::vector<s_particle> &, const Vec,
    std::function<bool(const s_particle &, const s_particle &, const Vec &)>);

std::vector<std::pair<ptclid_t, ptclid_t>>
collisionNaive(const std::vector<s_particle> &data, flt_t rad, flt_t size);
