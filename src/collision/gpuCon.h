#include "simulator/particle.h"
#include "simulator/s_particle.h"
#include <vector>
#pragma once

s_particle convertToS(const Particle &p);

std::vector<std::pair<ptclid_t, ptclid_t>> getCollisions(const pSystem &sys);
