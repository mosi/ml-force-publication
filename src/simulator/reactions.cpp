#include "reactions.h"
#include "system.h"

void BiMolecularReaction::react(Particle &A, Particle &B, pSystem &system) {
  pSystem &sys = *A.containingSystem;
  assert(sys.timestep > 0);
  assert(A.type == A_type);
  assert(B.type == B_type);

  /*if (react_type == BiReactionType_enum::AintoB) {
    if (A.parentId != -1) {
      assert(false);
      auto A_parent = sys.get(A.parentId);
      A_parent->removeChild(A.id);
    }
    A.parentId = B.id;
    // std::cout<<"A into B :"<<A.id<<" -> "<<B.id<<"\n";
    B.enclosed.push_back(A.id);
  } else if (react_type == BiReactionType_enum::onlyA) {
    assert(false);
    sys.removeFromSystem(B.id);
  } else if (react_type == BiReactionType_enum::AleaveB) {
    assert(A.parentId == B.id);
    B.removeChild(A.id);
    A.parentId = B.parentId;
    if (A.parentId != -1) {
      sys.get(A.parentId)->enclosed.push_back(A.id);
    }
  }*/
  A.hasReacted.push_back(B.id);
  B.hasReacted.push_back(A.id);

  funcAfter(A, B, system);

  // A.updateNeighbors();
  // B.updateNeighbors();
}

bool BiMolecularReaction::testReaktion(const Particle &A, const Particle &B,
                                       const pSystem &system) {
  if (std::find(A.hasReacted.begin(), A.hasReacted.end(), B.id) !=
      A.hasReacted.end()) {
    assert(std::find(B.hasReacted.begin(), B.hasReacted.end(), A.id) !=
           B.hasReacted.end());
    return false;
  }

  if ((!A.is_ignored_by_ids.empty()) || (!B.is_ignored_by_ids.empty())) {
    return false;
  }

  if (!reacPossibleFunc(A, B, system)) {
    return false;
  }
  // std::cout << " react dist: " << system.calcReactDistance(A, B) << "\n";

  if (partialOverlapp < 0) {
    if (system.calcReactDistance(A, B) >= 0) {

      // std::min(A.radius, B.radius)) {
      return false;
    }
  } else {
    assert(partialOverlapp <= 1.);
    if (system.calcReactDistance(A, B) >=
        (1. - partialOverlapp) * 2. * std::min(A.radius, B.radius)) {
      return false;
    }
  }
  /*switch (react_type) {
  case BiReactionType_enum::AleaveB:
    return B.id == A.parentId;
  case BiReactionType_enum::AintoB:
    return B.parentId == A.parentId;
  case BiReactionType_enum::onlyA:
    assert(false);
    break;
  case BiReactionType_enum::AandB:
    assert(false);
    break;
  }*/
  if (rateFunc(A, B, system) < 0) {
    std::cout << " ### NEGATIVE RATE ###\n";
    return false;
  }
  return true;
}

void UniMolecularReaction::react(Particle &A) {
  pSystem &sys = *A.containingSystem;
  assert(A.type == A_type);
  // sys.removeFromSystem(A.id);
  funcAfter(A, sys);
  return;
}
