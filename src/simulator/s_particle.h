#ifdef __CUDACC__
#define CUDA_HOSTDEV __host__ __device__
#else
#define CUDA_HOSTDEV
#endif

#include "utilities/Utilities.h"
#include "utilities/mVec.h"
#pragma once

struct s_particle {
  Vec pos;
  ptclid_t id;
  flt_t radius;
  flt_t inner_radius;

  s_particle(flt_t a, flt_t b, flt_t c);

  CUDA_HOSTDEV
  s_particle();

  CUDA_HOSTDEV
  bool operator<(const s_particle &b) const { return id < b.id; }

  CUDA_HOSTDEV
  bool operator==(const s_particle &b) const { return id == b.id; }
};

void resetParticleCounter();

CUDA_HOSTDEV
bool areIntersectingNonPer(const s_particle &a, const s_particle &b);

CUDA_HOSTDEV
bool areIntersectingNonPer2(const s_particle &a, const s_particle &b,
                            const Vec &sys);

CUDA_HOSTDEV
bool areIntersecting(const s_particle &, const s_particle &, const Vec &);

CUDA_HOSTDEV
bool areIntersectingSSX(const s_particle &, const s_particle &, const Vec &);

CUDA_HOSTDEV
bool AABB_intersect(const s_particle &, const s_particle &, const Vec &);

//#include "simulator/s_particle.cu"
