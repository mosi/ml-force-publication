#include "utilities/Utilities.h"
//#include "benchmarks.h"
#include "collision/gpuCon.h"
//#include "ofMain.h"
#include "simulator/system.h"
#include "visual/visual.h"
#include <fstream>
#include <iostream>
#include <map>
#include <memory>

#include "mapbox/variant.hpp"

#include "settings.h"
#include "utilities/ttimer.h"

#include <memory>

#include "simulator/dsl.h"
#include <ctime>

//#include "visual/opengl/OpenglSystemView.h"

//#include "valgrind/callgrind.h"

// pSystem simulation;

namespace dsl {

std::unique_ptr<pSystem> usedSystem(new pSystem);

std::ofstream file;

std::string filename;

struct reaction;
namespace propertyManagement {
std::vector<std::pair<std::string, std::function<std::string(const pSystem &)>>>
observations;

bool isCompletlyDone = false;
std::vector<std::vector<boxVariant>> propForTyps; // Holds the differnet
                                                  // Properties for different
                                                  // types
std::vector<bool> isDoneForType;
int nrOfExistingSpecies = 0;

int addSpecies() {
  nrOfExistingSpecies++;
  propForTyps.resize(nrOfExistingSpecies);
  isDoneForType.resize(nrOfExistingSpecies);

  return nrOfExistingSpecies - 1;
}
std::vector<reaction *> registeredReactions;
} // namespace propertyManagement

void setupDSL() {
  usedSystem = std::make_unique<pSystem>();
  propertyManagement::isCompletlyDone = false;
  propertyManagement::propForTyps.clear();
  propertyManagement::isDoneForType.clear();
  propertyManagement::nrOfExistingSpecies = 0;
  propertyManagement::registeredReactions.clear();
  propertyManagement::observations.clear();
}

template <typename T>
const T &propWrapper<T>::operator()(const Particle &P) const {
  // assert(P.type == type);
  return mapbox::util::get<T>(P.properties.at(typeToIdx.at(P.type)));
}

template <typename T> T &propWrapper<T>::operator()(Particle &P) const {
  // assert(P.type == type);
  return mapbox::util::get<T>(P.properties.at(typeToIdx.at(P.type)));
}

template <typename T>
void propWrapper<T>::operator=(const propWrapper &oldWrap) {
  for (const auto &val : oldWrap.typeToIdx) {
    if (this->typeToIdx.find(val.first) != this->typeToIdx.end()) {
      throw std::runtime_error("You can not add the same property to the same "
                               "species multiple times!\n");
    }
    this->typeToIdx.insert(val);
  }
}

template struct propWrapper<int>;
template struct propWrapper<double>;

species::species() {
  type = propertyManagement::addSpecies();
  std::cout << " Added new  with type Nr " << type << "\n";
}

template <typename T> propWrapper<T> species::addProperty(T defaultValue) {
  propertyManagement::propForTyps[this->type].push_back(defaultValue);
  this->properties = propertyManagement::propForTyps[this->type];

  propWrapper<T> wrap;
  wrap.typeToIdx.emplace(this->type, this->properties.size() - 1);
  return wrap;
}

template propWrapper<int> species::addProperty(int);
template propWrapper<double> species::addProperty(double);

as_A_struct as_A_struct::operator()(const Particle input) {
  spec = input;
  return *this;
}
as_A_struct as_A;

/*as_B_struct
  as_B_struct::operator()(const species input) {
    spec = input;
    return *this;
  }*/
as_B_struct as_B;

create_new_struct create_new;
/*
create_new_struct
  create_new_struct::operator()(const species input) {
    spec = input;
    return *this;
  }
}

creat_new_struct create_new;
*/

/*
struct reaction {
  double rate = -1;
  pSystem *sys;
  species A_spec;
  species B_spec;

  bool firstOrder = false;
  bool secondOrder = false;
  bool zeroOrder = false;
*/
void reaction::reg() {
  propertyManagement::registeredReactions.push_back(this);
}

void changeSpecies(Particle &A, const species &spec) {
  A.type = spec.type;
  A.properties = spec.properties;
  if (spec.colortype != -1) {
    A.colortype = spec.colortype;
  }
  if (!propertyManagement::isCompletlyDone) {
    propertyManagement::isCompletlyDone = true;
  }
}

/*
reaction(create_new_struct s) {
  zeroOrder = true;
  A_spec = s.spec;
  reg();
};

reaction(as_A_struct A_s) {
  firstOrder = true;
  A_spec = A_s.spec;
  reg();
};

reaction(as_A_struct A_s, as_B_struct B_s) {
  secondOrder = true;
  A_spec = A_s.spec;
  B_spec = B_s.spec;
  reg();
};
std::function<double(const Particle &, const Particle &, const pSystem &)>
    rateFunc =
        [](const Particle &A, const Particle &B, const pSystem &system) {
          assert(false);
          return -1;
        };

std::function<void(Particle &, Particle &, pSystem &)> afterFuncA =
    G_FUNC(assert(true););

std::function<void(Particle &, Particle &, pSystem &)> afterFuncB =
    G_FUNC(assert(true););

std::vector<Particle> create_in_A_vec;
std::vector<Particle> create_at_A_vec;

void createAt_A(const Particle &P) { create_at_A_vec.emplace_back(P); }
void createIn_A(const Particle &P) { create_in_A_vec.emplace_back(P); }

bool isAinB = false;
void AinB() {
  assert(!(isAinB || isBinA || isAdead || isBdead));
  isAinB = true;
}

bool isBinA = false;
void BinA() {
  assert(!(isAinB || isBinA || isAdead || isBdead));
  isBinA = true;
}

bool isAdead = false;
void Adead() {
  assert(!(isAinB || isBinA || isAdead || isBdead));
  isAdead = true;
}

bool isBdead = false;
void Bdead() {
  assert(!(isAinB || isBinA || isAdead || isBdead));
  isBdead = true;
}
};
*/

/*const double surface(const Particle &s) {
  if (DIMENSION == 3) {
    return 4. * mPI * s.radius * s.radius;
  }
  if (DIMENSION == 2) {
    return 2. * mPI * s.radius;
  }
}
const double volume(const Particle &s) {
  if (DIMENSION == 3) {
    return 4. / 3. * mPI * s.radius * s.radius * s.radius;
  }
  if (DIMENSION == 2) {
    return mPI * s.radius * s.radius;
  }
}*/

surface_struct surface_struct::operator()(Particle &s) {
  surface_struct res;
  res.ptr = &s;
  ptr = &s;
  res.c_ptr = ptr;
  c_ptr = ptr;
  assert(res.ptr != nullptr);
  return res;
}
double surface_struct::operator()(const Particle &s) const {
  if (DIMENSION == 3) {
    return 4. * mPI * radius(s) * radius(s);
  }
  if (DIMENSION == 2) {
    return 2. * mPI * (radius(s));
  }
}
void surface_struct::operator=(const double &in) {
  assert(in > 0);
  assert(ptr != nullptr);
  if (DIMENSION == 3) {
    radius(*ptr) = sqrt(in / (4. * mPI));
  }
  if (DIMENSION == 2) {
    radius(*ptr) = in / (2. * mPI);
  }
}
surface_struct::operator double() const {
  assert(c_ptr != nullptr);
  if (DIMENSION == 3) {
    return 4. * mPI * radius(*c_ptr) * radius(*c_ptr);
  }
  if (DIMENSION == 2) {
    return 2. * mPI * (radius(*c_ptr));
  }
}

surface_struct surface;

volume_struct volume_struct::operator()(Particle &s) {
  volume_struct res;
  res.ptr = &s;
  ptr = &s;
  res.c_ptr = ptr;
  c_ptr = ptr;
  assert(res.ptr != nullptr);
  return res;
}
void volume_struct::operator=(const double &in) {
  assert(in > 0);
  assert(ptr != nullptr);
  if (DIMENSION == 3) {
    radius(*ptr) = pow(in * 3. / (4. * mPI), 1. / 3);
  }
  if (DIMENSION == 2) {
    radius(*ptr) = sqrt(in / mPI);
  }
}

/*void volume_struct::operator=(const double &in) {// neu hinzugef?gt,
wahrscheinlich falsch
        assert(in > 0);
        assert(ptr != nullptr);
        volume(*ptr) = volume(in);
};

void volume_struct::operator+(const volume_struct &in) {// neu hinzugef?gt
definition des operators + keine Ahnung, ob richtig.
        assert(in > 0);
        assert(ptr != nullptr);
        if (DIMENSION == 3) {
                radius(*ptr) = radius(*ptr) + pow(in * 3. / (4. * mPI), 1. / 3);
        }
        if (DIMENSION == 2) {
                radius(*ptr) = radius(*ptr) + sqrt(in / mPI);
        }
};*/

double volume_struct::operator()(const Particle &s) const {
  if (DIMENSION == 3) {
    return 4. / 3 * mPI * radius(s) * radius(s) * radius(s);
  }
  if (DIMENSION == 2) {
    return mPI * radius(s) * radius(s);
  }
}
volume_struct::operator double() const {
  assert(c_ptr != nullptr);
  if (DIMENSION == 3) {
    return 4. / 3 * mPI * radius(*c_ptr) * radius(*c_ptr) * radius(*c_ptr);
  }
  if (DIMENSION == 2) {
    return mPI * radius(*c_ptr) * radius(*c_ptr);
  }
}
volume_struct volume;

double &radius(Particle &s) {
  assert((s.radius > 0) || (!propertyManagement::isCompletlyDone));
  return s.targetRadius;
}
const double &radius(const Particle &s) {
  assert((s.radius > 0) || (!propertyManagement::isCompletlyDone));
  return s.targetRadius;
}

int &colorType(Particle &s) { return s.colortype; }
const int &colorType(const Particle &s) { return s.colortype; }

void putInside(std::vector<ptclid_t> ids, const species spec, size_t nr) {
  for (const auto &id : ids) {
    usedSystem->addInside(id, spec, nr);
  }
}

void putInsideAt(std::vector<ptclid_t> ids, species spec, flt_t x, flt_t y,
                 flt_t z) {
  for (const auto &id : ids) {
    auto spec2 = spec;
    spec.parentId = id;
    Vec pos;
    pos[0] = x;
    pos[1] = y;
    if (DIMENSION == 3)
      pos.at(2) = z;
    usedSystem->get(id)->enclosed.push_back(
        usedSystem->addApproxHere(spec, pos, usedSystem->get(id)->radius / 20));
  }
}

std::vector<ptclid_t> putSomewhere(const species &s, size_t nr) {
  propertyManagement::isDoneForType[s.type] = true;
  std::vector<ptclid_t> ids;
  for (unsigned int count = 0; count < nr; count++) {
    auto newID = usedSystem->addWhereFree(s, 1);
    ids.push_back(newID);
  }
  return ids;
}

void setSize(const double length) { usedSystem->allEdgesTo(length); }

void set_temperature(const double temp) { usedSystem->systemTemp = temp; }
void set_viscocity(const double visco) { usedSystem->systemVisco = visco; }

bool hasBeenSetup = false;

void setupSimulation() {
  assert(!hasBeenSetup);
  propertyManagement::isCompletlyDone = true;

  if (file.is_open()) {
    file.close();
  }
  filename = "simulation_output_" + std::to_string(std::time(0)) + ".data";
  file.open(filename);
  write_header();

  for (const auto &reac : propertyManagement::registeredReactions) {
    if (reac->zeroOrder) {
      auto zero = usedSystem->addZeroMolecularReaction();
      zero->newParticle = reac->A_spec;
      if (reac->rate < 0) {
        zero->rateFunction = [=](const pSystem &system) {
          return reac->rateFunc(reac->A_spec, reac->A_spec, system);
        };
      } else {
        auto is_rate = reac->rate;
        zero->rateFunction = [=](const pSystem &system) { return is_rate; };
      }
      zero->funcAfter = [=](Particle &A, pSystem &system) {
        reac->afterFuncA(A, A, system);
      };
    } else if (reac->firstOrder) {
      auto first = usedSystem->addUniMolecularReaction();
      first->A_type = reac->A_spec.type;

      /*  if (!reac->doesDivide) {
        first->reacPossibleFunc = [=](const Particle &A, const pSystem &sys) {
          return reac->reacPossibleFunc(A, A, sys);
        };
      } else {*/ // does Divide
      first->reacPossibleFunc = [=](const Particle &A, const pSystem &sys) {
        if (A.is_ignored_by_ids.empty()) {
          return reac->reacPossibleFunc(A, A, sys);
        } else { // still dividing
          return false;
        }
      };
      //}

      if (reac->rate > 0) {
        auto is_rate = reac->rate;
        first->rateFunction = [=](const Particle &A,
                                  const pSystem &sys) { return is_rate; };
      } else {
        auto is_func = reac->rateFunc;
        first->rateFunction = [=](const Particle &A, const pSystem &sys) {
          return is_func(A, A, sys);
        };
      }

      std::function<void(Particle &, Particle &, pSystem &)> is_func_after =
          reac->afterFuncA;

      if (reac->isAdead) {
        is_func_after =
            G_FUNC(is_func_after(A, B, system); system.removeFromSystem(A.id););
      }

      for (auto &p : reac->create_at_A_vec) {
        is_func_after = G_FUNC(
            auto oldAPos = A.position; auto oldAParent = A.parentId;
            auto oldAradius = A.radius; is_func_after(A, B, system);
            Particle newParticle = p; auto oldAid = A.id;
            newParticle.radius = newParticle.targetRadius;
            system.copyIgnoreState1(A, newParticle);
            reac->create_atA_func(A, B, system, newParticle);
            newParticle.radius *= config::shrinkFaktor;
            newParticle.parentId = oldAParent; newParticle.position = oldAPos;
            ptclid_t newID = system.addApprox(newParticle, oldAradius);
            if (newParticle.parentId != -1) {
                  system.get(newParticle.parentId)->enclosed.push_back(newID);
                } system.copyIgnoreState2(*system.get(oldAid),
                                          *system.get(newID)););
      }

      for (auto &p : reac->create_in_A_vec) {
        assert(!reac->isAdead);
        is_func_after = G_FUNC( // auto oldAPos = A.position;
            is_func_after(A, B, system); Particle newParticle = p;
            newParticle.radius = newParticle.targetRadius;
            reac->create_atA_func(A, B, system, newParticle);
            newParticle.radius *= config::shrinkFaktor;
            system.addInside(A.id, newParticle););
      }

      if (reac->doesDivide) {
        assert(!reac->isAleaveB);
        assert(!reac->isBleaveA);
        assert(!reac->isAinB);
        assert(!reac->isBinA);
        assert(!reac->isAdead);
        assert(!reac->isBdead);

        is_func_after = G_FUNC(is_func_after(A, B, system);
                               reac->afterFuncB(A, B, system););

        is_func_after = G_FUNC(

            auto currentA = &A;

            Particle newParticle = reac->otherDivide;
            newParticle.otherDivideId = A.id;
            newParticle.position = currentA->position;
            newParticle.radius = currentA->radius;
            newParticle.parentId = currentA->parentId;
            newParticle.is_ignoring_ids = currentA->is_ignoring_ids;
            // assert(currentA->is_ignoring_ids.size() == 0);
            assert(currentA->is_ignored_by_ids.size() == 0);

            // only for no contained:
            // assert(currentA->is_ignoring_ids.size() == 0);

            is_func_after(*currentA, newParticle, system);

            // std::cout << " A has number " << currentA->id << "\n";

            // auto a_id = currentA->id;
            ptclid_t newId = system.addForce(newParticle);

            currentA->otherDivideId = newId;

            newParticle.id = -1;

            auto newPRef = system.get(newId);

            if (newPRef->parentId != -1) {
              system.get(newPRef->parentId)->enclosed.push_back(newPRef->id);
            }

            std::vector<Particle *>
                to_free;

            for (const auto &c_id
                 : currentA->enclosed) {
              auto child = system.get(c_id);
              // assert(child->is_ignored_by_ids.empty());
              if (chance(.5)) { // chance(.5)) {
                assert(SafeLess(newId, usedSystem->maxIDused + 1));

                child->is_ignoring_ids.push_back(newId);
                newPRef->is_ignored_by_ids.push_back(c_id);
                assert(child->isIgnoring(newId));
              } else {
                // std::cout << "now " << child->id << " -> " <<
                // child->parentId
                //         << "\n";
                assert(child->parentId == currentA->id);
                to_free.push_back(&(*child));

                currentA->is_ignored_by_ids.push_back(c_id);

                assert(currentA->id <= usedSystem->maxIDused);

                child->is_ignoring_ids.push_back(currentA->id);
                assert(child->isIgnoring(currentA->id));
              }
            } for (auto &x
                   : to_free) {
              system.becomeFree(*x, *currentA);
              system.becomeEnclosed(*x, *newPRef);
            }

            assert(currentA->id <= usedSystem->maxIDused);
            assert(newPRef->id <= usedSystem->maxIDused);

            newPRef->is_ignoring_ids.push_back(currentA->id);
            newPRef->is_ignored_by_ids.push_back(currentA->id);
            currentA->is_ignoring_ids.push_back(newPRef->id);
            currentA->is_ignored_by_ids.push_back(newPRef->id);

            /*for (auto &x
                 : currentA->is_ignoring_ids) {
              std::cout << " curA (" << currentA->id << ") is ignoriing " << x
                        << "\n";
            } for (auto &x
                   : currentA->is_ignored_by_ids) {
              std::cout << " curA (" << currentA->id << ") is ig by " << x
                        << "\n";
            }

            for (auto &x
                 : newPRef->is_ignoring_ids) {
              std::cout << " newP (" << newPRef->id << ") is ignoriing " << x
                        << "\n";
            } for (auto &x
                   : newPRef->is_ignored_by_ids) {
              std::cout << " NewP (" << newPRef->id << ") is ig by " << x
                        << "\n";
            }

            std::cout
            << "A " << currentA->id << " is ignoring "
            << currentA->is_ignoring_ids.size() << " espatially "
            << currentA->is_ignoring_ids.back() << "\n";
            std::cout << "NewP " << newPRef->id << " is ignoring "
                      << newPRef->is_ignoring_ids.size() << " espatially "
                      << newPRef->is_ignoring_ids.back() << "\n";
                      */
            assert(newPRef->isIgnoring(currentA->id));
            assert(currentA->isIgnoring(newPRef->id));

            auto &otherA3 = *system.get(currentA->id); //
            assert(otherA3.id == currentA->id);        //
            assert(&otherA3 == currentA);

            // std::cout << "SPLIT!!! of " << currentA->id << " and "
            //           << newPRef->id << "\n";

            system.pairFunctions.push_back(std::make_pair(
                std::make_pair(currentA->id, newId),

                B_FUNC2(
                    // std::cout << " pair func for " << A.id << " and " <<
                    // B.id
                    //        << " ";

                    assert(A.isIgnoring(B.id)); assert(B.isIgnoring(A.id));
                    auto dist = system.calcOuterDistance(A, B); if (dist < 0) {
                      // (A.isIgnoring(B.id) ||
                      // B.isIgnoring(A.id))) {
                      for (auto &x : A.sumForce) {
                        //  std::cout << "Old x: " << x << "\n";

                        assert(!std::isnan(x));
                      }
                      assert(
                          !std::isnan((B.mass + A.mass) * reac->divisionTime));
                      A.sumForce -= (B.mass + A.mass) * reac->divisionTime *
                                    system.direction(A, B);
                      B.sumForce -= (B.mass + A.mass) * reac->divisionTime *
                                    system.direction(B, A);
                      assert(SafeEqual(B.id, newId));
                      assert(A.isDividing());
                      assert(B.isDividing());
                      for (auto &x : A.sumForce) {
                        // std::cout << "x: " << x << "\n";
                        assert(!std::isnan(x));
                      }
                      return false;
                    } else {
                      /*  std::cout << "\nDone splitting " << A.id << " " <<
                        B.id
                                  << "\n";

                        for (auto &x : A.is_ignoring_ids) {
                          std::cout << " A (" << A.id << ") is ignoriing " <<
                        x
                                    << "\n";
                        }
                        for (auto &x : A.is_ignored_by_ids) {
                          std::cout << " A (" << A.id << ") is ig by " << x
                                    << "\n";
                        }

                        for (auto &x : B.is_ignoring_ids) {
                          std::cout << " B (" << B.id << ") is ignoriing " <<
                        x
                                    << "\n";
                        }
                        for (auto &x : B.is_ignored_by_ids) {
                          std::cout << " B (" << B.id << ") is ig by " << x
                                    << "\n";
                        }*/
                      for (const auto &par : { A, B }) {
                        for (auto cid : par.is_ignored_by_ids) {
                          // std::cout << " " << par.id << " is removingIgn "
                          //           << cid << " -- ";
                          auto pRef = system.get(cid);
                          auto newEnd =
                              std::remove(pRef->is_ignoring_ids.begin(),
                                          pRef->is_ignoring_ids.end(), par.id);
                          // assert(newEnd != pRef->is_ignoring_ids.end());
                          pRef->is_ignoring_ids.erase(
                              newEnd, pRef->is_ignoring_ids.end());
                          // assert(pRef->is_ignoring_ids.empty());
                        }
                        if (par.is_ignoring_ids.size() > 1) {
                          // assert(false);
                        }
                      }

                      // std::cout << "\n CLEAR \n";
                      A.is_ignored_by_ids.clear();
                      B.is_ignored_by_ids.clear();

                      A.otherDivideId = -1;
                      B.otherDivideId = -1;

                      /* std::cout << "\n";

                       for (auto &x : A.is_ignoring_ids) {
                         std::cout << " A (" << A.id << ") is ignoriing " << x
                                   << "\n";
                       }
                       for (auto &x : A.is_ignored_by_ids) {
                         std::cout << " A (" << A.id << ") is ig by " << x
                                   << "\n";
                       }

                       for (auto &x : B.is_ignoring_ids) {
                         std::cout << " B (" << B.id << ") is ignoriing " << x
                                   << "\n";
                       }
                       for (auto &x : B.is_ignored_by_ids) {
                         std::cout << " B (" << B.id << ") is ig by " << x
                                   << "\n";
                       }

                       if (!A.is_ignoring_ids.empty()) {
                         std::cout << "A " << A.id << " is not ignoring "
                                   << A.is_ignoring_ids.size() << " espatially
                       "
                                   << A.is_ignoring_ids.back() << "\n";
                         assert(false);
                       }*/
                      // assert(B.is_ignoring_ids.empty());
                      assert(A.is_ignored_by_ids.empty());
                      assert(B.is_ignored_by_ids.empty());
                      return true;
                    }))););
      }

      first->funcAfter = [=](Particle &A,
                             pSystem &sys) { is_func_after(A, A, sys); };

    } else if (reac->secondOrder) {
      auto second = usedSystem->addBiMolecularReaction();

      assert(usedSystem->biReactions.size() > 0);
      second->A_type = reac->A_spec.type;
      second->B_type = reac->B_spec.type;

      second->reacPossibleFunc = reac->reacPossibleFunc;

      // no reactions while one is dividing

      if (reac->isAinB) {
        second->reacPossibleFunc = B_FUNC(
            reac->reacPossibleFunc(A, B, system) && (A.parentId == B.parentId));
      }

      if (reac->isBinA) {
        second->reacPossibleFunc = B_FUNC(
            reac->reacPossibleFunc(A, B, system) && (A.parentId == B.parentId));
      }

      if (reac->isAleaveB) {
        second->reacPossibleFunc = B_FUNC(
            reac->reacPossibleFunc(A, B, system) && (A.parentId == B.id));
      }

      if (reac->isBleaveA) {
        second->reacPossibleFunc = B_FUNC(
            reac->reacPossibleFunc(A, B, system) && (B.parentId == A.id));
      }

      /*second->reacPossibleFunc = [=](const Particle &A, const Particle &B,
                                     const pSystem &system) {
        if (A.is_ignored_by_ids.empty()) {
          return second->reacPossibleFunc(A, B, system);
        } else { // still dividing
          return false;
        }
      };*/

      if (reac->rate > 0) {
        second->rateFunc = R_FUNC(reac->rate);

      } else if (reac->rate == 0) {
        auto is_rate = reac->rate;
        std::cout << "#  - MESSAGE - : Barrier/rate is 0\n";
        second->rateFunc = R_FUNC(reac->rate);
      } else {
        auto is_func = reac->rateFunc;
        second->rateFunc = reac->rateFunc;
      }
      const auto oldRate = second->rateFunc;
      second->rateFunc = [=](const Particle &A, const Particle &B,
                             const pSystem &system) {
        const auto rate = oldRate(A, B, system);
        return rate;
      };

      std::function<void(Particle &, Particle &, pSystem &)> is_func_after =
          G_FUNC(reac->afterFuncA(A, B, system);
                 reac->afterFuncB(A, B, system););

      if (reac->isAdead) {
        is_func_after =
            G_FUNC(is_func_after(A, B, system); system.removeFromSystem(A.id););
      }
      if (reac->isBdead) {
        is_func_after =
            G_FUNC(is_func_after(A, B, system); system.removeFromSystem(B.id););
      }

      if (reac->isAinB) {
        is_func_after =
            G_FUNC(system.becomeEnclosed(A, B); is_func_after(A, B, system););
      }

      if (reac->isBinA) {
        is_func_after =
            G_FUNC(system.becomeEnclosed(B, A); is_func_after(A, B, system););
      }

      if (reac->isAleaveB) {
        is_func_after =
            G_FUNC(system.becomeFree(A, B); is_func_after(A, B, system););
      }
      if (reac->isBleaveA) {
        is_func_after =
            G_FUNC(system.becomeFree(B, A); is_func_after(A, B, system););
      }

      if (reac->customOverlapp > 0) {
        second->partialOverlapp = reac->customOverlapp;
      }

      for (auto &p : reac->create_at_A_vec) {
        is_func_after = G_FUNC(
            auto oldAParent = A.parentId; auto oldAPos = A.position;
            auto oldAradius = A.radius; is_func_after(A, B, system);
            Particle newParticle = p; auto oldAid = A.id;
            newParticle.radius = newParticle.targetRadius;
            std::cout << " A is "; A.writeInfo(); // << A.id << "\n";
            system.copyIgnoreState1(*system.get(oldAid), newParticle);
            reac->create_atA_func(A, B, system, newParticle);
            newParticle.parentId = oldAParent;
            newParticle.radius *= config::shrinkFaktor;
            newParticle.position = oldAPos;
            ptclid_t newID = system.addApprox(newParticle, oldAradius);
            if (newParticle.parentId != -1) {
                  system.get(newParticle.parentId)->enclosed.push_back(newID);
                } system.copyIgnoreState2(*system.get(oldAid),
                                          *system.get(newID)););
      }
      for (auto &p : reac->create_at_B_vec) {
        is_func_after = G_FUNC(
            auto oldBParent = B.parentId; auto oldBPos = B.position;
            auto oldRadius = B.radius;
            // auto oldBId = B.id;
            is_func_after(A, B, system); Particle newParticle = p;
            newParticle.radius = newParticle.targetRadius;
            system.copyIgnoreState1(B, newParticle);
            reac->create_atB_func(A, B, system, newParticle);
            newParticle.parentId = oldBParent;
            newParticle.radius *= config::shrinkFaktor;
            newParticle.position = oldBPos;
            ptclid_t newID = system.addApprox(newParticle, oldRadius);
            if (newParticle.parentId != -1) {
                  system.get(newParticle.parentId)->enclosed.push_back(newID);
                } system.copyIgnoreState(*system.get(B.id),
                                         *system.get(newID)););
      }

      for (auto &p : reac->create_in_A_vec) {
        assert(!reac->isAdead);
        is_func_after = G_FUNC( // auto oldAPos = A.position;
            is_func_after(A, B, system); Particle newParticle = p;
            newParticle.radius = newParticle.targetRadius;
            reac->create_atA_func(A, B, system, newParticle);
            newParticle.radius *= config::shrinkFaktor;
            system.addInside(A.id, newParticle););
      }

      for (auto &p : reac->create_in_B_vec) {
        assert(!reac->isBdead);
        is_func_after = G_FUNC( // auto oldAPos = B.position;
            is_func_after(A, B, system); Particle newParticle = p;
            newParticle.radius = newParticle.targetRadius;
            reac->create_atB_func(A, B, system, newParticle);
            newParticle.radius *= config::shrinkFaktor;
            system.addInside(A.id, newParticle););
      }

      assert(!reac->doesDivide);

      second->funcAfter =
          is_func_after; //[=](Particle &A, Particle&B pSystem &sys) {
                         //  is_func_after(A, B, sys);
                         //};
    }
  }
  usedSystem->boxify();
}

bool chance(double likelyhood) {
  assert(likelyhood >= 0);
  assert(likelyhood <= 1);
  return likelyhood >= util::randomZeroOne();
}

void add_force(std::function<Vec(const pSystem &, const Particle &A)> forceExpr,
               std::function<bool(const Particle &, const Particle &,
                                  const pSystem &)> possibleFunc) {
  usedSystem->forceFunc.push_back([=](pSystem &sys, Particle &A) {
    if (possibleFunc(A, A, sys)) {
      A.sumForce += forceExpr(sys, A);
    }
  });
}

/* nrOfSteps = 0 runs for infinity */
void runNoVisual(const size_t nrOfSteps) {
  if (!hasBeenSetup) {
    setupSimulation();
  }
  for (size_t counter = 1; counter != nrOfSteps; counter++) {
    usedSystem->do_step();
  }
}

int max_used_internal_framerate = -1;
void maxInternalFramerate(int val) { max_used_internal_framerate = val; }

void visualize(const int framerate = 20) {
  if (!hasBeenSetup) {
    setupSimulation();
  }

  std::unique_ptr<systemView> viewer(getView(framerate));

  ttimer totalTime;
  ttimer simTime;
  ttimer renderTime;
  int counter = 0;
  // runtime of the simulator, e.g. time, stepCounter, size, ...
  while (usedSystem->time <= 2. * units::second) {
	  //saves data from individual particles e.g. position, ...
    /*if (usedSystem->stepCounter == 0 ||
        usedSystem->time >= 15e-6 * units::second) {
      for (auto test : usedSystem->data) {
        std::ofstream file_2;
        auto test_2 = test.second;
        std::string filename_2;
        filename_2 = "position_id_" + std::to_string(test_2.id) + ".data";
        file_2.open(filename_2, std::ios::out | std::ios::app);
        file_2 << std::setprecision(10) << usedSystem->time << "\t";

        // file_2 << std::setprecision(10) << test_2.colortype << "\t";

        // file_2 << std::setprecision(10) << test_2.mass << "\t";

        file_2 << std::setprecision(10) << test_2.position[0] << "\t"
               << test_2.position[1] << "\t" << test_2.position[2] << "\t";

        // file_2 << std::setprecision(10) << test_2.velocity[0] << "\t"
        //       << test_2.velocity[1] << "\t" << test_2.velocity[2] << "\t";

        // file_2 << std::setprecision(10) << test_2.sumForce[0] << "\t"
        //       << test_2.sumForce[1] << "\t" << test_2.sumForce[2] << "\n";

        // file_2 << std::setprecision(10) << test_2.mass << "\n";
        // file_2 << std::setprecision(10) << test_2.c1 << "\t" << test_2.c2 <<
        file_2 << "\n";
        file_2.close();
      }
    }*/
    counter++;
    totalTime.start();
    ttimer steptime;
    steptime.start();
    simTime.start();
    usedSystem->do_step();
    simTime.stop();

    ttimer busyWait;
    busyWait.start();
    while (steptime.timePassed() < 1. / max_used_internal_framerate) {
      // busy wait
      // std::cout << "busy..\n";
    }
    busyWait.stop();

    renderTime.start();
    // CALLGRIND_STOP_INSTRUMENTATION;

    if (usedSystem->stepCounter % 100 == 00) {
      viewer->updateWindow(*usedSystem);
      // CALLGRIND_START_INSTRUMENTATION;
    }
    renderTime.stop();
    totalTime.stop();
    if (counter == 10000) {
      write_to_file();
      counter = 0;
      std::cout << "#" << usedSystem->stepCounter
                << " T = " << usedSystem->time / units::second << "[s] @ "
                << nearbyint(100. / totalTime.timeInSec()) << "fps"
                << " " << usedSystem->size() << "/"
                << usedSystem->collisionlist.size()
                << " \t(sim(col/int/bir/for/zor/gch)/vis/wait) = ("
                << nearbyint(simTime.timeInSec() / totalTime.timeInSec() * 100)
                << "("
                << nearbyint(100 * usedSystem->collisionTime.timeInSec() /
                             simTime.timeInSec()) << "/"
                << nearbyint(100 * usedSystem->gausNumberTime.timeInSec() /
                             simTime.timeInSec()) << "/"
                << nearbyint(100 * usedSystem->biReacTime.timeInSec() /
                             simTime.timeInSec()) << "/"
                << nearbyint(100 * usedSystem->lowReacTime.timeInSec() /
                             simTime.timeInSec()) << "/"
                << nearbyint(100 * usedSystem->zeroReacTime.timeInSec() /
                             simTime.timeInSec()) << "/"
                << nearbyint(100 * usedSystem->listHandelTime.timeInSec() /
                             simTime.timeInSec()) << ")/"
                << nearbyint(renderTime.timeInSec() / totalTime.timeInSec() *
                             100) << "/"
                << nearbyint(100 * busyWait.timeInSec() / totalTime.timeInSec())
                << ")"
                << "\n";
      /*     << 100. - simTime.timeInSec() / totalTime.timeInSec() * 100
           << "% visualizing, generating " << 100. / totalTime.timeInSec()
           << "fps of data (render at " << framerate
           << "fps)  time: " << usedSystem->time << " (busy: "
           << 100. * busyWait.timeInSec() / totalTime.timeInSec()
           << "))\n";*/
      totalTime.reset();
      simTime.reset();
      usedSystem->collisionTime.reset();
      usedSystem->listHandelTime.reset();
      usedSystem->zeroReacTime.reset();
      usedSystem->gausNumberTime.reset();
      usedSystem->lowReacTime.reset();
      usedSystem->biReacTime.reset();
      renderTime.reset();
    }
  }
}

std::vector<ptclid_t> putHere(const species spec, const flt_t x, const flt_t y,
                              flt_t z = 0) {
  if (spec.targetRadius < 0) {
    std::cerr << "## You've tried to putHere a particle of type " << spec.name
              << " with a negative radius of " << spec.radius << "\n";
    throw std::runtime_error("negative radius");
  }
  if (DIMENSION == 2) {
    if (z != 0) {
      std::cout << "#put here 3rd coordinate forced to 0 in 2D\n";
    }
    z = 0;
    assert(z == 0);
  }
  Vec atPos;
  atPos.init();
  atPos[0] = x;
  atPos[1] = y;
  if (DIMENSION == 3) {
    atPos[2] = z;
  }
  auto newID = usedSystem->addApproxHere(spec, atPos,
                                         usedSystem->systemSize.at(0) / 500);
  std::vector<ptclid_t> res;
  res.push_back(newID);
  return res;
}

size_t fileCounter = 0;

void write_to_file() {
  assert(file.is_open());

  file << usedSystem->time / units::second << "\t";
  for (const auto &funcPair : propertyManagement::observations) {
    file << funcPair.second(*usedSystem) << "\t";
  }
  file << "\n";

  fileCounter++;
  if (fileCounter >= config::filebuffer) {
    file.close();
    file.open(filename, std::fstream::out | std::fstream::app);
  }
}

void write_header() {
  assert(file.is_open());
  file << "#time[s]"
       << "\t";
  for (const auto &funcPair : propertyManagement::observations) {
    file << funcPair.first << "\t";
  }
  file << "\n";
}

size_t nr_in_state = 0;
std::map<std::pair<int, int>, int> cachedNrIn;

int nr_in(const species &spec, const Particle &ptcl) {
  if (nr_in_state != usedSystem->stepCounter) {
    cachedNrIn.clear();
    nr_in_state = usedSystem->stepCounter;
  }

  auto val = cachedNrIn.find(std::make_pair(spec.type, ptcl.id));
  if (val != cachedNrIn.end()) {
    // std::cout << " chaching worked!\n";
    return val->second;
  }

  // not found in chache
  int counter = 0;
  for (const auto &pptr : ptcl.enclosed) {
    const auto p = ptcl.containingSystem->get(pptr);
    if (p->type == spec.type) {
      counter++;
    }
  }
  cachedNrIn.insert(
      std::make_pair(std::make_pair(spec.type, ptcl.id), counter));
  return counter;
}

int nr_in_system(const species &spec) {
  int counter = 0;
  for (const auto &p : usedSystem->data) {
    if (p.second.type == spec.type) {
      counter++;
    }
  }
  return counter;
}

namespace observe {
void nr_of_struct::operator()(const species &s) {
  auto type = s.type;
  std::function<std::string(const pSystem &)> func = [=](const pSystem &sys) {
    size_t counter = 0;
    for (const auto &p : sys.data) {
      if (p.second.type == type) {
        counter++;
      }
    }
    std::string result = std::to_string(counter);
    return result;
  };
  std::string propName =
      "nr_of_" + s.name; //"Nr_of_type_" + std::to_string(type);
  propertyManagement::observations.push_back(std::make_pair(propName, func));
}

void nr_of_struct::
operator()(const dsl::species &s,
           std::function<bool(const Particle &, const Particle &,
                              const pSystem &)> if_func) {
  auto type = s.type;
  std::function<std::string(const pSystem &)> func = [=](const pSystem &sys) {
    size_t counter = 0;
    for (const auto &p : sys.data) {
      if ((p.second.type == type) && if_func(p.second, p.second, sys)) {
        counter++;
      }
    }
    std::string result = std::to_string(counter);
    return result;
  };
  std::string propName =
      "nr_of_" + s.name; //"Nr_of_type_" + std::to_string(type);
  propertyManagement::observations.push_back(std::make_pair(propName, func));
}

nr_of_struct nr_of;

void nr_of_A_struct::
operator()(const species &s,
           std::function<bool(const Particle &, const Particle &,
                              const pSystem &)> funcIN) {
  auto type = s.type;
  std::function<std::string(const pSystem &)> func = [=](const pSystem &sys) {
    size_t counter = 0;
    for (const auto &p : sys.data) {
      if (p.second.type == type) {
        if (funcIN(p.second, p.second, sys)) {
          counter++;
        }
      }
    }
    std::string result = std::to_string(counter);
    return result;
  };
  std::string propName =
      "nr_of_BOOL_" + s.name; //"Nr_of_type_" + std::to_string(type);
  propertyManagement::observations.push_back(std::make_pair(propName, func));
}

nr_of_A_struct nr_of_A;

template <typename T>
void average_prop(const species &spec, const propWrapper<T> &prop) {
  auto type = spec.type;
  std::function<std::string(const pSystem &)> func = [=](const pSystem &sys) {
    size_t counter = 0;
    T val = 0;
    for (const auto &p : sys.data) {
      if (p.second.type == type) {
        counter++;
        val += prop(p.second);
      }
    }
    if (counter == 0) {
      return std::string("ERR");
    }
    return std::to_string(static_cast<double>(val) / counter);
  };
  std::string name = "avr_" + prop.name + "(" + spec.name + ")";
  propertyManagement::observations.push_back(std::make_pair(name, func));
}

/*template <typename T> void average_prop(const species &spec,  T prop) {
  ptclid_t type = spec.type;
  std::function<std::string(const pSystem &)> func = [=](const pSystem &sys) {
    size_t counter = 0;
    double val = 0;
    for (const auto &p : sys.data) {
      if (p.type == type) {
        counter++;
        val += prop(p);
      }
    }
    return std::to_string(val / counter);
  };
  std::string name = "average_" + std::to_string(type);
  propertyManagement::observations.push_back(std::make_pair(name, func));
}*/

template void average_prop<int>(const species &spec,
                                const propWrapper<int> &prop);
template void average_prop<double>(const species &spec,
                                   const propWrapper<double> &prop);
} // namespace observe
} // namespace dsl
