#pragma once

//#include "system.h"
#include "particle.h"
#include <functional>

struct pSystem;

/* these enums will later be used to switch between different execution logics
 * for different types of reaction*/
enum class BiReactionType_enum {
  AintoB,
  AandB,
  onlyA,
  AleaveB,
  NOT_SET
};
/*enum class UniReactionType_enum {
  Asurvive,
  Adead,
  NOT_SET
};*/

/* A reaction, starting out with two particels*/
struct BiMolecularReaction {
  CellType A_type = -1;
  CellType B_type = -1;

  double partialOverlapp = -1;

  BiReactionType_enum react_type = BiReactionType_enum::NOT_SET;

  // Functions on the particles to be called after reaction
  std::function<void(Particle &, Particle &, pSystem &)> funcAfter = [](
      Particle &A, Particle &B, pSystem &system) { return; };

  // used, when rate is negative
  std::function<double(const Particle &, const Particle &,
                       const pSystem &system)> rateFunc =
      [](const Particle &A, const Particle &B,
         const pSystem &system) { return -1; };

  void react(Particle &A, Particle &B, pSystem &system);

  std::function<bool(const Particle &, const Particle &, const pSystem &)>
  reacPossibleFunc = [=](const Particle &A, const Particle &B,
                         const pSystem &system) {
    throw std::runtime_error("\n\ncall to undefined function\n");
    return false;
  };

  bool testReaktion(const Particle &A, const Particle &B,
                    const pSystem &system);
};

/* a reaction starting out with one particle*/
struct UniMolecularReaction {
  CellType A_type = -1;
  // UniReactionType_enum react_type = UniReactionType_enum::NOT_SET;

  std::function<bool(const Particle &, const pSystem &)> reacPossibleFunc = [=](
      const Particle &A, const pSystem &system) {
    throw std::runtime_error("\n\ncall to undefined function\n");
    return false;
  };

  std::function<void(Particle &, pSystem &)> funcAfter = [](
      Particle &A, pSystem &) { return; };
  void react(Particle &A);

  // per individual Particle
  std::function<double(const Particle &, const pSystem &)> rateFunction = [](
      const Particle &A, const pSystem &sys) {
    throw std::runtime_error("\n\ncall to undefined function\n");
    return 1.;
  };
};

/* a reaciton starting out with no particels */
struct ZeroMolecularReaction {
  Particle newParticle;
  std::function<double(const pSystem &)> rateFunction = [](const pSystem &sys) {
    throw std::runtime_error("\n\ncall to undefined function\n");
    return 1.;
  };

  std::function<void(Particle &, pSystem &)> funcAfter = [](
      Particle &A, pSystem &system) { return; };

  // std::function<void(Particle &)> init_func = [](Particle &A) { return; };
};
