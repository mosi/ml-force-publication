/*
Here the particel is defined
*/

#include "particle.h"
#include "system.h"

/* perform velocity rescaling as requierd by langevin fluctuation disipation
 * theorem leading to brownian motion*/
void Particle::diffusionForce() {
  for (auto &x : velocity) {
    assert(!std::isnan(x));
  }
  Vec force_velocity = c1 * velocity;
  velocity = (mass * c1 * velocity + c2 * util::randomGausVec(1)) / mass;
  if (!move) {
    velocity.init();
  }
  if (!diffuse) {
    velocity = force_velocity;
  }

  for (auto &x : velocity) {
    assert(!std::isnan(x));
  }
  return;
}

/* integrate the velocity from the forces*/
void Particle::updateVelocity() {
  for (auto &x : velocity) {
    assert(!std::isnan(x));
  }
  for (auto &x : sumForce) {
    assert(!std::isnan(x));
  }

  velocity += .5 / mass * containingSystem->timestep * sumForce;
  if (!move) {
    velocity.init();
  }

  for (auto &x : velocity) {
    assert(!std::isnan(x));
  }
}

/* calculate the clasical diffunios constat for the particel */
double Particle::diffusionConstant() const {
  assert(isSetup);
  return nature::boltzmann * containingSystem->systemTemp / frictionGamma;
}

/* remove a singe child particel (for example after reaction) */
void Particle::removeChild(int c_id) {
  // std::cout << " PTCL " << id << " is removing child " << c_id << "\n";
  auto oldSize = enclosed.size();
  auto child =
      std::find_if(enclosed.begin(), enclosed.end(), [=](const int &p) {
        // std::cout << p << "/" << c_id << "\n";
        return p == c_id;
      });
  // make sure it was found:
  assert(child != enclosed.end());
  enclosed.erase(child);
  assert(enclosed.size() == oldSize - 1);
  // std::cout << "child successfully removed\n";
}

const Particle &Particle::parent() const {
  assert(parentId >= 0);
  return *containingSystem->get(parentId);
}

/* integrate the posiotion from the velocity
if other particels are containd in this one, move them too*/
void Particle::updatePosition() {
  assert(isSetup);
  assert(containingSystem->timestep > 0);

  if (move) {

    // integrate
    auto deltaPos = velocity * containingSystem->timestep;
    position += deltaPos;
    /*
    if (containingSystem->time > 0.05) {
      std::cout << "timestep:" << containingSystem->timestep
                << ", Geschwindigkeit: " << velocity
                << ", deltaPos:" << deltaPos << "\n";
    }
        */
    // move child particels
    for (auto &childId : enclosed) {
      assert(containingSystem != nullptr);
      containingSystem->get(childId)->position += deltaPos;
    }
  }
}

/* update the list of neighbors with are in close proximity, and the only ones
neccecary to check for possible interaciton*/
/*void Particle::updateNeighbors() {
  const int nn = containingSystem->neighborsRenewNumber;
  if (nn > 0) {
    neighbors.clear();
    // calculate the maximum distance per timestep of the fastest particel in
    // the system
    const double maxDistPerTimestep =
        containingSystem->maxSpeed * containingSystem->timestep;
    for (auto &p2 : containingSystem->data) {
      // a particle is ed to the neighborlist under two conditions
      //1. it is a differnt particel
      if ((id != p2.id) &&
          // 2. even with the maximum velocity in the system it will not collide
          // befor the next neighbor update
          (containingSystem->calcOuterDistance(*this, p2) <
           nn * maxDistPerTimestep)) {
        neighbors.push_back(p2.id);
      }
    }
  }
}*/

/* removes a single neigbor from the list, for example after their death */
/*void Particle::removeNeighbor(int neighborId) {
  auto res = std::find_if(neighbors.begin(), neighbors.end(),
                          [=](const int &pId) { return pId == neighborId; });
  // check if actually found
  assert(res != neighbors.end());
  neighbors.erase(res);
}*/

/* this returns the average distance the particel will move under diffusion*/
double Particle::meanMoveDist() const {
  assert(containingSystem != nullptr);
  // TODO check math on that
  return sqrt(diffusionConstant() * containingSystem->timestep * 2);
}

void Particle::recalculateAllConstants() {
  if (parentId == -1) {
    frictionGamma = 6 * mPI * containingSystem->systemVisco * radius;
    recalculateIntegrationConstants();
  } else {
    frictionGamma = 6 * mPI * parent().local_viscosity * radius;
    recalculateIntegrationConstants();
  }
}

void Particle::recalculateIntegrationConstants() {
  // corrected Version with Error found by Philipp
  c1 = exp(-frictionGamma * containingSystem->timestep / (2. * mass));
  c2 = sqrt((1. - c1 * c1) * mass * nature::boltzmann *
            containingSystem->systemTemp);
}

void Particle::recalculateMass() {
  if (local_density <= 0) {
    std::cerr << "\n\nERR: local density of a particle is below 0.\n";
    throw std::runtime_error("wrong density value");
  }
  if (DIMENSION == 2) {
    // mass = mPI * radius * radius * constants::density;
    // use 3d mass/density as requested by Philipp
    mass = mPI * 4 / 3 * radius * radius * radius * local_density;

  } else if (DIMENSION == 3) {
    mass = mPI * 4 / 3 * radius * radius * radius * local_density;
  } else {
    assert(false);
  }
}

/* each particel needs to be set up once it has enterd the system*/
void Particle::setup() {
  assert(containingSystem->systemTemp > 0);
  assert(containingSystem->systemVisco > 0);
  assert(!isSetup);
  isSetup = true;
  assert(targetRadius > 0);
  if (radius <= 0) {
    radius = targetRadius;
  }
  assert(containingSystem->systemVisco > 0);
  if (local_viscosity < 0) {
    local_viscosity = containingSystem->systemVisco;
  }

  frictionGamma = 6 * mPI * containingSystem->systemVisco * radius;

  /*if mass has not been set, it will be calculated from the volume and
   * density*/
  if (mass < 0) {
    recalculateMass();
  }

  // set velocity to resonable initial value from Maxwell Bolzmann Distro
  for (auto &v : velocity) {
    // fix from Philipp
    v = 1e-10 * util::gaus(sqrt(nature::boltzmann *
                                containingSystem->systemTemp / mass));
  }

  /*if system has already started running(otherwise this
  would be a waste, since more particels are likley going to be added to the
  system*/
  /*if (containingSystem->stepCounter > 0) {
    updateNeighbors();
    containingSystem->updateNeighbors();
  }*/

  // a resonable estimate from particel size for the systems initial timestep
  if (containingSystem->timestep < 0) {
    recalculateAllConstants();
    containingSystem->setTimestep(diffusionConstant(), radius);
    std::cout << "# automatically set timeste to : "
              << containingSystem->timestep
              << "\n"; // hier wird timestep zum allerersten Mal berechnet
  }
  assert(containingSystem->timestep > 0);

  recalculateAllConstants();
}
