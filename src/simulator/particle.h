#pragma once
struct Particle;
typedef int CellType;
struct pSystem;

#include "utilities/Utilities.h"
#include "utilities/mVec.h"
//#include "system.h"
#include <algorithm>
#include <cmath>
#include <memory>
//#include <variant>
#include "mapbox/variant.hpp"
#include <string>
#include <vector>
#undef NDEBUG
#include "settings.h"
#include <cassert>

struct Particle {
  bool isSetup = false; // flag that needs to be true befor starting calculation
  Vec position = Vec(0);
  double c1, c2; // integration constants
  double radius = -1;
  double targetRadius = 1;

  int colortype = -1;
  Vec sumForce = Vec(0); // the sum of all external forces
  Vec velocity = Vec(0);
  bool move = true;
  bool diffuse = true;

  void writeInfo() const {
    std::cout << "id: " << id << " with parent " << parentId << " ignoring ";
    for (auto &x : is_ignoring_ids) {
      std::cout << x << " ";
    }
    std::cout << "\n";
  }

  std::vector<boxVariant> properties;

  // mapbox::util::variant<int, float> test;

  double frictionGamma = 1e0; // numerical constant, depending on size
  double mass = -1;
  double local_density;
  double local_viscosity = -1;

  CellType type; // the type of the cell. needed for reactions
  // std::vector<int> neighbors;
  int id = -1; // a unique identifier for each cell
  // TODO use size_t instead of int

  pSystem *containingSystem =
      nullptr;                 // a pointer to the system this is containd in
  std::vector<int> hasReacted; // all particels that have reacted with this
                               // particel in thes step. Needed to avoid doupble
                               // accunting.
  std::vector<int> enclosed;   // all particels contind in thes particel
  int parentId = -1;           //-1 if free, id of parent particel else
  std::vector<int> is_ignoring_ids;
  std::vector<int> is_ignored_by_ids;

  int otherDivideId = -1;

  bool isDividing() const { return otherDivideId != -1; }

  bool isIgnoring(const ptclid_t igId) const {
    return is_ignoring_ids.end() !=
           std::find(is_ignoring_ids.begin(), is_ignoring_ids.end(), igId);
  }

  const Particle &parent() const;

  // function declarations (see particle.cpp)
  double diffusionConstant() const;
  void diffusionForce();
  void updateVelocity();
  void updatePosition();
  void removeChild(int c_id);
  double meanMoveDist() const;
  void recalculateAllConstants();
  void recalculateIntegrationConstants();
  void recalculateMass();
  //  void updateNeighbors();
  //  void removeNeighbor(int neighborId);
  void setup();
};
