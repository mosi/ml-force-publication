
#include "utilities/Utilities.h"
//#include "benchmarks.h"
#include "collision/gpuCon.h"
//#include "ofMain.h"
#include "simulator/system.h"
#include "visual/visual.h"
#include <map>

#include "mapbox/variant.hpp"

#include "settings.h"
#include "utilities/ttimer.h"

#include <iomanip>

#pragma once

namespace dsl {

extern std::ofstream file;

void model();
void model2();
void mausmodel();

// extern pSystem simulation;

extern std::unique_ptr<pSystem> usedSystem;

Particle &created(int id);

void setupDSL();

struct reaction;
namespace propertyManagement {
extern std::vector<std::pair<
    std::string, std::function<std::string(const pSystem &)>>> observations;
int addSpecies();
} // namespace propertyManagement

template <typename T> struct propWrapper {
  std::map<CellType, int> typeToIdx;
  std::string name = "unnamedProperty";
  const T &operator()(const Particle &P) const;
  T &operator()(Particle &P) const;

  void operator=(const propWrapper &oldWrap);
};

struct species : Particle {
  species();
  std::string name = "nameNotSet";

  template <typename T> propWrapper<T> addProperty(T defaultValue);
};

#define R_FUNC(funcText)                                                       \
  ([=](const Particle &A, const Particle &B,                                   \
       const pSystem &system) { return static_cast<double>(funcText); })

#define F_FUNC(funcText)                                                       \
  ([=](const pSystem &system,                                                  \
       const Particle &A) { return static_cast<Vec>(funcText); })

#define G_FUNC(funcText)                                                       \
  [=](Particle &A, Particle &B, pSystem &system) { funcText }

#define C_FUNC(funcText)                                                       \
  [=](Particle &A, Particle &B, pSystem &system,                               \
      Particle &created) { funcText };

#define B_FUNC(funcText)                                                       \
  ([=](const Particle &A, const Particle &B,                                   \
       const pSystem &system) { return static_cast<bool>(funcText); })

#define B_FUNC2(funcText)                                                      \
  ([=](Particle &A, Particle &B, pSystem &system) { funcText })

#define ADD_SPECIES(__ARG)                                                     \
  species __ARG;                                                               \
  __ARG.name = #__ARG

#define ADD_PROPERTY(_NAME, _SPECIES, _TYPE, _DEFAULT)                         \
  _NAME = _SPECIES.addProperty<_TYPE>(_DEFAULT);                               \
  _NAME.name = #_NAME

struct as_A_struct {
  Particle spec;
  as_A_struct operator()(const Particle input);
};
extern struct as_A_struct as_A;

struct as_B_struct {
  Particle spec;
  as_B_struct operator()(const species input) {
    spec = input;
    return *this;
  }
};
extern as_B_struct as_B;

struct create_new_struct {
  Particle spec;
  create_new_struct operator()(const Particle input) {
    spec = input;
    return *this;
  }
};
extern create_new_struct create_new;

struct reaction {
  double rate = -1;
  double &barrier = rate;
  pSystem *sys;
  Particle A_spec;
  Particle B_spec;

  bool firstOrder = false;
  bool secondOrder = false;
  bool zeroOrder = false;

  void reg();

  reaction(create_new_struct s) {
    zeroOrder = true;
    A_spec = s.spec;
    reg();
  };

  reaction(as_A_struct A_s) {
    firstOrder = true;
    A_spec = A_s.spec;
    reg();
  };

  reaction(as_A_struct A_s, as_B_struct B_s) {
    secondOrder = true;
    A_spec = A_s.spec;
    B_spec = B_s.spec;
    reg();
  };
  std::function<double(const Particle &, const Particle &, const pSystem &)>
  rateFunc = [](const Particle &A, const Particle &B, const pSystem &system) {
    throw std::runtime_error("There was no rate specified!\n");
    return -1;
  };

  std::function<bool(const Particle &, const Particle &, const pSystem &)>
  reacPossibleFunc = B_FUNC(true);

  std::function<void(Particle &, Particle &, pSystem &)> afterFuncA =
      G_FUNC(while (false) {});

  std::function<void(Particle &, Particle &, pSystem &)> afterFuncB =
      G_FUNC(while (false) {});

  std::vector<Particle> create_in_A_vec;
  std::vector<Particle> create_at_A_vec; // TODO: implement that

  std::function<void(Particle &, Particle &, pSystem &, Particle &)>
  create_atA_func = C_FUNC(while (false) {}) std::function<
      void(Particle &, Particle &, pSystem &, Particle &)> create_inA_func =
      C_FUNC(while (false) {}) std::function<
          void(Particle &, Particle &, pSystem &, Particle &)> create_atB_func =
          C_FUNC(while (false) {}) std::function<void(
              Particle &, Particle &, pSystem &, Particle &)> create_inB_func =
              C_FUNC(while (false) {}) std::vector<Particle> create_in_B_vec;
  std::vector<Particle> create_at_B_vec; // TODO: implement that

  void createAt_A(const Particle &P) { create_at_A_vec.emplace_back(P); }
  void createIn_A(const Particle &P) { create_in_A_vec.emplace_back(P); }
  void createAt_B(const Particle &P) { create_at_B_vec.emplace_back(P); }
  void createIn_B(const Particle &P) { create_in_B_vec.emplace_back(P); }

  bool specialBehavior = false;

  Particle otherDivide;
  double divisionTime = 10;
  bool doesDivide = false;
  void divide(const as_B_struct spec, double divTime) {
    doesDivide = true;
    otherDivide = spec.spec;
    divisionTime = divTime;
    std::cout << " div time " << divTime << "\n";
  }

  bool isAinB = false;
  void AinB() {
    assert(!(isAinB || isBinA || isAdead || isBdead));
    assert(!specialBehavior);
    specialBehavior = true;
    isAinB = true;
  }

  bool isBinA = false;
  void BinA() {
    assert(!(isAinB || isBinA || isAdead || isBdead));
    assert(!specialBehavior);
    specialBehavior = true;
    isBinA = true;
  }

  bool isAdead = false;
  void Adead() {
    assert(!(isAinB || isBinA || isAdead));
    assert(!specialBehavior);
    isAdead = true;
  }

  bool isBdead = false;
  void Bdead() {
    assert(!specialBehavior);
    isBdead = true;
  }

  bool isAleaveB = false;
  void AleaveB() {
    assert(!specialBehavior);
    specialBehavior = true;
    isAleaveB = true;
  }

  bool isBleaveA = false;
  void BleaveA() {
    assert(!specialBehavior);
    specialBehavior = true;
    isBleaveA = true;
  }

  double customOverlapp = -1;
  void setOverlapp(double overapp) {
    assert(overapp > 0.);
    assert(overapp <= 1.);
    customOverlapp = overapp;
    assert(!specialBehavior);
    // specialBehavior = true;
  }
};

bool chance(double likelyhood);

int nr_in(const species &spec, const Particle &ptcl);
int nr_in_system(const species &spec);

const double &radius(const Particle &s);
double &radius(Particle &s);
// const double &radius(const species &);

// const double &radius2(const Particle &s);

struct surface_struct {
  surface_struct operator()(Particle &s);
  double operator()(const Particle &s) const;
  void operator=(const double &);
  operator double() const;
  Particle *ptr = nullptr;
  const std::string name = "surface";
  const Particle *c_ptr = nullptr;
};
extern surface_struct surface;

struct volume_struct {
  volume_struct operator()(Particle &s);
  double operator()(const Particle &s) const;
  void operator=(const double &);
  // void operator=(const volume_struct &);//neu hinzugef�gt
  // void operator+(const volume_struct &);//neu hinzugef�gt
  operator double() const;
  const std::string name = "volume";
  Particle *ptr = nullptr;
  const Particle *c_ptr = nullptr;
};
extern volume_struct volume;

int &colorType(Particle &s);
const int &colorType(const Particle &s);

std::vector<ptclid_t> putSomewhere(const species &s, size_t nr = 1);

void putInside(std::vector<ptclid_t>, const species, size_t nr = 2);

void putInsideAt(std::vector<ptclid_t>, species, flt_t, flt_t, flt_t = 0.);

template <typename T> void average_prop(const species &, propWrapper<T>);

void setSize(const double length);
void set_temperature(const double temp);
void set_viscocity(const double visco);

void setupSimulation();

void visualize(const int framerate);

void write_to_file();
void write_header();

void maxInternalFramerate(int val);

void changeSpecies(Particle &A, const species &spec);

void runNoVisual(const size_t nrOfSteps);

std::vector<ptclid_t> putHere(const species, const flt_t, const flt_t, flt_t);

void add_force(
    std::function<Vec(const pSystem &, const Particle &A)> forceExpr,
    std::function<bool(const Particle &, const Particle &, const pSystem &)>
        possibleFunc = [](const Particle &A, const Particle &B,
                          const pSystem &sys) { return true; });

namespace observe {
template <typename T>
void average_prop(const species &spec, const propWrapper<T> &prop);

/*template <typename T>
void average_prop_if(const species &spec, const propWrapper<T>
&prop,std::function<bool(const Particle& A, const Particle& B , const pSystem&
system)> func);*/

template <typename T>
void average_prop_if(const species &spec, T prop,
                     std::function<bool(const Particle &A, const Particle &B,
                                        const pSystem &system)> if_func) {
  auto type = spec.type;
  std::function<std::string(const pSystem &)> func = [=](const pSystem &sys) {
    size_t counter = 0;
    double val = 0;
    for (const auto &p : sys.data) {
      if ((p.second.type == type) && (if_func(p.second, p.second, sys))) {
        counter++;
        val += prop(p.second);
      }
    }
    if (counter == 0) {
      return std::string("ERR");
    }
    return std::to_string(val / counter);
  };
  std::string name = "avr_" + prop.name + "(" + spec.name + ")";
  propertyManagement::observations.push_back(std::make_pair(name, func));
}

template <typename T> void average_prop(const species &spec, T prop) {
  auto type = spec.type;
  std::function<std::string(const pSystem &)> func = [=](const pSystem &sys) {
    size_t counter = 0;
    double val = 0;
    for (const auto &p : sys.data) {
      if (p.second.type == type) {
        counter++;
        val += prop(p.second);
      }
    }
    if (counter == 0) {
      return std::string("ERR");
    }
    return std::to_string(val / counter);
  };
  std::string name = "avr_" + prop.name + "(" + spec.name + ")";
  propertyManagement::observations.push_back(std::make_pair(name, func));
}
// template <typename T> void average_prop(const species &spec, T prop);

struct nr_of_struct {
  void operator()(const species &s);
  void operator()(const species &s,
                  std::function<bool(const Particle &A, const Particle &B,
                                     const pSystem &system)>);
};
extern nr_of_struct nr_of;

struct nr_of_A_struct {
  void operator()(
      const species &s,
      std::function<bool(const Particle &, const Particle &, const pSystem &)>);
};
extern nr_of_A_struct nr_of_A;
} // namespace observe
} // namespace dsl
