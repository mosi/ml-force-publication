#include "simulator/s_particle.h"
#include "utilities/Utilities.h"
#include "utilities/mVec.h"
#include <string>
#include <vector>

#pragma once

extern double sysRadDec;

void normalize(std::vector<std::pair<ptclid_t, ptclid_t>> &a);
bool equivalent(std::vector<std::pair<ptclid_t, ptclid_t>> a,
                std::vector<std::pair<ptclid_t, ptclid_t>> b);
// check if all a's are in b
bool contains(std::vector<std::pair<ptclid_t, ptclid_t>> a,
              std::vector<std::pair<ptclid_t, ptclid_t>> b);
bool fullyContained(s_particle p, flt_t s);
std::vector<s_particle> randomSystem(ptclid_t size, flt_t systemSize);
std::vector<s_particle> randomStepdSystem(ptclid_t size, flt_t systemSize);
std::vector<s_particle> superNestedSystem(ptclid_t size, flt_t systemSize);
std::vector<s_particle> nestedSystem(ptclid_t size, flt_t systemSize);
bool checkSystemValid(const std::vector<s_particle> &dat, ptclid_t size,
                      flt_t systemSize);
void toFile(const std::vector<s_particle> dat, std::string str);
void relaxSystem(std::vector<s_particle> &, flt_t, size_t);

bool noDublicates(std::vector<std::pair<ptclid_t, ptclid_t>>);
void
removeDublicatesAndNormalize(std::vector<std::pair<ptclid_t, ptclid_t>> &a);

flt_t how_much_contained(std::vector<std::pair<ptclid_t, ptclid_t>>,
                         std::vector<std::pair<ptclid_t, ptclid_t>>);
