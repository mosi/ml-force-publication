#pragma once

// forward declarations to avoid circular dependencies when compiling
struct pSystem;

#include "simulator/particle.h"
#include "simulator/reactions.h"
#include "utilities/Utilities.h"
#include "utilities/mVec.h"
#include "utilities/ttimer.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#undef NDEBUG
#include <cassert>

struct pSystem {
  /* the different kinds of reaction stored in the system */
  std::vector<BiMolecularReaction> biReactions;
  std::vector<UniMolecularReaction> uniReactions;
  std::vector<ZeroMolecularReaction> zeroReactions;

  ttimer collisionTime;
  ttimer gausNumberTime;
  ttimer listHandelTime;
  ttimer intTime;
  ttimer lowReacTime;
  ttimer biReacTime;
  ttimer zeroReacTime;

  std::vector<std::function<void(pSystem &)>> generalFunc;
  std::vector<std::function<void(pSystem &, Particle &)>> forceFunc;

  /**this removes the copy constructability. The problem is that particels
  remember the system via address.
  you could easaly write a copy constructure, manually bending pointers, but I
  just haven't  done that jet.*/
  pSystem &operator=(pSystem const &) = delete;
  pSystem(const pSystem &x) = delete;
  pSystem() {} // if default constructors are deleted you need to manually
               // provide one to create such an object

  double timestep = -1; // the numerical integration timestep
  // the largest id of any particel that ever was in the system. nedded to
  // assign new ids
  int maxIDused = -1;

  // needed to enumerate outputfiles. mutable as system state doesn't change
  mutable size_t print_counter = 0;
  // the time the system has advanced to
  double time = 0;
  // the (rectangular) size of the system. Inifnite if negative
  Vec systemSize = Vec(-1);
  // how often should neghbrs be renewd? If negative no negihbor optimization
  int neighborsRenewNumber = 1;
  // the number of steps already performed
  size_t stepCounter = 0;
  // the speed of the fastest particle in the system (recalculated after every
  // step)
  flt_t maxSpeed = 1e10;

  // A list of all collisions in the system
  std::vector<std::pair<ptclid_t, ptclid_t>> collisionlist;

  // A to be becomeEnclosed in B
  void becomeEnclosed(Particle &A, Particle &B) {
    assert(A.parentId == B.parentId);
    if (A.parentId != -1) {
      get(A.parentId)->removeChild(A.id);
    }
    B.enclosed.push_back(A.id);
    A.parentId = B.id;
  }
  ptclid_t addToSystem(const Particle &A);
  // A to leave B
  void becomeFree(Particle &A, Particle &B) {
    assert(A.parentId == B.id);
    B.removeChild(A.id);
    A.parentId = B.parentId;
    if (A.parentId != -1) {
      this->get(A.parentId)->enclosed.push_back(A.id);
    }
  }

  // copy the ignore state from A to B
  void copyIgnoreState(Particle &A, Particle &B) {
    copyIgnoreState1(A, B);
    copyIgnoreState2(A, B);
  }

  void copyIgnoreState1(Particle &A, Particle &B) {
    for (auto &iid : A.is_ignoring_ids) {
      if (iid > maxIDused) {
        throw std::runtime_error("id to high");
      }
      B.is_ignoring_ids.push_back(iid);
    }
  }
  void copyIgnoreState2(Particle &A, Particle &B) {
    for (auto &iid : A.is_ignoring_ids) {
      get(iid)->is_ignored_by_ids.push_back(A.id);
    }
  }

  ////function declaritions (see system.cpp)

  std::vector<std::pair<std::pair<ptclid_t, ptclid_t>,
                        std::function<bool(Particle &, Particle &, pSystem &)>>>
  pairFunctions;

  // ptclid_t id2idx(const ptclid_t find_id) const;
  Particle const *get(const ptclid_t &find_id) const;
  Particle *get(const ptclid_t &find_id);
  void removeFromSystem(const ptclid_t find_id);
  void removeFromSystem(std::vector<Particle>::iterator find_iter);
  void updateCollisionlist();
  ptclid_t size() const;
  void print() const;
  ptclid_t addWhereFree(Particle A, ptclid_t nrOf = 1);
  ptclid_t addInside(const ptclid_t, Particle A, ptclid_t nrOf = 1);
  ptclid_t addForce(Particle A);
  int addIfFree(Particle A);
  ptclid_t addApprox(Particle A, double);
  ptclid_t addApproxHere(Particle A, Vec pos, double);
  void do_step();
  double calcDistance(const Particle &A, const Particle &B) const;
  double calcOuterDistance(const Particle &A, const Particle &B) const;
  double calcReactDistance(const Particle &A, const Particle &B) const;
  void updateNeighbors();
  void testAndPerformReactions();
  void calculateInteractionForces();
  ZeroMolecularReaction *addZeroMolecularReaction();
  UniMolecularReaction *addUniMolecularReaction();
  BiMolecularReaction *addBiMolecularReaction();
  double Volume() const;
  void allEdgesTo(double length);
  void performZeroOrderReactions();
  double getKineticEnergy();
  void performUnimolecularReactions();
  Vec direction(const Particle &A, const Particle &B) const;
  void boxify();
  double nonReactForce(double x, const Particle &A, const Particle &B);
  double reactForce(double dist, double rate, const Particle &A,
                    const Particle &B, const double overlap);
  void print(std::ofstream &file, const Particle p) const;
  void setTimestep(double diffusion, double radius);
  void reset();

  double systemTemp = -1;
  double systemVisco = -1;
  double stepness = 40;

  // private: this is a big TODO it would make the programm easier to extent
  std::map<ptclid_t, Particle> data; // Contains all the particels in the system
};
