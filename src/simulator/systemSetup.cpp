#include "utilities/mVec.h"
//#include "gpu_plot.h"
#include "collision/collisionCPU.h"
#include "simulator/s_particle.h"
#include "utilities/Utilities.h"
#include <algorithm>
#include <fstream>
#include <future>
#include <iostream>
#include <string>
#include <vector>

#include "simulator/systemSetup.h"

double sysRadDec;

void normalize(std::vector<std::pair<ptclid_t, ptclid_t>> &a) {
  for (auto &p : a) {
    if (p.first > p.second) {
      std::swap(p.first, p.second);
    }
  }
  std::sort(a.begin(), a.end());
}
bool equivalent(std::vector<std::pair<ptclid_t, ptclid_t>> a,
                std::vector<std::pair<ptclid_t, ptclid_t>> b) {
  if (a.size() != b.size()) {
    return false;
  }

  normalize(a);
  normalize(b);

  for (ptclid_t idx = 0; idx != a.size(); idx++) {
    if (a[idx].first != b[idx].first)
      return false;
    if (a[idx].second != b[idx].second)
      return false;
  }
  return true;
}

// check if all a's are in b
bool contains(std::vector<std::pair<ptclid_t, ptclid_t>> a,
              std::vector<std::pair<ptclid_t, ptclid_t>> b) {
  assert(a.size() <= b.size());
  normalize(a);
  normalize(b);

  return std::includes(b.begin(), b.end(), a.begin(), a.end());
}

bool noDublicates(std::vector<std::pair<ptclid_t, ptclid_t>> a) {
  normalize(a);
  return (std::adjacent_find(a.begin(), a.end()) == a.end());
}

void
removeDublicatesAndNormalize(std::vector<std::pair<ptclid_t, ptclid_t>> &a) {
  normalize(a);
  a.erase(std::unique(a.begin(), a.end()), a.end());
  assert(noDublicates(a));
}

// checks how much of a is in b
flt_t how_much_contained(std::vector<std::pair<ptclid_t, ptclid_t>> a,
                         std::vector<std::pair<ptclid_t, ptclid_t>> b) {
  normalize(a);
  normalize(b);
  ptclid_t ret = 0;
  ptclid_t notFound = 0;
  for (auto &p : a) {
    if (std::find(b.begin(), b.end(), p) != b.end()) {
      ret++;
    } else {
      notFound++;
      std::cout << notFound << "/" << ret << " not found " << p.first << " "
                << p.second << "\n";
    }
  }
  assert(notFound + ret == a.size());
  return 1. * ret / (1. * a.size());
}

bool fullyContained(s_particle p, flt_t s) {
  for (auto &px : p.pos) {
    if (px - p.radius <= 0) {
      return false;
    }
    if (px + p.radius >= s) {
      return false;
    }
  }
  return true;
}

void fixIdx(std::vector<s_particle> &d) {
  for (ptclid_t idx = 0; idx != d.size(); idx++) {
    d[idx].id = idx;
  }
}

std::vector<s_particle> randomSystem(ptclid_t size, flt_t systemSize) {
  std::vector<s_particle> data(size);
  for (auto &p : data) {
    do {
      for (auto &px : p.pos) {
        px = systemSize * util::randomZeroOne();
      }
      auto rd = util::randomPMone();
      if (rd > .90) {
        p.radius = 3;
      } else {
        p.radius = .3; // util::randomExp()*2 + .1; // abs(util::gaus(2)) + .1;
      }
      p.radius = util::randomExp();
      //} while (p.radius > .20 * systemSize);
    } while (!fullyContained(p, systemSize) || p.radius > .05 * systemSize);

    p.inner_radius = (1.) * p.radius;
  }
  fixIdx(data);

  // relaxSystem(data, systemSize, 10000);
  return data;
}

std::vector<s_particle> randomStepdSystem(ptclid_t size, flt_t systemSize) {
  flt_t minSize = .4;
  std::vector<s_particle> data(size);
  for (ptclid_t idx = 0; idx < size; idx++) {
    auto &p = data[idx];
    if (idx < size * .1) {
      for (auto &px : p.pos) {
        px = systemSize * util::randomZeroOne();
      }
      p.radius = util::randomZeroOne() * 10 + minSize;
      p.inner_radius = .99 * p.radius;
    } else {
      p.radius = minSize;
      p.inner_radius = p.radius * .9;
      for (auto &px : p.pos) {
        px = systemSize * util::randomZeroOne();
      }
    }
  }
  fixIdx(data);
  return data;
}

std::vector<s_particle> superNestedSystem(ptclid_t size, flt_t systemSize) {
  std::vector<s_particle> data(size);
  std::vector<Vec> pos;
  fixIdx(data);
  const flt_t f2 = .1 * systemSize;
  assert(DIMENSION == 3);
  for (int x : { 0, 1, 2, 3, 4 }) {
    for (int y : { 0, 1, 2, 3, 4 }) {
      for (int z : { 0, 1, 2, 3, 4 }) {
        Vec res;
        res[0] = f2 * (2 * x + 1);
        res[1] = f2 * (2 * y + 1);
        res[2] = f2 * (2 * z + 1);
        pos.push_back(res);
      }
    }
  }

  // int posIdx = 0;
  flt_t radius = .1 * systemSize;
  flt_t faktor = sysRadDec;
  for (ptclid_t idx = 0; idx < size; idx++) {
    auto &p = data.at(idx);
    p.radius = radius;
    p.pos = pos[idx % pos.size()];
    p.inner_radius = radius;
    radius *= faktor;
  }
  fixIdx(data);
  return data;
}

std::vector<s_particle> nestedSystem(ptclid_t size, flt_t systemSize) {
  std::vector<s_particle> data(size);
  assert(size > 0);
  assert(systemSize > 0);
  ptclid_t nrInside = 3;
  ptclid_t idx = 1;
  ptclid_t lastStart = 0;
  for (auto &x : data.at(0).pos) {
    x = .5 * systemSize;
  }
  data[0].radius = .4 * systemSize;
  data[0].inner_radius = .95 * .4 * systemSize;
  ptclid_t lastStop = 1;
  while (idx < data.size()) {
    for (ptclid_t c = lastStart; c != lastStop; c++) {
      if (idx >= data.size()) {
        break;
      }
      for (ptclid_t lowerC = 0; lowerC < nrInside; lowerC++) {
        assert(idx < data.size());
        assert(c < data.size());
        data.at(idx).radius =
            (.01 * util::randomZeroOne() + .6) * data.at(c).inner_radius;
        for (ptclid_t dim = 0; dim < data.at(idx).pos.dim(); dim++) {
          data.at(idx).pos[dim] =
              data.at(c).pos[dim] +
              1. * util::randomPMone() *
                  (data.at(c).inner_radius - data[idx].radius);
        }
        /*std::cout <<idx<<" "<< data[idx].radius << "\t" <<
         data[c].inner_radius <<"("<<
         lastStart<<"->"<<lastStop<< "\n";*/
        data.at(idx).inner_radius = .999 * data[idx].radius;
        idx++;
        if (idx >= data.size()) {
          break;
        }
      }
      lastStart = lastStop;
      lastStop = idx;
    }
  }

  return data;
}

bool checkSystemValid(const std::vector<s_particle> &dat, ptclid_t size,
                      flt_t systemSize) {
  if (size != dat.size())
    return false;

  for (auto &x : dat) {
    if (!fullyContained(x, systemSize)) {
      // no longer true
      // return false;
    }
    for (ptclid_t dim = 0; dim < dat.at(0).pos.dim(); dim++) {
      auto &px = x.pos[dim];
      if (px < 0) {
        std::cout << "below 0\n";
        return false;
      }
      if (px > systemSize) {
        std::cout << "larger system\n";
        return false;
      }
    }
  }
  return true;
}

void relaxSystem(std::vector<s_particle> &dat, flt_t systemSize, size_t iter) {
  auto dat2 = dat;
  auto ptr1 = &dat;
  auto ptr2 = &dat2;
  double temp = 3;
  std::vector<std::future<void>> futs;
  double disp = std::min(1., .1 * systemSize);
  int res1 = FRNN_cpu_omp(*ptr1, 0, systemSize).size();
  for (size_t count = 0; count < iter; count++) {
    int acc = 0;
    temp *= .9;

    // removed to run on Windows:
    // futs.push_back(createPlotAsync(*ptr1, systemSize, "testplot", count));
    // systemSize != 1.01;
    for (int c = 0; c < 100; c++) {
      *ptr2 = *ptr1;
      for (auto &p : *ptr2) {
        for (auto &x : p.pos) {
          x += util::randomPMone() * disp;
          while (x < 0) {
            x += systemSize;
          }
          while (x >= systemSize) {
            x -= systemSize;
          }
          assert(x <= systemSize);
        }
      }

      const int res2 = FRNN_cpu_omp(*ptr2, 0, systemSize).size();

      std::cout << disp << " ->  " << res1 << " v " << res2 << " ";
      if (res2 < res1) {
        std::swap(ptr1, ptr2);
        res1 = res2;
        std::cout << "akk\n";
        acc++;
      } else {
        // std::cout << (res1 - res2) / temp << " = " << res2 << " - " << res1;
        if (exp((res1 - res2) / temp) > util::randomZeroOne()) {
          std::swap(ptr1, ptr2);
          res1 = res2;
          acc++;
          std::cout << "akk\n";
        } else {
          std::cout << "nak\n";
        }
      }
    }
    if (acc >= 60) {
      disp *= 1.5;
    } else if (acc <= 40) {
      disp /= 1.5;
    }
  }
  for (auto &x : futs)
    x.get();
}

void toFile(const std::vector<s_particle> dat, std::string str) {
  std::ofstream file;
  file.open(str + "-" + std::to_string(DIMENSION) + "-" +
            std::to_string(dat.size()) + ".dat");

  for (const auto &p : dat) {
    file << p.id << "\t";
    for (const auto &x : p.pos) {
      file << x << "\t";
    }
    file << p.radius << "\t" << p.inner_radius << "\n";
  }
  file.close();
}
