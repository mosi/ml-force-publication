
/**
Here the main part of the simulator the system is defined. It contains all the
particels as well as the reactions.
*/

#include "system.h"
#include "collision/gpuCon.h"
#include "utilities/Utilities.h"
#include "utilities/ttimer.h"
#include <algorithm>
#include <limits.h>
#include <sstream>

// #include "valgrind/callgrind.h"

using std::vector;

///////////////////////////////////////////////////////
//       Functions pretaining to particAle storage
///////////////////////////////////////////////////////

/**
Each particle has a unique id. This function translates this id to the index of
the particle in the data structure
As particles are contained in data in order of ascending id, a binary search is
performed
*/

/*ptclid_t pSystem::id2idx(const ptclid_t find_id) const {
  if (data.size() == 1) {
    if (data.back().id == find_id) {
      return 0;
    } else {
      throw std::runtime_error("not found in one");
    }
  }

  if (data.size() <= 100000) {
    for (int x = 0; x < 100000; x++) {
      if (data[x].id == find_id) {
        return x;
      }
    }
    throw std::runtime_error("not found in few");
  }

  assert(data.size() > 0);
  ptclid_t lowerLimit = 0;
  ptclid_t upperLimit = data.size() - 1;
  // std::cout << "find :" << find_id << " limit " << data[upperLimit].id <<
  // "\n";
  if (find_id <= data[upperLimit].id) {
    std::cerr << " Can not find ID " << find_id
              << " in system\nCurrently there are " << data.size() << "\n";
    for (auto &x : data) {
      std::cerr << x.id << " ";
    }
    throw std::runtime_error("Can not find particle\n");
  }
  ptclid_t test = std::min(find_id, upperLimit);

  size_t counter = 0;

  assert(data[lowerLimit].id <=
         find_id); // ietarniea tr atirenas  tr   tariena t
  assert(data[upperLimit].id >= find_id);
  assert(lowerLimit < upperLimit);
  assert(test >= lowerLimit);
  assert(test <= upperLimit);
  while (data[test].id != find_id) {
    if (data[test].id < find_id) {
      lowerLimit = test;
    } else {
      upperLimit = test;
    }
    assert(data[lowerLimit].id <= find_id);
    assert(data[upperLimit].id >= find_id);
    assert(lowerLimit <= upperLimit);
    test = (lowerLimit + upperLimit) / 2;
    assert(test >= lowerLimit);
    assert(test <= upperLimit);
      std::cerr << " ERROR: can not find an object you are looking for\n";
      throw std::runtime_error("Can not find particle\n");
    }
  }
  return test;
}*/

/**return a mutable iterator to a particle based on its id.
 */
Particle const *pSystem::get(const ptclid_t &find_id) const {
  const auto &ptcl = data.find(find_id);

  if (ptcl == data.end()) {
    throw std::runtime_error("particle not found\n");
  }

  return &ptcl->second;
}

/**returns a nonmutable iterator to a prticle based on its id*/
Particle *pSystem::get(const ptclid_t &find_id) {
  const auto &ptcl = data.find(find_id);

  if (ptcl == data.end()) {
    throw std::runtime_error("particle not found\n");
  }

  return &ptcl->second;
}

// removes a particle from storage via its id
void pSystem::removeFromSystem(const ptclid_t find_id) {
  auto find_iter = get(find_id);

  // std::cout << " --\n--REMOVING " << find_iter->id;
  if (find_iter->parentId != -1) {
    // std::cout << " has parent\n";
    auto parent = get(find_iter->parentId);
    parent->removeChild(find_iter->id);
  } else {
    // std::cout << " has no parent\n";
  }

  if (!find_iter->is_ignored_by_ids.empty()) {
    assert(false);
    throw std::runtime_error(
        "removing particle that is currently splitting is not allowed!");
  }

  for (auto &iid : find_iter->is_ignoring_ids) {
    auto &otherP = *get(iid);
    // std::cout << " removing ignore for " << iid << "\n";
    otherP.is_ignored_by_ids.erase(std::remove(otherP.is_ignored_by_ids.begin(),
                                               otherP.is_ignored_by_ids.end(),
                                               find_iter->id),
                                   otherP.is_ignored_by_ids.end());
  }

  // TODO not very efficent ... can do better

  auto endCollList = std::remove_if(collisionlist.begin(), collisionlist.end(),
                                    [=](std::pair<ptclid_t, ptclid_t> par) {
    return SafeEqual(par.first, find_iter->id) ||
           SafeEqual(par.second, find_iter->id);
  });

  collisionlist.resize(endCollList - collisionlist.begin());

  auto newLast =
      std::remove_if(pairFunctions.begin(), pairFunctions.end(),
                     [=
  ](const std::pair<std::pair<ptclid_t, ptclid_t>,
                     std::function<void(Particle &, Particle &, pSystem &)>> &
                         pr) {
        return SafeEqual(find_iter->id, pr.first.first) ||
               SafeEqual(find_iter->id, pr.first.second);
      });
  if (newLast != pairFunctions.end()) {
    std::cout << "removing particle involved in pairfunction" << find_iter->id
              << " of " << pairFunctions.size() << "\n";
    for (const auto &x : pairFunctions) {
      std::cout << "  -> bevor: " << x.first.first << " and " << x.first.second
                << "\n";
    }
    pairFunctions.erase(newLast, pairFunctions.end());
    for (const auto &x : pairFunctions) {
      std::cout << "  -> remain: " << x.first.first << " and " << x.first.second
                << "\n";
    }
  }

  /*for (auto id : find_iter->neighbors) {
    get(id)->removeNeighbor(find_iter->id);
  }*/
  data.erase(data.find(find_id));
}

// returns the number of particels in the system
ptclid_t pSystem::size() const { return data.size(); }
/* This looks for a free places to put particels and adds them.
A is used as a template. nrOf says how many particels to add.
*/
ptclid_t pSystem::addWhereFree(Particle A, ptclid_t nrOf) {
  int newId = -1;
  for (ptclid_t i = 0; i < nrOf; i++) {
    size_t emerencyCounter = 0; // to check for overcrowding
    // do look at random position while position occupied
    newId = -1;
    do {
      // count if impossible
      emerencyCounter++;
      const size_t maxTries = 1000000;
      if (emerencyCounter > maxTries) {
        // fail to overcrowded
        std::stringstream msg_s;
        std::cerr << "RUNTIME ERROR\nIt took more than " << maxTries
                  << " tries, to add a particle add a random free place."
                  << " This means, that the system is to overcrowded."
                  << " add the point of failer a system of Dimensions "
                  << systemSize << " contained " << size() << " particels.";
        throw std::runtime_error(
            "To crowded for adding particels (addWhereFree)");
      }
      // set position randomly
      for (int x = 0; x < DIMENSION; x++) {
        if (systemSize[x] <= 0) {
          throw std::runtime_error(
              "You can not add an object to a volumeless system");
        }
        A.position[x] = util::randomZeroOne() * fabs(systemSize[x]);
      }
      newId = addIfFree(A);
      // std::cout << " newID: " << newId << "\n";
    } while (newId < 0);
    // a free position has been found :) particle has been put there
  }
  return newId;
}

ptclid_t pSystem::addInside(const ptclid_t outerID, Particle A, ptclid_t nrOf) {
  int newId = -1;
  for (ptclid_t i = 0; i < nrOf; i++) {
    auto outerParticle = get(outerID);
    newId = -1;
    copyIgnoreState(*outerParticle, A);

    size_t emerencyCounter = 0; // to check for overcrowding
    // do look at random position while position occupied

    do {
      // count if impossible
      emerencyCounter++;
      const size_t maxTries = 100000;
      if (emerencyCounter > maxTries) {
        // fail to overcrowded
        std::stringstream msg_s;
        std::cerr << "RUNTIME ERROR\nIt took more than " << maxTries
                  << " tries, to add a particle add a random free place."
                  << " This means, that the system is to overcrowded."
                  << " add the point of failer a system of Dimensions "
                  << systemSize << " contained " << size() << " particels.";
        throw std::runtime_error("To crowded for adding particels (addInside)");
      }
      // set position randomly
      for (int x = 0; x < DIMENSION; x++) {
        if (systemSize[x] <= 0) {
          throw std::runtime_error(
              "You can not add an object to a volumeless system");
        }
        A.position[x] = outerParticle->position[x] +
                        util::randomPMone() * outerParticle->radius;
      }
      A.parentId = outerID;
      newId = addIfFree(A);
    } while (newId < 0);
    get(A.parentId)->enclosed.push_back(newId);

    // a free position has been found :) particle has been put there
  }
  return newId;
}

/* This expects a Particle A with a specified locaton. If the location is free,
it return true and adds A to the sytem,
els false is returned*/
int pSystem::addIfFree(Particle A) {
  for (auto &x : A.position) {
    assert(!std::isnan(x));
  }

  // Id needs to always be set, if calculating Distances, since the
  // distancefunctions might use it
  A.id = ++maxIDused;
  A.containingSystem = this;

  if (A.radius < 0) {
    A.radius = A.targetRadius;
  }
  assert(A.radius > 0);

  if (maxIDused > INT_MAX - 100) {
    throw std::runtime_error("IDs are leaving int range. Too many particles "
                             "have been generated throughout the runtime.");
  }

  assert(size() < ptclid_t_max - 5);

  // check if this position is free:
  /*bool collision = false;
  for (const auto &x : data) {
    auto &ptcl = x.second;
    if ((calcOuterDistance(ptcl, A) <= 0) && !(A.isIgnoring(ptcl.id)) &&
        !(ptcl.isIgnoring(A.id))) {
      collision = true;
      break;
    }
  }*/
  if (std::any_of(data.begin(), data.end(),
                  [&](decltype(*(data.begin())) & p)->bool {
        const auto &ptcl = p.second;
        return (calcOuterDistance(ptcl, A) <= 0) && !(A.isIgnoring(ptcl.id)) &&
               !(ptcl.isIgnoring(A.id));
      })) {

    // not free so return false
    maxIDused--; // maxId not actually used
    return -1;
  } else {
    // free so add particle
    return addToSystem(A);
  }
}

ptclid_t pSystem::addToSystem(const Particle &A) {
  assert(A.id == maxIDused);
  auto addedIter = data.insert(std::make_pair(A.id, A));
  assert(addedIter.second == true);
  Particle *ptcl = &(addedIter.first->second);
  ptcl->setup();
  // std::cout << "have added ";
  // ptcl->writeInfo();
  // std::cout << "\n";
  // std::cout << "Add to system returns " << ptcl->id << "\n";
  return ptcl->id;
}

ptclid_t pSystem::addForce(Particle A) {
  A.id = ++maxIDused;
  A.containingSystem = this;

  if (A.radius < 0) {
    A.radius = A.targetRadius;
  }
  assert(A.radius > 0);

  assert(size() < ptclid_t_max - 5);

  return addToSystem(A);
}

ptclid_t pSystem::addApprox(Particle A, double search_var) {
  // auto originalPos = A.position;
  size_t emerencyCounter = 0;

  assert(A.targetRadius > 0);

  int newId = -1;
  while (newId < 0) {
    newId = addIfFree(A);
    if (emerencyCounter++ > 1000000) {
      // fail to overcrowded
      std::stringstream msg_s;
      std::cerr << "RUNTIME ERROR (add here)\nIt took more than " << 100000
                << " tries, to add a particle add a random free place."
                << " This means, that the system is to overcrowded."
                << " add the point of failer a system of Dimensions "
                << systemSize << " contained " << size() << " particels.\n";

      std::cerr << " The particle is ignoring " << A.is_ignoring_ids.size()
                << " Particles\n";
      for (auto &x : A.is_ignoring_ids) {
        std::cout << A.id << " -> " << x << " \n";
      }
      throw std::runtime_error("To crowded for adding particels (addApprox)");
    }
    // std::cout << " Pos test is " << A.position << "\n";
    A.position +=
        util::randomGausVec(search_var / 10); // config::ApproxaddVar);
  }
  // std::cout << "  #addApprox: " << emerencyCounter << " tries.\n";
  return newId;
}

ptclid_t pSystem::addApproxHere(Particle A, Vec pos, double search_var) {
  A.position = pos;
  assert(A.targetRadius > 0);
  return addApprox(A, search_var);
}

void pSystem::reset() {
  biReactions.clear();
  uniReactions.clear();
  zeroReactions.clear();

  generalFunc.clear();

  timestep = -1;
  maxIDused = -1;

  time = 0;
  stepCounter = 0;
  maxSpeed = 1e10;
  collisionlist.clear();

  data.clear();
  throw std::runtime_error(" not implemented ");
  assert(false);
}

/////////////////////////////////////////////////////////////////
//  Functions related to distance calculation between particels
////////////////////////////////////////////////////////////////

/** returs the distance between two particels centers (their "position")
for finite system size, periodic boundary conditions are applied*/
double pSystem::calcDistance(const Particle &A, const Particle &B) const {
  double dist = 0;
  for (size_t i = 0; i < systemSize.dim(); i++) {
    double metaDist = fabs(A.position[i] - B.position[i]);
    if (systemSize[i] > 0) {
      dist += std::pow(std::min(metaDist, fabs(metaDist - systemSize[i])), 2);
    } else {
      dist += std::pow(metaDist, 2);
    }
  }
  return sqrt(dist);
}

/** returns the distance of the outer shell of two particels
        this can be negative, if the particels overlap.
        if one partile is containd within the other particle, the sign is
   revesed.
        The further away from the outer shell (closer to the center) of the
   larger
        containing particle the smaller containd particle is, the larger the
   number is.
        It will get negative, if the contained particles outer shell is
   partially outsite the larger one. */
double pSystem::calcOuterDistance(const Particle &A, const Particle &B) const {
  double dist = calcDistance(A, B) - A.radius - B.radius;
  if (A.parentId == B.id || B.parentId == A.id) {
    return -(dist + 2 * std::min(A.radius, B.radius));
  } else {
    return dist;
  }
}

/** return the reaction distance of two particels
         a reaction can occur if this distance is larger than 0;*/
double pSystem::calcReactDistance(const Particle &A, const Particle &B) const {
  double dist = calcDistance(A, B);
  if (A.parentId == B.id) {
    return B.radius - dist + A.radius;
  } else if (B.parentId == A.id) {
    return A.radius - dist + B.radius;
  }
  return calcOuterDistance(A, B) + 2 * std::min(A.radius, B.radius);
}

/** this returns the direction from one particle to another as a normalized
vector
this follows minimum image convention
if one particel contains the other, this is flipped*/
Vec pSystem::direction(const Particle &from, const Particle &to) const {
  for (auto &x : from.position) {
    assert(!std::isnan(x));
  }
  for (auto &x : to.position) {
    assert(!std::isnan(x));
  }
  // std::cout << "from " << from.position << " to " << to.position << "\n";

  Vec v{ to.position };
  v -= from.position;
  for (size_t i = 0; i < v.dim(); i++) {
    if ((systemSize[i] > 0) && (fabs(v[i]) > systemSize[i] / 2.)) {
      if (v[i] > 0) {
        v[i] -= systemSize[i];
      } else {
        v[i] += systemSize[i];
      }
    }
  }
  if (from.parentId == to.id || to.parentId == from.id) {
    v *= -1.;
  }
  v = v.normalize();
  // std::cout << "v: " << v << " -> abs = " << abs(v) << "\n";
  if (!(abs(v) > 0)) {
    // direction between identical
    v = util::randomGausVec(1.);
    v = v.normalize();
    //  std::cout << " rando v: " << v << " -> abs = " << abs(v) << "\n";
  }
  assert(abs(v) > .99);
  assert(abs(v) < 1.01);
  return v;
}

//////////////////////////////////////////////////////
////////  Other utilities
//////////////////////////////////////////////////////

/* prints the particle to the file. First the parent, then the childern*/
void pSystem::print(std::ofstream &file, const Particle p) const {
  file << p.id << "\ttype: " << p.id << "\tat " << p.position << "\tradius "
       << p.radius << "\n";
  for (const auto &id : p.enclosed) {
    print(file, *get(id));
  }
}

/* prints the whole system to a file. First parents than their childuren.
if printet more than once, the files are automatically named */
void pSystem::print() const {
  std::ofstream file;
  file.open("data/runPos" + std::to_string(print_counter));
  file << "\n#The systems contains " << data.size()
       << " particles, at time = " << time << "\n";

  for (const auto &p : data) {
    if (p.second.parentId == -1) {
      print(file, p.second);
    }
  }
  file.close();

  file.open("data/parent" + std::to_string(print_counter));
  for (const auto &p : data) {
    const auto &ptcl = p.second;

    file << ptcl.position << " " << 10. * ptcl.sumForce + ptcl.position << " 3"
         << "\n";
  }

  file.close();
  print_counter++;
}

/** this function has been used to set the timestep based on the expected rate
 * of diffusion for a small (fast) particle*/
[[deprecated]] void pSystem::setTimestep(double diffusion, double radius) {
  // timestep = std::pow(1. / 2000. * radius * sqrt(6. * diffusion), 2.);
  timestep = radius * radius / (4000 * diffusion);
  // std::cout << "timestep set to: " << timestep << "\n";
}

//////////////////////////////////////////////////////////////////////////////
/////// Functions related to Reactions and other multi particle interaction
//////////////////////////////////////////////////////////////////////////////

/* if particels don't react they just bounce of each other. This is governd by
this function. There are many possible definitions. Exponintial is numerically
uncompftable,
but you needn't really wory about parameters, as it will get step fast.*/
double pSystem::nonReactForce(double dist, const Particle &A,
                              const Particle &B) {
  assert(dist > 0);
  const double radii = A.radius + B.radius;
  const double minRad = std::min(A.radius, B.radius);
  const double minMas = std::min(A.mass, B.mass);
  // const double masses = A.mass + B.mass;

  // Contact between two spheres with identical Young's modulus
  const double E_M = 10. * 1E10 * units::pascal; // Young's modulus
  const double p_r = 0.35;                       // poison's ratio
  const double E_M2 = 2. * E_M / (1 - p_r * p_r);
  const double R = 1. / (1. / A.radius + 1. / B.radius);
  const double forceVal = 4. / 3 * E_M2 * sqrt(R) * sqrt(dist * dist * dist);

  // std::cout << " NonReactForce is " << forceVal << "\n";
  return forceVal * 1E-8;
  // return .1;
  // return pow(1e-2 * x, 2);
  // return exp(dist * constants::stepness) - 1.;
}

double pSystem::reactForce(double dist, double rate, const Particle &A,
                           const Particle &B, const double overlap) {

  /*
  double nrf = nonReactForce(-dist, A, B);
  if (rate <= 1) {
    std::cerr << "Rate is to small for bimolecular reaction potential\n";
    rate = 1;
  }
  auto tmp = 1. / (rate) * nrf;
  // std::cout << " react force is " << tmp << "\n";
  return tmp;
  */
  assert(rate >= 0);
  const double minRad = std::min(A.radius, B.radius);
  auto tmp = rate / (overlap * 2 * minRad);
  return tmp;
}

/* Each particle keeps a list of those particels in close proximity.
This list needs to be update every so often, as done here*/
/*void pSystem::updateNeighbors() {
  // Check if actually usind the neighbor update scheme
  assert(neighborsRenewNumber <= 1);
  if (neighborsRenewNumber <= 1) {
    for (auto &p : data) {
      p.neighbors.clear();
      assert(p.neighbors.size() == 0);
      for (auto &p2 : data) {
        if (p2.id != p.id)
          if (calcOuterDistance(p, p2) <= 0)
            p.neighbors.push_back(p2.id);
      }
    }
  }

  // Renew Naigborlistengs
  else if (stepCounter % static_cast<size_t>(neighborsRenewNumber) == 0 &&
           neighborsRenewNumber > 0) {
    for (auto &p : data) {
      //  p.updateNeighbors();
    }
  }
}*/

/* update the collision list using one of many different methods*/
void pSystem::updateCollisionlist() {
  for (ptclid_t id = 0; id < size(); id++) {
    // data[id].id = id;
  }
  collisionlist = getCollisions(*this);
}

/* Perform all collision based reactions.
Here all reactions are matched with close particels (neighbors).
If the test is positive (reac.testReaktion), then the reaction is executed*/
void pSystem::testAndPerformReactions() {
  std::set<ptclid_t> have_reacted; // Make sure, particles react only once

  std::vector<std::tuple<BiMolecularReaction *, Particle *, Particle *>>
  reactions_to_do;

  for (auto &col : collisionlist) {
    if ((have_reacted.find(col.first) == have_reacted.end()) &&
        (have_reacted.find(col.second) == have_reacted.end())) {

      /*std::cout << "back: " << data.back().id << " looking for " << col.first
                << " and " << col.second << "\n";*/
      auto A = get(col.first);
      auto B = get(col.second);

      for (auto &reac : biReactions) {
        if ((A->type == reac.A_type) && (B->type == reac.B_type)) {
          if (reac.testReaktion(*A, *B, *this)) {
            have_reacted.insert(A->id);
            have_reacted.insert(B->id);
            reactions_to_do.push_back(std::make_tuple(&reac, A, B));
            // reac.react(*A, *B, *this);
          }
        } else if ((B->type == reac.A_type) && (A->type == reac.B_type)) {
          if (reac.testReaktion(*B, *A, *this)) {
            have_reacted.insert(A->id);
            have_reacted.insert(B->id);
            reactions_to_do.push_back(std::make_tuple(&reac, B, A));
            // reac.react(*B, *A, *this);
          }
        }
      }
    }
  }
  for (auto &ding : reactions_to_do) {
    std::get<0>(ding)->react(*std::get<1>(ding), *std::get<2>(ding), *this);
  }
  // clear all reactions from this step from memory
  for (auto &p : data) {
    p.second.hasReacted.clear();
  }
}

/* in This function the interaction forces between all particels are calculated
 */
void pSystem::calculateInteractionForces() {
  for (const auto &col : collisionlist) {
    // std::cout << " handeling collision " << col.first << " and " <<
    // col.second
    //        << "\n";
    auto &A = *get(col.first);
    auto &B = *get(col.second);
    // no double accounting:
    assert(A.id < B.id);
    if (A.isIgnoring(B.id) || B.isIgnoring(A.id)) {
      continue; // Particles are ignoring one another. Skip interaction
    }
    double dist = calcOuterDistance(A, B);
    // check if they interact:
    if (dist <= 0) {
      // they are interacting. so check if they could react
      auto reacIter = std::find_if(biReactions.begin(), biReactions.end(),
                                   [&](BiMolecularReaction &reac) {
        return (A.type == reac.A_type && B.type == reac.B_type) ||
               (B.type == reac.A_type && A.type == reac.B_type);
      });

      if (reacIter == biReactions.end()) {
        // In Proximity, but NonReacting
        A.sumForce -= nonReactForce(-dist, A, B) * direction(A, B);
        B.sumForce -= nonReactForce(-dist, B, A) * direction(B, A);
      } else {

        // check for multiple reactions for particles of the same type.
        // choose one that is currently possible
        auto Aptr = &A;
        auto Bptr = &B; // swap to mach reaction
        if (A.type != reacIter->A_type) {
          std::swap(Aptr, Bptr);
        }

        // assert(biReactions.size()==2);
        if (!reacIter->reacPossibleFunc(*Aptr, *Bptr, *this)) {
          auto currentIter = reacIter;
          while (currentIter != biReactions.end()) {
            currentIter = std::find_if(currentIter + 1, biReactions.end(),
                                       [&](BiMolecularReaction &reac) {
              return (A.type == reac.A_type && B.type == reac.B_type) ||
                     (B.type == reac.A_type && A.type == reac.B_type);
            });
            if (Aptr->type != currentIter->A_type) {
              std::swap(Aptr, Bptr);
            }
            if (currentIter != biReactions.end()) {
              if (currentIter->reacPossibleFunc(*Aptr, *Bptr, *this)) {
                //  std::cout << "found better one!!\n";
                reacIter = currentIter;
              }
            }
          }
        } else {
          // std::cout << "stay with original\n";
        }
        // swap to mach reaction
        if (Aptr->type != reacIter->A_type) {
          std::swap(Aptr, Bptr);
        }

        // In Proximity, but there is a reaction
        // assert(false);
        // TODO Check for right type (in out) of reaction

        assert(reacIter != biReactions.end());
        assert(Aptr->type == reacIter->A_type);
        assert(Bptr->type == reacIter->B_type);

        double rate = reacIter->rateFunc(*Aptr, *Bptr, *this);
        double overlap = reacIter->partialOverlapp;

        // std::cout<<" found rate of "<<rate<<"\n";
        assert(rate >= 0);
        if (reacIter->reacPossibleFunc(*Aptr, *Bptr, *this)) {
          A.sumForce -= reactForce(dist, rate, A, B, overlap) * direction(B, A);
          B.sumForce -= reactForce(dist, rate, A, B, overlap) * direction(A, B);
        } else if (!reacIter->reacPossibleFunc(*Aptr, *Bptr, *this)) {
          A.sumForce -= nonReactForce(-dist, A, B) * direction(A, B);
          B.sumForce -= nonReactForce(-dist, B, A) * direction(B, A);
        }
        if (dist < -(1. + config::pushOutFakt) * 2 *
                       std::min(Aptr->radius, Bptr->radius)) {
          // std::cout << " Push out\n";
          dist += (1. + config::pushOutFakt) * 2 *
                  std::min(Aptr->radius, Bptr->radius);
          // Use PushOutForce if to close
          if (A.parentId == B.parentId) {
            A.sumForce -= nonReactForce(-dist, A, B) * direction(A, B);
            B.sumForce -= nonReactForce(-dist, B, A) * direction(B, A);
          }
        }
      }
    }
  }
}

/* This is the public adder for a zero order reaciton*/
ZeroMolecularReaction *pSystem::addZeroMolecularReaction() {
  zeroReactions.emplace_back();
  return &zeroReactions.back();
}

/* This is the public adder for a first order reaciton*/
UniMolecularReaction *pSystem::addUniMolecularReaction() {
  uniReactions.emplace_back();
  return &uniReactions.back();
}

/* This is the public adder for a first order reaciton*/
BiMolecularReaction *pSystem::addBiMolecularReaction() {
  biReactions.emplace_back();
  return &biReactions.back();
}

/* returrs the Volume (2d Area) of the sytem*/
double pSystem::Volume() const {
  double V = 1;
  for (const auto &x : systemSize) {
    V *= x;
  }
  return V;
}

/* this sets all the edges of the rectengular system to some value*/
void pSystem::allEdgesTo(double length) {
  for (auto &x : systemSize)
    x = length;
}

void pSystem::performZeroOrderReactions() {
  for (const auto &r : zeroReactions) {
    assert(-r.rateFunction(*this) * timestep < 0.0);
    double tmp = util::randomExp();
    if (r.rateFunction(*this) * timestep <= tmp) {
      auto newP = r.newParticle;
      r.funcAfter(newP, *this);
      addWhereFree(newP);
    }
  }
}

/* calculates the total kinetic Energy residing in the sytem
Ekin = 1/2 * mass * velocity^2 */
double pSystem::getKineticEnergy() {
  double e = 0;
  for (const auto &p : data) {
    e += .5 * p.second.mass * pow(p.second.velocity, 2);
  }
  return e;
}

/* this functions performs all the unimolecular reactions in the system.
If this uses to much time, maybe a sheduling appoach, could be faster*/
void pSystem::performUnimolecularReactions() {
  for (auto &r : uniReactions) {
    vector<int> reactingParticelIDs;
    for (auto &p : data) {
      if (p.second.type == r.A_type) {
        if (r.reacPossibleFunc(p.second, *this)) {
          double usedRate = r.rateFunction(p.second, *this);
          // double probability = 1. - exp(-usedRate * timestep);
          double tmp = util::randomExp();
          assert(-usedRate * timestep < 0.0);
          if (usedRate * timestep >= tmp) {
            reactingParticelIDs.push_back(p.second.id);
          }
        }
      }
    }
    for (auto &p : reactingParticelIDs) {
      r.react(*get(p));
    }
  }
}

/* if the system is finite in any direction, periodic boundary conditions are
applyed,
by moving the particels*/
void pSystem::boxify() {
  for (auto &p : data) {
    auto &ptcl = p.second;
    for (size_t i = 0; i < systemSize.dim(); i++) {
      assert(!std::isnan(ptcl.position[i]));
      assert(systemSize[i] > 0);
      if (systemSize[i] > 0) {
        while (ptcl.position[i] < 0) {
          ptcl.position[i] += systemSize[i];
          if (ptcl.position[i] < 0) {
            std::cout << "ERROR in position: " << ptcl.position[i] << " < 0\n";
          }
          // assert(p.position[i] >= 0);
        }
        while (ptcl.position[i] >= systemSize[i]) {
          ptcl.position[i] -= systemSize[i];
          if (ptcl.position[i] > systemSize[i]) {
            std::cout << "ERROR in position: " << ptcl.position[i]
                      << " >= " << systemSize[i] << "\n";
          }
          // assert(p.position[i] <= systemSize[i]);
        }
      }
    }
  }
}

/** this is the main integrator loop. Ones the system is setup you
only need to call thes funciton until the system has reached a disierd state
(e.g. time > x)*/
void pSystem::do_step() {
  assert(systemTemp > 0);
  assert(systemVisco > 0);

  // if this is the first step, initialize the neighbors
  double timestep_1 = 0;
  double timestep_2 = 0;
  double maxVel = abs(std::max_element(
      data.begin(), data.end(), [](auto par1, auto par2) {
        return (abs(par1.second.velocity) < abs(par2.second.velocity) ||
                par1.second.move == false);
      })->second.velocity);

  double minRad =
      std::min_element(data.begin(), data.end(), [](auto par1, auto par2) {
                         return (par1.second.radius < par2.second.radius);
                       })->second.radius;

  /*  double maxForce = abs(//falsche Kraft ausgew?hlt
            std::max_element(data.begin(), data.end(), [](auto par1, auto
    par2) {

                          return abs(par1.second.sumForce[2]) <
    abs(par2.second.sumForce[2]);

    }) ->second.sumForce);*/

  double maxForce = abs( // falsche Kraft ausgew?hlt
      std::max_element(
          data.begin(), data.end(), [](auto par1, auto par2) {
            return (abs(par1.second.sumForce) * (double)par1.second.move <
                    abs(par2.second.sumForce) * (double)par2.second.move /* ||
                                 par1.second.move == false*/);
          })->second.sumForce);

  double minMass =
      std::min_element(data.begin(), data.end(), [](auto par1, auto par2) {
                         return (par1.second.mass < par2.second.mass);
                       })->second.mass;

  timestep_1 = -maxVel * minMass / maxForce +
               sqrt(pow(maxVel * minMass / maxForce, 2) +
                    2. / config::granularity * minRad * minMass / maxForce);
  timestep_2 = 1. / config::granularity * minRad / maxVel;
  if (timestep_1 < timestep_2 && timestep_1 > 0) {
    timestep = timestep_1;
  } else {
    timestep = timestep_2;
  }

  if (stepCounter == 0) {
    updateCollisionlist();
    timestep = timestep / 1e10;
  }
  stepCounter++;

  if (stepCounter < 1000) {
    timestep = timestep / 100000;
  }
  ttimer total;
  total.start();

  // this assert is to fail, if numerical summation error accumulates
  // TODO there should be a better algorithm for time calculation or it should
  // throw an actuall Error
  // std::cout << "timestep is " << timestep << "\n";
  if ((time + timestep / 2 <= time) || (timestep <= 0)) {
    std::cerr << "\n\nERR: unusually extreme timestep of " << timestep
              << " at time " << time << " in step no. " << stepCounter << "\n";
    double maxVel = abs(std::max_element(
        data.begin(), data.end(), [](auto par1, auto par2) {
          return abs(par1.second.velocity) < abs(par2.second.velocity);
        })->second.velocity);
    double minRad =
        std::min_element(data.begin(), data.end(), [](auto par1, auto par2) {
                           return par1.second.radius < par2.second.radius;
                         })->second.radius;
    std::cerr << minRad << " is min rad and " << maxVel << " is max vel\n";
    throw std::runtime_error("extreme timestep (no particle?)");
  }

  for (auto &x : data) {
    auto &ptcl = x.second;
    for (auto &p : ptcl.position) {
      assert(p >= 0);
      assert(p <= systemSize.at(0));
    }
  }

  time += timestep;
  assert(timestep > 0);
  // Run the reactions witch are not collision bound
  // CALLGRIND_START_INSTRUMENTATION;
  lowReacTime.start();
  performUnimolecularReactions();
  lowReacTime.stop();

  zeroReacTime.start();
  performZeroOrderReactions();
  zeroReacTime.stop();
  // CALLGRIND_STOP_INSTRUMENTATION;

  flt_t minRadius =
      data.begin()->second.radius; // for calculation of the smallest particel
  maxSpeed = 0;
  assert(timestep > 0);
  // the first part of the main numerical iterator loop

  for (auto &x : data) {
    auto &ptcl = x.second;
    for (auto &p : ptcl.position) {
      assert(!std::isnan(p));
    }
  }

  gausNumberTime.start();
  for (auto &p : data) {
    auto &ptcl = p.second;
    assert(ptcl.containingSystem == this); // could be because of wrong copying

    for (auto &p : ptcl.position) {
      assert(!std::isnan(p));
    }

    ptcl.diffusionForce();
    ptcl.updateVelocity();
    ptcl.updatePosition();

    for (auto &p : ptcl.position) {
      assert(!std::isnan(p));
    }

    // calculated min/max
    minRadius = std::min(static_cast<flt_t>(ptcl.radius), minRadius);
    maxSpeed = std::max(abs(ptcl.velocity), maxSpeed);
  }
  gausNumberTime.stop();

  // now that positions have changed reaplly periodic boundary conditions
  boxify();
  // updateCollisionlist();

  // now all collision bound reactions can be performed
  biReacTime.start();
  testAndPerformReactions();
  biReacTime.stop();

  collisionTime.start();
  updateCollisionlist();
  collisionTime.stop();

  listHandelTime.start();
  // Calculate forces from inter particl interaction
  for (auto &p : data) {
    p.second.sumForce.init();
    // Example central Force for Demonstration:
    // p.sumForce -= .01 * (p.position - .5 * systemSize);

    p.second.recalculateAllConstants();
    p.second.recalculateMass();
    Vec richtung;
    richtung.init();
    richtung[0] = 1;
    // p.sumForce += .4*richtung*sin(time*.5);
  }
  // std::cerr << "starting interaction calculation\n";
  calculateInteractionForces();

  for (auto &x : data) {
    for (auto &func : forceFunc) {
      func(*this, x.second);
    }
  }

  // std::cout << "before pF: " << pairFunctions.size();
  auto me = this;
  pairFunctions.erase(
      std::remove_if(pairFunctions.begin(), pairFunctions.end(),
                     [=
  ](std::pair<std::pair<ptclid_t, ptclid_t>,
                     std::function<bool(Particle &, Particle &, pSystem &)>> &
                         ding) {
        auto A = me->get(ding.first.first);
        auto B = me->get(ding.first.second);
        assert(A->isIgnoring(B->id));
        assert(B->isIgnoring(A->id));
        return ding.second(*A, *B, *me);
      }),
      pairFunctions.end());
  // std::cerr << " looking for " << ding.first.first << " and "
  //          << ding.first.second << "\n";
  // std::cout << "  after pF: " << pairFunctions.size() << "\n";

  listHandelTime.stop();

  gausNumberTime.start();
  // second part of numerical iterator
  for (auto &p : data) {
    auto &ptcl = p.second;
    ptcl.updateVelocity();
    ptcl.diffusionForce();
    ptcl.recalculateAllConstants();
    ptcl.recalculateMass();

    // extra model specific funcions
    if (ptcl.targetRadius != ptcl.radius) {
      assert(ptcl.targetRadius > 0);
      assert(ptcl.radius > 0);
      if (ptcl.is_ignored_by_ids.empty()) {
        double delta = ptcl.radius - ptcl.targetRadius;
        // std::cout << "target " << ptcl.targetRadius << " is " << ptcl.radius
        //         << " delta " << delta << "\n";
        if (delta < 0) {
          if (ptcl.radius - delta * timestep * config::growthRate >
              ptcl.targetRadius) {
            ptcl.radius = ptcl.targetRadius;
          } else {
            ptcl.radius -= delta * timestep * config::growthRate;
          }
        } else {
          if (ptcl.radius - delta * timestep * config::growthRate <
              ptcl.targetRadius) {
            ptcl.radius = ptcl.targetRadius;
          } else {
            ptcl.radius -= delta * timestep * config::growthRate;
          }
        }

        // ptcl.radius = ptcl.targetRadius;
      } else { // is in division
        assert(ptcl.is_ignoring_ids.size() >= 1);
        auto otherPtcl = get(ptcl.is_ignoring_ids.back());
        double deltaReg = ptcl.radius - ptcl.targetRadius;
        double fakt = -calcOuterDistance(ptcl, *otherPtcl) /
                      (ptcl.radius + otherPtcl->radius);
        assert(fakt <= 1);
        assert(fakt >= -.1);

        if (deltaReg < 0) {
          if (ptcl.radius -
                  timestep * config::growthRate * (1 - fakt) * deltaReg >
              ptcl.targetRadius) {
            ptcl.radius = ptcl.targetRadius;
          } else {
            ptcl.radius -=
                timestep * config::growthRate * (1 - fakt) * deltaReg;
          }
        } else {
          if (ptcl.radius -
                  timestep * config::growthRate * (1 - fakt) * deltaReg <
              ptcl.targetRadius) {
            ptcl.radius = ptcl.targetRadius;
          } else {
            ptcl.radius -=
                timestep * config::growthRate * (1 - fakt) * deltaReg;
          }
        }
      }
      assert(ptcl.radius > 0);
    }
    /*if (p.type == 2) {
      p.targetRadius *= std::pow(.6, timestep);
    }
    if (p.type == 3) {
      p.targetRadius *= std::pow(.8, timestep);
    }*/
  }
  gausNumberTime.stop();

  // do model specific general functions
  for (auto &func : generalFunc) {
    assert(false);
    func(*this);
  }

  /*from the maxiumm speed and the minimum radius a new timestep can be
  calculated the
  worst case assumption is, that the smallest particel moves with the maximum
  speed in the sytem,
  it will still be sampiled with a certian gnarularity*/

  /////////////////////
  // TODO: remove this
  /*minRadius = .5;

  assert(maxSpeed > 0);
  assert(minRadius > 0);
  timestep = minRadius / maxSpeed / config::granularity;
  if (stepCounter < 100) {
    timestep /= 3;
  }
  double maxTimeStep = 200 * sqrt(2 * nature::boltzmann * systemTemp /
                                  data.begin()->second.mass);
  if (timestep > maxTimeStep) {
    std::cout << "timestep too large\n";
    timestep = maxTimeStep;
  }
  total.stop();

  double maxVel =
      abs(std::max_element(data.begin(), data.end(), [](auto par1, auto par2) {
            return abs(par1.second.velocity) < abs(par2.second.velocity);
          })->second.velocity);
  double minRad =
      std::min_element(data.begin(), data.end(), [](auto par1, auto par2) {
        return par1.second.radius < par2.second.radius;
      })->second.radius;
  timestep = minRad / maxVel / config::granularity;
  assert(minRad / maxVel / timestep >= .9 * config::granularity);

  // Calc kinetic enegy
  /*std::cout << "KinEnergy: " << getKineticEnergy() / units::joule << "\t minR"
            << minRad / units::nano_meter << "\tmaxVel "
            << maxVel / (units::meter / units::second) << " in a system of "
            << systemSize[0] / units::nano_meter << "\n";
*/
  /*  std::cout << 100 * colup.timeInSec() / total.timeInSec()
           << "% spend finding collisions\n";*/
}
