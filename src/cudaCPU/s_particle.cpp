#include "simulator/s_particle.h"
#include "utilities/mVec.h"

ptclid_t idCounter = 0;

s_particle::s_particle(flt_t a, flt_t b, flt_t c) {
  pos[0] = a;
  pos[1] = b;
  pos[2] = c;
  id = idCounter;
  idCounter++;
}

void resetParticleCounter() { idCounter = 0; }

CUDA_HOSTDEV
s_particle::s_particle() {};

CUDA_HOSTDEV
bool areIntersectingNonPer2(const s_particle &a, const s_particle &b,
                            const Vec &sys) {
  return areIntersectingNonPer(a, b);
}

CUDA_HOSTDEV
bool areIntersectingNonPer(const s_particle &a, const s_particle &b) {
  const flt_t dist = abs(a.pos - b.pos);
  if (a.radius + b.radius > dist) {
    // return true;//// ONLY OUTSIDE
    if (a.radius > b.radius) {
      if (dist + b.radius > a.inner_radius) {
        return true;
      }
    } else {
      if (dist + a.radius > b.inner_radius)
        return true;
    }
  }
  return false;
}

CUDA_HOSTDEV
bool areIntersecting(const s_particle &a, const s_particle &b,
                     const Vec &systemSize) {
  flt_t dist = 0;
  for (unsigned int idx = 0; idx < systemSize.dim(); idx++) {
    flt_t metaDist = fabs(a.pos[idx] - b.pos[idx]);
    assert(systemSize[idx] > 0);
    dist +=
        std::pow(std::min(metaDist,
                          static_cast<flt_t>(fabs(metaDist - systemSize[idx]))),
                 2);
  }
  dist = sqrt(dist);
  if (a.radius + b.radius > dist) {
    // return true;//// ONLY OUTSIDE
    if (a.radius > b.radius) {
      if (dist + b.radius > a.inner_radius) {
        return true;
      }
    } else {
      if (dist + a.radius > b.inner_radius)
        return true;
    }
  }
  return false;
}

CUDA_HOSTDEV
bool AABB_intersect(const s_particle &a, const s_particle &b,
                    const Vec &systemSize) {
  for (unsigned int idx = 0; idx < systemSize.dim(); idx++) {
    flt_t dist = fabs(a.pos[idx] - b.pos[idx]);
    if (dist > systemSize[idx] / 2.) {
      dist -= systemSize[idx];
      dist = fabs(dist);
    }
    if (dist > a.radius + b.radius) {
      return false;
    }
  }
  return true;
}

CUDA_HOSTDEV
bool areIntersectingSSX(const s_particle &a, const s_particle &b,
                        const Vec &systemSize) {
  flt_t dist = 0;
  for (unsigned int idx = 0; idx < systemSize.dim(); idx++) {
    flt_t metaDist = fabs(a.pos[idx] - b.pos[idx]);
    assert(systemSize[idx] > 0);
    dist +=
        std::pow(std::min(metaDist,
                          static_cast<flt_t>(fabs(metaDist - systemSize[idx]))),
                 2);
  }
  dist = sqrt(dist);
  if (a.radius + b.radius > dist) {
    return true;
  }
  return false;
}
