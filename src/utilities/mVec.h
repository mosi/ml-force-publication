#ifdef __CUDACC__
#define CUDA_HOSTDEV __host__ __device__
#else
#define CUDA_HOSTDEV
#endif

#include "types.h"
//#include "utilities/Utilities.h"
#include <array>
#include <assert.h>
#include <cmath>
#include <iostream>

//##include "utilities/Utilities.h"

#pragma once
/** @brief Mathematical Vector implementation
 *
 * This provides a mathematical Vector including overloads on common Operators
 * (*,+, ... ) and some functions (abs, normalize)
 */
template <size_t N> struct mVec {
  CUDA_HOSTDEV mVec<N> operator-() const;

  CUDA_HOSTDEV mVec<N> operator+=(mVec<N> const &rhs);

  CUDA_HOSTDEV mVec<N> operator-=(mVec<N> const &rhs);

  CUDA_HOSTDEV mVec<N> operator*=(flt_t const &rhs);

  CUDA_HOSTDEV mVec<N> operator/=(const flt_t &d);

  CUDA_HOSTDEV mVec<N>();
  CUDA_HOSTDEV mVec<N>(flt_t val);

  CUDA_HOSTDEV void init(flt_t = 0.);
  CUDA_HOSTDEV size_t dim() const { return N; }

  CUDA_HOSTDEV mVec<N> normalize(const flt_t scale);
  CUDA_HOSTDEV mVec<N> normalize();
  CUDA_HOSTDEV const flt_t &operator[](size_t idx) const;
  CUDA_HOSTDEV flt_t &operator[](size_t idx);
  const flt_t &at(size_t) const;
  flt_t &at(size_t);
  CUDA_HOSTDEV flt_t *begin();
  CUDA_HOSTDEV flt_t *end();
  CUDA_HOSTDEV flt_t const *begin() const;
  CUDA_HOSTDEV flt_t const *end() const;
  /*  flt_t* begin() const;
    flt_t* end() const;*/
private:
  flt_t data[N];
};

///////////////
// Functions //
///////////////
/**This normalizes the Given Vector (meaning its length becomes 1)
 * */
template <size_t N> CUDA_HOSTDEV mVec<N> normalize(const mVec<N> &a);

template <size_t N> CUDA_HOSTDEV flt_t abs(mVec<N> V);

template <size_t N> CUDA_HOSTDEV flt_t pow(const mVec<N> &b, const int e);

///////////////////////
//  Simple Overloads //
///////////////////////
template <size_t N>
CUDA_HOSTDEV mVec<N> operator+(mVec<N> const &lhs, mVec<N> const &rhs);

template <size_t N>
CUDA_HOSTDEV mVec<N> operator-(mVec<N> const &lhs, mVec<N> const &rhs);

template <size_t N, class T>
CUDA_HOSTDEV mVec<N> operator*(T const &lhs, mVec<N> const &rhs);

template <size_t N, class T>
CUDA_HOSTDEV mVec<N> operator*(mVec<N> const &lhs, T const &rhs);

template <size_t N, class T>
CUDA_HOSTDEV mVec<N> operator/(mVec<N> const &lhs, T const &rhs);

template <size_t N>
CUDA_HOSTDEV std::ostream &operator<<(std::ostream &lhs, mVec<N> const &rhs);

#include "mVec.cu"
