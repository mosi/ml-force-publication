/* in this file some globaly usefull constants and function are defined*/

#pragma once

#include "mVec.h"
#include "types.h"
#include "units.h"
#include <algorithm>
#include <random>

#undef NDEBUG
#include <cassert>

const bool use_periodic = false;

const int DIMENSION = DEF_DIM;
static_assert(DIMENSION == 3 || DIMENSION == 2, "only 2d or 3d possible");
typedef mVec<DIMENSION> Vec;

const flt_t mPI = static_cast<flt_t>(3.14159265359);

namespace util {
flt_t gaus(flt_t);
flt_t randomZeroOne();
flt_t randomExp(flt_t l = 1);
flt_t randomPMone();
// std::vector<std::pair<flt_t, flt_t>> binning(const std::vector<flt_t> &dat,
// size_t nrOfBins);
/* some simple binning utility for creating a histogramm */

template <typename T>
std::vector<std::pair<T, T>> binning(const std::vector<T> &dat,
                                     size_t nrOfBins) {
  assert(dat.size() > 10);
  assert(nrOfBins > 4);

  std::vector<size_t> countDat(nrOfBins + 1);
  for (auto &x : countDat)
    x = 0;

  T min = *std::min_element(dat.begin(), dat.end());
  T max = *std::max_element(dat.begin(), dat.end());
  assert(min < max);
  const double delta = (max - min) / (nrOfBins);

  for (const auto &x : dat) {
    assert(x >= min);
    assert(x <= max);
    countDat.at(static_cast<size_t>((x - min) / delta))++;
  }

  std::vector<std::pair<T, T>> result(nrOfBins + 1);
  assert(result.size() == countDat.size());

  for (size_t idx = 0; idx < result.size(); idx++) {
    result[idx].first = idx * delta + min + .5 * delta;
    result[idx].second = countDat[idx];
    result[idx].second /= delta * dat.size();
  }
  return result;
}

Vec randomGausVec(flt_t);
} // namespace util

bool SafeLess(int x, unsigned int y);
bool SafeLess(unsigned int x, int y);

bool SafeEqual(int x, unsigned int y);

bool SafeEqual(unsigned int x, int y);
