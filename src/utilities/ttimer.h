#include <cassert>
#include <chrono>

#pragma once

struct ttimer {
  std::chrono::time_point<std::chrono::high_resolution_clock> startTime, end;
  bool running = false;
  double totalTimePast = 0;
  void start() {
    assert(!running);
    running = true;
    startTime = std::chrono::high_resolution_clock::now();
  }

  double timePassed() const {
    auto now = std::chrono::high_resolution_clock::now();
    assert(running);
    std::chrono::duration<double> totalTime = now - startTime;
    return totalTime.count();
  }

  void reset() {
    assert(!running);
    totalTimePast = 0;
  }

  void restart() {
    assert(running);
    running = false;
    start();
  }

  void stop() {
    end = std::chrono::high_resolution_clock::now();
    assert(running);
    std::chrono::duration<double> totalTime = end - startTime;
    totalTimePast += totalTime.count();
    running = false;
  }
  double timeInSec() {
    assert(!running);

    return totalTimePast;
  }
};
