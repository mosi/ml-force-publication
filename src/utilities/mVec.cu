
#include <array>
#include <assert.h>
#include <cmath>
#include <iostream>

#include "mVec.h"

/** @brief Mathematical Vector implementation
 *
 * This provides a mathematical Vector including overloads on common Operators
 * (*,+, ... ) and some functions (abs, normalize)
 */
template <size_t N> CUDA_HOSTDEV flt_t &mVec<N>::operator[](size_t idx) {
  return data[idx];
}
template <size_t N>
CUDA_HOSTDEV const flt_t &mVec<N>::operator[](size_t idx) const {
  return data[idx];
}

template <size_t N> const flt_t &mVec<N>::at(size_t idx) const {
  if (idx >= N) {
    std::cout << "Error: mVec access out of bound " << idx << " > " << N
              << "\n";
    assert(idx < N);
  }
  return data[idx];
}

template <size_t N> flt_t &mVec<N>::at(size_t idx) {
  if (idx >= N) {
    std::cout << "Error: mVec access out of bound " << idx << " > " << N
              << "\n";
    assert(idx < N);
  }
  return data[idx];
}

/*template <size_t N>  CUDA_HOSTDEV flt_t* mVec<N>::end() const {
  return data + N ;///TODO FALSE FALSE
}

template <size_t N>  CUDA_HOSTDEV flt_t* mVec<N>::begin() const {
  return data;
}*/

template <size_t N> CUDA_HOSTDEV flt_t *mVec<N>::end() {
  return data + N; /// TODO FALSE FALSE
}

template <size_t N> CUDA_HOSTDEV flt_t const *mVec<N>::end() const {
  return data + N; /// TODO FALSE FALSE
}

template <size_t N> CUDA_HOSTDEV flt_t *mVec<N>::begin() { return data; }
template <size_t N> CUDA_HOSTDEV flt_t const *mVec<N>::begin() const {
  return data;
}

template <size_t N> CUDA_HOSTDEV mVec<N> mVec<N>::operator-() const {
  mVec<N> res;
  for (size_t i = 0; i < N; i++) {
    res[i] = -(*this)[i];
  }
  return res;
}

template <size_t N>
CUDA_HOSTDEV mVec<N> mVec<N>::operator+=(mVec<N> const &rhs) {
  for (size_t i = 0; i < N; i++) {
    (*this)[i] += rhs[i];
  }
  return *this;
}

template <size_t N>
CUDA_HOSTDEV mVec<N> mVec<N>::operator-=(mVec<N> const &rhs) {
  for (size_t i = 0; i < N; i++) {
    (*this)[i] -= rhs[i];
  }
  return *this;
}

template <size_t N> CUDA_HOSTDEV mVec<N> mVec<N>::operator*=(flt_t const &rhs) {
  for (auto &k : *this) {
    k *= rhs;
  }
  return *this;
}

template <size_t N> CUDA_HOSTDEV mVec<N> mVec<N>::operator/=(const flt_t &d) {
  for (size_t i = 0; i < N; i++) {
    (*this)[i] /= d;
  }
  return *this;
}

template <size_t N> CUDA_HOSTDEV mVec<N>::mVec() {}
template <size_t N> CUDA_HOSTDEV mVec<N>::mVec(flt_t val) {
  for (auto &k : *this) {
    k = val;
  }
}

template <size_t N> CUDA_HOSTDEV void mVec<N>::init(flt_t initVal) {
  for (auto &k : *this) {
    k = initVal;
  }
}

template <size_t N> CUDA_HOSTDEV mVec<N> mVec<N>::normalize(const flt_t scale) {
  *this /= (abs(*this) / scale);
  return *this;
}

template <size_t N> CUDA_HOSTDEV mVec<N> mVec<N>::normalize() {
  return *this / abs(*this);
}

///////////////
// Functions //
///////////////
/**This normalizes the Given Vector (meaning its length becomes 1)
 * */
template <size_t N> CUDA_HOSTDEV mVec<N> normalize(const mVec<N> &a) {
  return (a / abs(a));
}

template <size_t N> CUDA_HOSTDEV flt_t abs(mVec<N> V) {
  if (N == 1) {
    return std::abs(V[0]);
  }

  flt_t sum = 0;
  for (auto &k : V) {
    sum += k * k;
  }
  return std::sqrt(sum);
}

template <size_t N> CUDA_HOSTDEV flt_t pow(const mVec<N> &b, const int e) {
  assert(e == 2);
  flt_t ret = 0;
  for (auto &i : b) {
    ret += i * i;
  }
  return ret;
}

///////////////////////
//  Simple Overloads //
///////////////////////
template <size_t N>
mVec<N> CUDA_HOSTDEV operator+(mVec<N> const &lhs, mVec<N> const &rhs) {
  mVec<N> res(lhs);
  for (size_t i = 0; i < N; i++) {
    res[i] += rhs[i];
  }
  return res;
}

template <size_t N>
CUDA_HOSTDEV mVec<N> operator-(mVec<N> const &lhs, mVec<N> const &rhs) {
  mVec<N> res(lhs);
  for (size_t i = 0; i < N; i++) {
    res[i] -= rhs[i];
  }
  return res;
}

template <size_t N, class T>
CUDA_HOSTDEV mVec<N> operator*(T const &lhs, mVec<N> const &rhs) {
  mVec<N> res(rhs);
  for (auto &k : res) {
    k *= lhs;
  }
  return res;
}

template <size_t N, class T>
CUDA_HOSTDEV mVec<N> operator*(mVec<N> const &lhs, T const &rhs) {
  return rhs * lhs;
}

template <size_t N, class T>
CUDA_HOSTDEV mVec<N> operator/(mVec<N> const &lhs, T const &rhs) {
  mVec<N> res(lhs);
  for (auto &k : res) {
    k /= rhs;
  }
  return res;
}

template <size_t N>
std::ostream &operator<<(std::ostream &lhs, mVec<N> const &rhs) {
  lhs << "\t";
  for (auto &k : rhs) {
    lhs << k << " ";
  }
  lhs << "\t";
  return lhs;
}

// No need to call this TemporaryFunction() function,
// it's just to avoid link error.
/*void __TemporaryFunction ()
{
    mVec<1> a;
    abs(a);
    a-a;
    mVec<2> b;
    abs(b);
    b-b;
    mVec<3> c;
    abs(c);
    c-c;
    mVec<4> d;
    assert(false);
}*/
