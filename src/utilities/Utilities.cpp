#include "Utilities.h"
#include "settings.h"
#include <algorithm>
#include <future>
#include <ctime>

namespace util {
// use a deterministec mersene twister random number generator
const unsigned int seed = time(0);
std::mt19937_64 mersenneGenerator = std::mt19937_64(seed);
std::normal_distribution<flt_t> gausDistro =
    std::normal_distribution<flt_t>(0, 1);
std::uniform_real_distribution<flt_t> zeroOneDist(0, 1);

std::exponential_distribution<flt_t> expoDist(1.);

// Prepare async gaus
std::array<flt_t, config::nrOfGausToPrepare> randomAgaus;
std::array<flt_t, config::nrOfGausToPrepare> randomBgaus;

std::future<void> waitAgaus;
std::future<void> waitBgaus;

bool useAgaus = false;
bool useBgaus = false;
size_t indxgaus = 0;

void fillgaus(std::array<flt_t, config::nrOfGausToPrepare> &dat) {
  for (auto &val : dat) {
    val = gausDistro(mersenneGenerator);
  }
}

// Prepare async random zeroNone
std::array<flt_t, config::nrOfGausToPrepare> randomAzo;
std::array<flt_t, config::nrOfGausToPrepare> randomBzo;

std::future<void> waitAzo;
std::future<void> waitBzo;

bool useAzo = false;
bool useBzo = false;
size_t indxzo = 0;

void fillzo(std::array<flt_t, config::nrOfGausToPrepare> &dat) {
  for (auto &val : dat) {
    val = zeroOneDist(mersenneGenerator);
  }
}

/* returns a normaly distributed flt_t */
flt_t gaus(flt_t x = 1) {
  if (!config::useAsyncGaus) {
    return gausDistro(mersenneGenerator) * x;
  }
  indxgaus++;
  if (indxgaus == config::nrOfGausToPrepare - 1) {
    if (useAgaus) {
      useAgaus = false;
      waitAgaus = std::async([&]() { fillgaus(randomAgaus); });
      waitBgaus.get();
      useBgaus = true;
    } else if (useBgaus) {
      useBgaus = false;
      waitBgaus = std::async([&]() { fillgaus(randomBgaus); });
      waitAgaus.get();
      useAgaus = true;
    }
    indxgaus = 0;
  }

  if (useAgaus) {
    return x * randomAgaus[indxgaus];
  } else if (useBgaus) {
    return x * randomBgaus[indxgaus];
  } else {
    fillgaus(randomAgaus);
    waitBgaus = std::async([&]() { fillgaus(randomBgaus); });
    useAgaus = true;
    indxgaus = 1;
    return x * randomAgaus[0];
  }
}

flt_t randomExp(flt_t l) {
  // assert(false);
  return expoDist(mersenneGenerator);
}

flt_t randomExp() { return expoDist(mersenneGenerator); }
/* this returns a psoudorandom flt_t between 0 and 1 */
flt_t randomZeroOne() {
  if (!config::useAsyncGaus) {
    return zeroOneDist(mersenneGenerator);
  }

  indxzo++;
  if (indxzo == config::nrOfGausToPrepare - 1) {
    if (useAzo) {
      useAzo = false;
      waitAzo = std::async([&]() { fillzo(randomAzo); });
      waitBzo.get();
      useBzo = true;
    } else if (useBzo) {
      useBzo = false;
      waitBzo = std::async([&]() { fillzo(randomBzo); });
      waitAzo.get();
      useAzo = true;
    }
    indxzo = 0;
  }

  if (useAzo) {
    return randomAzo[indxzo];
  } else if (useBzo) {
    return randomBzo[indxzo];
  } else {
    fillzo(randomAzo);
    waitBzo = std::async([&]() { fillzo(randomBzo); });
    useAzo = true;
    indxzo = 1;
    return randomAzo[0];
  }
}

flt_t randomPMone() { return 2. * (randomZeroOne() - .5); }

/* generate a random vector with normaly distrebuted componets*/
Vec randomGausVec(flt_t sigma = 1) {
  Vec v;
  for (auto &x : v) {
    x = gaus(sigma);
  }
  return v;
}

} // namespace util

bool SafeLess(int x, unsigned int y) {
  return (x < 0) || (static_cast<unsigned int>(x) < y);
}

bool SafeLess(unsigned int x, int y) {
  return (y >= 0) && (x < static_cast<unsigned int>(y));
}

bool SafeEqual(int x, unsigned int y) {
  return (x >= 0) && (y == static_cast<unsigned int>(x));
}

bool SafeEqual(unsigned int x, int y) { return SafeEqual(y, x); }
