/* typdefs used throughout */

#pragma once
#include <climits>

typedef unsigned int uint;

typedef double flt_t;
typedef uint ptclid_t;
typedef uint cellid_t; // limted by atomic capacity of GPU

const ptclid_t ptclid_t_max = UINT_MAX;
const cellid_t cellid_t_max = UINT_MAX;
