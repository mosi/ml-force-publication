#pragma once
#include <cmath>

namespace units {

// PREFIXES

const double tera = 1e15;
const double giga = 1e9;
const double mega = 1e6;
const double kilo = 1e3;
const double hecto = 1e2;
const double deca = 1e1;
const double deci = 1e-1;
const double centi = 1e-2;
const double milli = 1e-3;
const double micro = 1e-6;
const double nano = 1e-9;
const double pico = 1e-12;
const double femto = 1e-15;

// EDIT TO CHANGE THE INTERNAL UNITS
const double internal_length = nano;
const double internal_mass = 1.;
const double internal_time = nano;
const double internal_temperature = 1.;

// Basal units
const double meter = 1. / internal_length;
const double kilogram = 1. / internal_mass;
const double second = 1. / internal_time;
const double kelvin = 1. / internal_temperature;

// derived units
const double nano_meter = 1e-9 * meter;
const double centimeter = 1e-1 * meter;
const double angstrom = 1e-10 * meter;
const double gram = 1e-3 * kilogram;
const double newton = kilogram * meter / (second * second);
const double pascal = newton / (meter * meter);
const double joule = newton * meter;
const double minute = 60. * second;
} // namespace units

namespace nature {
const double water_density = 1. * units::gram / std::pow(units::centimeter, 3);
const double protein_density =
    1400e-27 * units::kilogram / std::pow(units::nano_meter, 3);
const double boltzmann = 1.3806485e-23 * units::joule / units::kelvin;
const double water_viscocity = 8.9e-4 * units::pascal * units::second;
const double room_temperature = 300 * units::kelvin;
const double typical_radius = .5 * units::nano_meter;
} // namespace nature
