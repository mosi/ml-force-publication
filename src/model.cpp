#include "simulator/dsl.h"

using namespace dsl;

namespace model {
// List of all awailable Models
void create_test();
void decay_test();
void irrev_react();
void irrev_react_2D();
void rev_react();
void diff_test();
void vesicular();
void yeast_model();
void lipidraft_model();
} // namespace model

void dsl::model() {
  // The model to use
  model::irrev_react_2D();
}
