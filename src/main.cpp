#include "utilities/Utilities.h"
//#include "benchmarks.h"
#include "collision/gpuCon.h"
//#include "ofMain.h"
#include "simulator/system.h"
#include <map>

#include "mapbox/variant.hpp"
#include "settings.h"
#include "simulator/dsl.h"
#include "utilities/ttimer.h"

#include <iostream>

double size_faktor = 3000. / 11.;

//#include "valgrind/callgrind.h"

int main() {
  std::cout << "water_density: " << nature::water_density
            << "\nwater_visc: " << nature::water_viscocity
            << "\nboltzmann: " << nature::boltzmann << "\n";

  dsl::setupDSL();
  dsl::model();
  return 0;
}
